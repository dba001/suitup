module.exports = {
  env: {
    production: {
      plugins: ['transform-remove-console'],
    },
  },
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          // app
          aws: './src/aws',
          components: './src/components',
          constants: './src/static/constants',
          helpers: './src/helpers',
          images: './src/static/images',
          scenes: './src/scenes',
          theme: './src/theme',
          // redux stuff
          articles: './src/redux/articles',
          categoryQueues: './src/redux/categoryQueues',
          baskets: './src/redux/baskets',
          categories: './src/redux/categories',
          errors: './src/redux/errors',
          selectors: './src/redux/selectors',
          sessions: './src/redux/sessions',
          settings: './src/redux/settings',
          users: './src/redux/users',
          variationInfo: './src/redux/variationInfo',
        },
        extensions: ['.js', '.ios.js', '.android.js'],
      },
    ],
    '@babel/transform-flow-strip-types',
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread',
    '@babel/transform-runtime',
  ],
  presets: ['module:metro-react-native-babel-preset'],
};
