#!/usr/bin/env bash

NODE_VERSION=$(node --version)
NPM_VERSION=$(npm --version)

echo "*******************************************************"
echo "*                 POST CLONE INFO                     *"
echo "*******************************************************"
echo "*                                                     *"
echo "* NODE VERSION: $NODE_VERSION                         *"
echo "* NPM VERSION: $NPM_VERSION                           *"
echo "*                                                     *"
echo "*******************************************************"
