module.exports = {
  preset: 'react-native',
  setupFiles: [
    './jest.setup.js',
    '<rootDir>/node_modules/appcenter/test/AppCenterMock.js',
    '<rootDir>/node_modules/appcenter-analytics/test/AppCenterAnalyticsMock.js',
    '<rootDir>/node_modules/appcenter-crashes/test/AppCenterCrashesMock.js',
  ],
};
