/* @flow */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateArticleInput = {|
  asin: string,
  browsenode: string,
|};

export type UpdateArticleInput = {|
  asin?: ?string,
  browsenode?: ?string,
|};

export type DeleteArticleInput = {|
  id?: ?string,
|};

export type CreateUserInput = {|
  id?: ?string,
  sub: string,
|};

export type UpdateUserInput = {|
  id: string,
  sub?: ?string,
|};

export type DeleteUserInput = {|
  id?: ?string,
|};

export type CreateSessionInput = {|
  id?: ?string,
  sessionUserId?: ?string,
|};

export type UpdateSessionInput = {|
  id: string,
  sessionUserId?: ?string,
|};

export type DeleteSessionInput = {|
  id?: ?string,
|};

export type ModelArticleFilterInput = {|
  asin?: ?ModelIDFilterInput,
  browsenode?: ?ModelIDFilterInput,
  and?: ?Array< ?ModelArticleFilterInput >,
  or?: ?Array< ?ModelArticleFilterInput >,
  not?: ?ModelArticleFilterInput,
|};

export type ModelIDFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
|};

export type ModelUserFilterInput = {|
  id?: ?ModelIDFilterInput,
  sub?: ?ModelStringFilterInput,
  and?: ?Array< ?ModelUserFilterInput >,
  or?: ?Array< ?ModelUserFilterInput >,
  not?: ?ModelUserFilterInput,
|};

export type ModelStringFilterInput = {|
  ne?: ?string,
  eq?: ?string,
  le?: ?string,
  lt?: ?string,
  ge?: ?string,
  gt?: ?string,
  contains?: ?string,
  notContains?: ?string,
  between?: ?Array< ?string >,
  beginsWith?: ?string,
|};

export type ModelSessionFilterInput = {|
  id?: ?ModelIDFilterInput,
  and?: ?Array< ?ModelSessionFilterInput >,
  or?: ?Array< ?ModelSessionFilterInput >,
  not?: ?ModelSessionFilterInput,
|};

export type CreateArticleMutationVariables = {|
  input: CreateArticleInput,
|};

export type CreateArticleMutation = {|
  createArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type UpdateArticleMutationVariables = {|
  input: UpdateArticleInput,
|};

export type UpdateArticleMutation = {|
  updateArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type DeleteArticleMutationVariables = {|
  input: DeleteArticleInput,
|};

export type DeleteArticleMutation = {|
  deleteArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type CreateUserMutationVariables = {|
  input: CreateUserInput,
|};

export type CreateUserMutation = {|
  createUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type UpdateUserMutationVariables = {|
  input: UpdateUserInput,
|};

export type UpdateUserMutation = {|
  updateUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type DeleteUserMutationVariables = {|
  input: DeleteUserInput,
|};

export type DeleteUserMutation = {|
  deleteUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type CreateSessionMutationVariables = {|
  input: CreateSessionInput,
|};

export type CreateSessionMutation = {|
  createSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type UpdateSessionMutationVariables = {|
  input: UpdateSessionInput,
|};

export type UpdateSessionMutation = {|
  updateSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type DeleteSessionMutationVariables = {|
  input: DeleteSessionInput,
|};

export type DeleteSessionMutation = {|
  deleteSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type GetArticleQueryVariables = {|
  id: string,
|};

export type GetArticleQuery = {|
  getArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type ListArticlesQueryVariables = {|
  filter?: ?ModelArticleFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListArticlesQuery = {|
  listArticles: ? {|
    __typename: "ModelArticleConnection",
    items: ? Array<? {|
      __typename: string,
      asin: string,
      browsenode: string,
    |} >,
    nextToken: ?string,
  |},
|};

export type GetUserQueryVariables = {|
  id: string,
|};

export type GetUserQuery = {|
  getUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type ListUsersQueryVariables = {|
  filter?: ?ModelUserFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListUsersQuery = {|
  listUsers: ? {|
    __typename: "ModelUserConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |} >,
    nextToken: ?string,
  |},
|};

export type GetSessionQueryVariables = {|
  id: string,
|};

export type GetSessionQuery = {|
  getSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type ListSessionsQueryVariables = {|
  filter?: ?ModelSessionFilterInput,
  limit?: ?number,
  nextToken?: ?string,
|};

export type ListSessionsQuery = {|
  listSessions: ? {|
    __typename: "ModelSessionConnection",
    items: ? Array<? {|
      __typename: string,
      id: string,
      user: ? {|
        __typename: string,
        id: string,
        sub: string,
      |},
    |} >,
    nextToken: ?string,
  |},
|};

export type OnCreateArticleSubscription = {|
  onCreateArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type OnUpdateArticleSubscription = {|
  onUpdateArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type OnDeleteArticleSubscription = {|
  onDeleteArticle: ? {|
    __typename: "Article",
    asin: string,
    browsenode: string,
  |},
|};

export type OnCreateUserSubscription = {|
  onCreateUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnUpdateUserSubscription = {|
  onUpdateUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnDeleteUserSubscription = {|
  onDeleteUser: ? {|
    __typename: "User",
    id: string,
    sub: string,
    sessions: ? {|
      __typename: string,
      items: ? Array<? {|
        __typename: string,
        id: string,
      |} >,
      nextToken: ?string,
    |},
  |},
|};

export type OnCreateSessionSubscription = {|
  onCreateSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type OnUpdateSessionSubscription = {|
  onUpdateSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};

export type OnDeleteSessionSubscription = {|
  onDeleteSession: ? {|
    __typename: "Session",
    id: string,
    user: ? {|
      __typename: string,
      id: string,
      sub: string,
      sessions: ? {|
        __typename: string,
        nextToken: ?string,
      |},
    |},
  |},
|};