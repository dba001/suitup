import React from 'react';
import renderer from 'react-test-renderer';

import { InitializeContainer } from './../Initialize/Container';

describe('Initialize Container --- Shallow Render', () => {
  let container;

  it('+++ SNAPSHOT InitializeContainer +++', () => {
    const renderedValue = renderer.create(<InitializeContainer />).toJSON();
    expect(renderedValue).toMatchSnapshot();
  });
});
