/* global window, document */
import React from 'react';
import { Dimensions, Text, View, ViewPropTypes } from 'react-native';
import { WebView as RNWebView } from 'react-native-webview';
import PropTypes from 'prop-types';

import defaultStyles from './styles';

const autoHeightScript = function() {
  setTimeout(() => {
    let height = 0;
    if (document.documentElement.clientHeight > document.body.clientHeight) {
      height = document.documentElement.clientHeight;
    } else {
      height = document.body.clientHeight;
    }
    window.ReactNativeWebView.postMessage(height);
  }, 200);
};

export class WebView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      webViewHeight: props.defaultHeight,
    };
  }

  _onMessage = e => {
    this.setState({
      webViewHeight: parseInt(e.nativeEvent.data),
    });
  };

  stopLoading() {
    this.webview.stopLoading();
  }

  renderError = () => {
    return (
      <View>
        <Text>Something did go wrong</Text>
      </View>
    );
  };

  injectingScripts = scripts => {
    const { autoHeight } = this.props;
    if (autoHeight)
      scripts
        ? (scripts = [autoHeightScript, ...scripts])
        : (scripts = undefined);
    if (scripts) {
      return (
        scripts
          .map(script => ` (${String(script)})();`)
          .reduce(
            (injectString, scriptString) => `${injectString}${scriptString}`,
            '',
          ) || ''
      );
    } else {
      return undefined;
    }
  };

  renderLoading = (renderLoading, width, height) =>
    renderLoading
      ? () => (
          <View
            style={[
              defaultStyles.loadingViewContainer,
              { width: width, height: height },
            ]}>
            {renderLoading()}
          </View>
        )
      : undefined;

  render() {
    const {
      autoHeight,
      defaultHeight,
      injectScripts,
      navigation,
      renderLoading,
      scrollEnabled,
      width,
      source,
      style,
    } = this.props;
    const webSource = source || navigation.getParam('source', {});
    const { webViewHeight } = this.state;
    const _width = width || Dimensions.get('window').width;
    const _height = autoHeight
      ? webViewHeight && !isNaN(webViewHeight)
        ? webViewHeight
        : defaultHeight
      : defaultHeight;
    const injectingScripts = this.injectingScripts(injectScripts) || '';
    const _renderLoading = this.renderLoading(renderLoading, _width, _height);
    return (
      <RNWebView
        ref={ref => {
          this.webview = ref;
        }}
        injectedJavaScript={`${injectingScripts}`}
        scrollEnabled={scrollEnabled || false}
        onMessage={this._onMessage}
        javaScriptEnabled={true}
        automaticallyAdjustContentInsets={true}
        {...this.props}
        renderError={this.renderError}
        renderLoading={_renderLoading}
        source={webSource}
        style={[style, { width: _width, height: _height }]}
      />
    );
  }
}

WebView.defaultProps = {
  autoHeight: true,
  defaultHeight: 200,
};

WebView.propTypes = {
  autoHeight: PropTypes.bool,
  injectScripts: PropTypes.arrayOf(PropTypes.func),
  navigation: PropTypes.object,
  renderLoading: PropTypes.func,
  scrollEnabled: PropTypes.bool,
  style: ViewPropTypes.style,
  source: PropTypes.shape({ uri: PropTypes.string }),
  width: PropTypes.number,
};

export default WebView;
