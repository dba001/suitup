import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  loadingViewContainer: { padding: 16 },
});

export default styles;
