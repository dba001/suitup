import React from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'components/';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';

import { translate } from 'helpers/';
import { colors } from 'theme/';
import styles from './styles';

const PopupMenuView = ({ login, changeUser }) => (
  <Menu renderer={renderers.Popover}>
    <MenuTrigger customStyles={styles}>
      <Icon type={'ion'} name={'md-more'} color={colors.highlight} size={24} />
    </MenuTrigger>
    <MenuOptions>
      <MenuOption
        onSelect={() => login()}
        text={translate('LoginWithAmazon')}
      />
      <MenuOption onSelect={() => changeUser()} text={'change User data'} />
    </MenuOptions>
  </Menu>
);

export default PopupMenuView;
