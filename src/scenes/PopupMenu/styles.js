import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  triggerOuterWrapper: {
    padding: 16,
  },
});

export default styles;
