import React from 'react';
import { connect } from 'react-redux';

import { login } from 'users/';
import { awsGetUser, awsUpdateUser } from 'aws/users';

import PopupMenuView from './View';

class PopupMenu extends React.Component {
  constructor(props) {
    super(props);
  }

  handleLogin = async () => {
    const { login } = this.props;
    await login();
  };

  onChangeData = async () => {
    const { awsUpdateUser } = this.props;
    console.log('Change user data');
    const r = await awsUpdateUser({ sub: 'try no 1' });
    console.log('RESPONSE', r);
  };

  render = () => (
    <PopupMenuView login={this.handleLogin} changeUser={this.onChangeData} />
  );
}

PopupMenu.propTypes = {};

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  login,
  awsUpdateUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopupMenu);
