import React from 'react';
import { Animated, Text, View } from 'react-native';

import { Button, Icon, Touchable } from 'components/';

import { translate } from 'helpers/';

import EmptyView from './EmptyView';
import ListView from './ListView';

import styles from './styles';

const ActiveSessionView = ({
  session,
  articles,
  filter,
  onChangeSearchText,
  onPressArrow,
  onPressArticle,
  onRefresh,
  refreshing,
  transformStyle,
}) => (
  <Animated.View style={[styles.container, transformStyle()]}>
    <Touchable onPress={onPressArrow}>
      <View style={styles.headerContainer}>
        <View style={styles.iconContainer}>
          <Icon
            name="ios-arrow-down"
            style={{
              icon: styles.arrowIcon,
              iconContainer: styles.arrowIconContainer,
            }}
          />
        </View>
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={styles.title}>
            {`${translate('CurrentSession')}`}
          </Text>
          <Text numberOfLines={1} style={styles.subtitle}>
            {`${translate('ArticlesInSession', {
              count: session.likes.length,
            })}`}
          </Text>
        </View>
      </View>
    </Touchable>
    {articles && articles.length > 0 ? (
      <ListView
        session={session}
        onChangeSearchText={onChangeSearchText}
        onPressArticle={onPressArticle}
        onRefresh={onRefresh}
        articles={articles.filter(article => {
          if (!filter) {
            return true;
          }
          if (article.title.toLowerCase().includes(filter.toLowerCase())) {
            return true;
          }
          return false;
        })}
        refreshing={refreshing}
      />
    ) : (
      <EmptyView />
    )}
    <View style={styles.buttonPositioningContainer}>
      {/*<Button*/}
      {/*raised*/}
      {/*style={{*/}
      {/*buttonContainer: styles.finishButtonContainer,*/}
      {/*buttonTitle: styles.finishButtonTitle,*/}
      {/*}}*/}
      {/*title={'Finish session'}*/}
      {/*/>*/}
    </View>
  </Animated.View>
);

export default ActiveSessionView;
