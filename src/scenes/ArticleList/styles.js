import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  arrowIcon: {
    backgroundColor: colors.transparent,
    color: colors.highlight,
    fontSize: 24,
  },
  arrowIconContainer: {
    backgroundColor: colors.contrast,
    alignItems: 'center',
    borderRadius: 16,
    height: 32,
    justifyContent: Platform.select({
      android: 'center',
      ios: 'flex-start',
    }),
    width: 32,
  },
  buttonSpacer: {
    height: 76,
  },
  collapsedContainer: {
    flex: 1,
    backgroundColor: colors.background,
  },
  container: {
    backgroundColor: colors.background,
    flex: 1,
  },
  flatListSeparator: {
    backgroundColor: colors.separator,
    height: StyleSheet.hairlineWidth,
  },
  headerContainer: {
    flexDirection: 'row',
    height: 64,
  },
  iconContainer: {
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  inputContainer: {
    alignItems: 'center',
    borderColor: colors.separator,
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    padding: 16,
  },
  textInputContainer: {
    backgroundColor: colors.input.background,
    height: 36,
  },
  textInputDivider: {
    width: 0,
  },
  textInputIcon: {
    backgroundColor: colors.transparent,
    color: colors.input.placeholderText,
  },
  subtitle: {
    backgroundColor: colors.transparent,
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
});

export default styles;
