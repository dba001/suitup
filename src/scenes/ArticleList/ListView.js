import React from 'react';
import { FlatList, RefreshControl, View } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { Icon, TextInput } from 'components/';

import { SessionListItemArticle } from 'components/';
import { translate, getId } from 'helpers/';
import { colors } from 'theme/';

import styles from './styles';

const ListView = ({
  session,
  articles,
  isEdit,
  onChangeSearchText,
  onPressArticle,
  onRefresh,
  refreshing,
}) => (
  <View style={styles.container}>
    <View style={styles.inputContainer}>
      <TextInput
        clearButton
        iconClass={IonIcon}
        iconName="ios-search"
        onChangeText={onChangeSearchText}
        placeholder={`${translate('SearchArticle')} ...`}
        style={styles}
      />
    </View>
    <FlatList
      data={articles}
      ItemSeparatorComponent={() => <View style={styles.flatListSeparator} />}
      keyExtractor={article => article.asin}
      ListFooterComponent={() => (
        <View>
          <View style={styles.flatListSeparator} />
          <View style={styles.buttonSpacer} />
        </View>
      )}
      refreshControl={
        <RefreshControl
          colors={[colors.highlight]}
          onRefresh={onRefresh}
          progressBackgroundColor={colors.background}
          refreshing={refreshing}
          size={RefreshControl.SIZE.LARGE}
        />
      }
      renderItem={({ item }) => {
        return (
          <SessionListItemArticle
            title={item.Title}
            imageSource={{
              uri: item.imagesets.primary.ThumbnailImage,
            }}
            onPress={() => onPressArticle(item.asin)}
          />
        );
      }}
    />
  </View>
);

export default ListView;
