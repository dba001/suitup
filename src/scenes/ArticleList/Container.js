import React from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';
import { connect } from 'react-redux';

import { selectArticle } from 'selectors/';

import ArticleListView from './View';
import EmptyView from './EmptyView';

class ArticleListContainer extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  handlePressChoose = () => {};

  render = () => {
    const { article } = this.props;
    if (article) {
      return <ArticleListView />;
    } else {
      return <EmptyView />;
    }
  };
}

ArticleListContainer.propTypes = {
  article: PropTypes.object.isRequired,
  handlePressChoose: PropTypes.func,
};

ArticleListContainer.defaultProps = {};

const mapStateToProps = (state, ownProps) => ({
  article: selectArticle(state, ownProps.navigation.getParam('asin', null)),
});

export default connect(mapStateToProps)(ArticleListContainer);
