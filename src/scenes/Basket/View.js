import React from 'react';
import { Text, View } from 'react-native';

import { Button, Icon, Touchable } from 'components/';

import { translate } from 'helpers/';

import { ListView } from 'components/';

import BasketListItem from './BasketListItem';
import EmptyView from './EmptyView';

import styles from './styles';

class BasketView extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      basketItems,
      basketItemCount,
      onPressBasketItem,
      filter,
      onChangeSearchText,
      onPressItem,
      renderBasketItem,
    } = this.props;
    return (
      <View style={[styles.container]}>
        {basketItemCount > 0 ? (
          <ListView
            onChangeSearchText={onChangeSearchText}
            onPressBasket={onPressItem}
            items={basketItems.filter(item => {
              if (!filter) {
                return true;
              }
              if (item.Title.toLowerCase().includes(filter.toLowerCase())) {
                return true;
              }
              return false;
            })}
            renderItem={renderBasketItem}
          />
        ) : (
          <EmptyView />
        )}
      </View>
    );
  };
}

export default BasketView;
