import React from 'react';
import { Text, View } from 'react-native';

import { Touchable } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

class AddBasketButton extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const { totalPrice, onPressCheckout, style } = this.props;
    return (
      <View
        style={[
          defaultStyle.basketInformationContainer,
          style && style.basketInformationContainer,
        ]}>
        <View
          style={[
            defaultStyle.priceInfoContainer,
            style && style.priceInfoContainer,
          ]}>
          <View
            style={[
              defaultStyle.salePriceInfoContainer,
              style && style.salePriceInfoContainer,
            ]}>
            <Text style={[defaultStyle.price, style && style.price]}>
              {salePrice || price}
            </Text>
            {salePrice ? (
              <Text style={[defaultStyle.salePrice, style && style.salePrice]}>
                {price}
              </Text>
            ) : (
              undefined
            )}
          </View>
          {availability ? (
            <Text
              style={[defaultStyle.availability, style && style.availability]}>
              {availability}
            </Text>
          ) : (
            undefined
          )}
        </View>
        <Touchable onPress={checkoutButton}>
          <View
            style={[
              defaultStyle.checkoutWithAmazonButton,
              style && style.checkoutWithAmazonButton,
            ]}
          />
        </Touchable>
      </View>
    );
  };
}

export default AddBasketButton;
