import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  basketInformationContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 64,
    paddingHorizontal: 8,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.background,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: colors.separator,
  },
  checkoutButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingHorizontal: 8,
  },
  availability: {
    backgroundColor: colors.transparent,
    color: colors.subText,
    fontSize: fontStyles.fontSize.caption,
    fontWeight: fontStyles.fontWeight.regular,
  },
  salePrice: {
    backgroundColor: colors.transparent,
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
    textDecorationLine: 'line-through',
    marginLeft: 8,
  },
  priceInfoContainer: {
    flex: 2,
    paddingHorizontal: 8,
  },
  price: {
    backgroundColor: colors.transparent,
    color: colors.highlight,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  salePriceInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
