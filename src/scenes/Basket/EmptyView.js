import React from 'react';
import { Text, View } from 'react-native';

import { Icon } from 'components/';

import { translate } from 'helpers/';

import styles from './styles';

const EmptyView = () => (
  <View style={styles.container}>
    <View style={styles.emptyViewContentContainer}>
      <Icon name="md-people" style={{ icon: styles.emptyViewIcon }} />
      <Text style={styles.emptyViewText}>
        {translate('EmptySessionPlaceholder')}
      </Text>
    </View>
    <View style={styles.buttonSpacer} />
  </View>
);

export default EmptyView;
