/* global WeakMap */

import memoize from 'lodash/memoize';

import itemAPI from 'aws/itemAPI';
import { addRemoteBasket } from 'baskets/';
import { getActiveSession, selectArticle, selectVariation } from 'selectors/';

/** STATE SELECTOR FUNCTIONS **/
memoize.Cache = WeakMap;

const getBasketItem = (state, parentAsin, variationAsin) => ({
  ...selectArticle(state, parentAsin),
  ...selectVariation(state, parentAsin, variationAsin),
});

export const getBasketItems = memoize(
  (state, basket) => {
    const items = Object.values(basket.items);
    return items.map(item => ({
      ...getBasketItem(state, item.parentAsin, item.asin),
      ...item,
    }));
  },
  (state, basket) => basket,
);

export const getBasketTotal = memoize(basketItems => {
  let currency;
  const basketTotal = basketItems.reduce((total, item) => {
    const { amount, offer } = item;
    const price = (offer.salePrice && offer.salePrice) || offer.price;
    const currencyCheck = price.CurrencyCode;

    if (!currency) {
      currency = currencyCheck;
    } else {
      if (currency !== currencyCheck)
        throw new Error(
          'getBasketTotal: Basket with different Currencies is currently not supported.',
        );
    }

    return total + amount * Number.parseInt(price.Amount);
  }, 0);
  return `${currency} ${Number(basketTotal / 100).toFixed(2)}`;
});

/** SERVER REQUEST FUNCTIONS **/
export const getRemoteBasket = basket => async (dispatch, getState) => {
  if (basket) {
    const remoteBasketData = await itemAPI.createRemoteBasket(
      Object.values(basket.items),
    );
    const remoteBasket =
      remoteBasketData && remoteBasketData._data && remoteBasketData._data.Cart;
    if (
      remoteBasket &&
      remoteBasket.Request &&
      remoteBasket.Request.IsValid === 'True'
    ) {
      const activeSession = getActiveSession(getState());
      dispatch(addRemoteBasket(activeSession.basket, remoteBasket));
      return remoteBasket;
    } else {
      console.log('pressBasket: Error --> remote basket is invalid.');
    }
  }
};
