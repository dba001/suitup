import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  basketSummaryContainer: {
    alignItems: 'center',
    backgroundColor: colors.background,
    borderTopColor: colors.separator,
    borderTopWidth: StyleSheet.hairlineWidth,
    bottom: 0,
    height: 164,
    left: 0,
    padding: 8,
    position: 'absolute',
    right: 0,
  },
  shopNowContainer: {
    height: 48,
    backgroundColor: '#F7CB53',
    padding: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  shopNow: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  infoContainer: {
    flex: 2,
  },
  totalSumContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16,
  },
  totalSum: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  totalSumPrice: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.subTitle,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  VAT: {
    backgroundColor: colors.transparent,
    color: colors.subText,
    fontSize: fontStyles.fontSize.caption,
    fontWeight: fontStyles.fontWeight.regular,
  },
});

export default styles;
