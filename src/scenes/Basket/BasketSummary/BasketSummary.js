import React from 'react';
import { Text, View, SafeAreaView } from 'react-native';

import { Touchable } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

class BasketSummary extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const { basketTotal, checkoutText, onPressCheckout, style } = this.props;
    return (
      <SafeAreaView
        style={[
          defaultStyle.basketSummaryContainer,
          style && style.basketSummaryContainer,
        ]}>
        <View
          style={[
            defaultStyle.totalSumContainer,
            style && style.totalSumContainer,
          ]}>
          <View
            style={[defaultStyle.infoContainer, style && style.infoContainer]}>
            <Text style={[defaultStyle.totalSum, style && style.totalSum]}>
              {translate('TotalSum')}
            </Text>
            <Text style={[defaultStyle.VAT, style && style.VAT]}>
              {translate('IncludingVAT')}
            </Text>
          </View>
          <Text
            style={[defaultStyle.totalSumPrice, style && style.totalSumPrice]}>
            {basketTotal}
          </Text>
        </View>
        <Touchable onPress={onPressCheckout}>
          <View
            style={[
              defaultStyle.shopNowContainer,
              style && style.shopNowContainer,
            ]}>
            <Text style={[defaultStyle.shopNow, style && style.shopNow]}>
              {checkoutText}
            </Text>
          </View>
        </Touchable>
      </SafeAreaView>
    );
  };
}

export default BasketSummary;
