import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, Text, View } from 'react-native';

import { TextInput, Touchable } from 'components/';

import { colors, fontStyles } from 'theme/';
import { translate } from 'helpers/';

import styles from './styles';

const defaultStyle = StyleSheet.create({
  container: {
    height: 128,
  },
  containerTop: {
    flex: 1,
    padding: 8,
  },
  containerCenter: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  containerBottom: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 8,
  },
  containerInfo: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
  image: {
    width: 64,
    height: 64,
  },
  merchantText: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  operationText: {
    color: colors.highlight,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  priceText: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  textInput: {
    textAlign: 'center',
    color: colors.text,
  },
  textInputContainer: {
    flex: 0,
    backgroundColor: colors.input.background,
    height: 40,
    width: 40,
  },
  textInputDivider: {
    width: 0,
  },
  textInputIcon: {
    backgroundColor: colors.transparent,
    color: colors.input.placeholderText,
  },
  title: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.subTitle,
    fontWeight: fontStyles.fontWeight.regular,
  },
});

class BasketListItem extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      amount,
      availability,
      imageSource,
      merchant,
      onChangeAmount,
      onPressRemove,
      price,
      title,
    } = this.props;
    return (
      <View style={defaultStyle.container}>
        <View style={defaultStyle.containerTop}>
          <Text style={defaultStyle.title} numberOfLines={1}>
            {title}
          </Text>
        </View>
        <View style={defaultStyle.containerCenter}>
          <Image style={defaultStyle.image} source={imageSource} />
          <View style={defaultStyle.containerInfo}>
            <Text style={defaultStyle.merchantText} numberOfLines={1}>
              {merchant}
            </Text>
            <Text style={availability.style} numberOfLines={1}>
              {availability.text}
            </Text>
          </View>
          <TextInput
            textAlign={'center'}
            onChangeText={onChangeAmount}
            style={defaultStyle}
            keyboardType={'numeric'}
            value={amount}
          />
        </View>
        <View style={defaultStyle.containerBottom}>
          <Touchable onPress={onPressRemove}>
            <Text style={defaultStyle.operationText}>
              {translate('Remove')}
            </Text>
          </Touchable>
          <Text style={defaultStyle.priceText}>{price.FormattedPrice}</Text>
        </View>
      </View>
    );
  };
}

BasketListItem.propTypes = {
  amount: PropTypes.string.isRequired,
  availability: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  imageSource: PropTypes.object.isRequired,
  merchant: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  rightIcon: PropTypes.object,
  rightSubtitle: PropTypes.string,
  subtitle: PropTypes.string,
};

export default BasketListItem;
