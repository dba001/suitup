import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  listArrowIcon: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: 24,
  },
  listImage: {
    height: 36,
    width: 36,
  },
  listImageContainer: {
    alignItems: 'center',
    backgroundColor: colors.background,
    justifyContent: 'center',
    height: 40,
    width: 40,
  },
  listItemRightSubtitle: {
    backgroundColor: colors.transparent,
    color: colors.background,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  listItemTitle: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  listPlaceholderIcon: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: 32,
  },
  listPlaceholderIconContainer: {
    alignItems: 'center',
    backgroundColor: colors.background,
    borderRadius: 20,
    justifyContent: 'center',
    height: 40,
    width: 40,
  },
});

export default styles;
