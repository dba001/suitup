import React from 'react';
import { View, Linking } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { changeAmount, removeFromBasket } from 'baskets/';
import { getActiveSessionBasket, basketItemCount } from 'selectors/';

import { translate } from 'helpers/';

import WebView from './../WebView';
import BasketView from './View';

import { getBasketItems, getBasketTotal, getRemoteBasket } from './actions';

import BasketSummary from './BasketSummary';
import BasketListItem from './BasketListItem';

import defaultStyles from './styles';

export class BasketContainer extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { filter: '' };
  }

  handlePressBasketItem = () => item => {
    console.log('PRESS BASKET ITEM');
  };

  handlePressRemoveBasketItem = item => () => {
    const { removeFromBasket } = this.props;
    if (item) {
      removeFromBasket(item.basket, item.asin);
    }
  };

  handleChangeAmountBasketItem = item => textAmount => {
    const { changeAmount } = this.props;
    if (item) {
      const amount = Number(textAmount);
      if (Number.isInteger(amount)) {
        changeAmount(item.basket, item.asin, amount);
      } else {
        console.log('Amount has to be an Integer.');
      }
    }
  };

  handleAvailability = availability => {
    if (availability) {
      const { type, text } = availability;
      if (type === 'now') {
        return {
          text,
          style: {
            fontSize: 12,
            color: 'green',
          },
        };
      } else {
        return {
          text,
          style: {
            fontSize: 12,
            color: 'red',
          },
        };
      }
    }
    return {
      text: translate('NotAvailable'),
      style: {
        fontSize: 12,
        color: 'red',
      },
    };
  };

  handlePressCheckout = async () => {
    const { basket, getRemoteBasket, navigation } = this.props;
    try {
      const remoteBasket = await getRemoteBasket(basket);
      // TODO: find a solution to use internal webview !?
      const purchaseUrl = remoteBasket.PurchaseURL;
      // const purchaseUrl = remoteBasket.PurchaseURL.replace('/gp/', '/gp/aw/');

      // ('https://www.amazon.de/gp/aw/cart/aws-merge.html?cart-id=259-7990455-5876936&associate-id=finderai-21&hmac=%2BtqnKTSg81NMt7C3ciRnnfUTaYE%3D&SubscriptionId=AKIAI7VUU3DQZW5R5W5Q&MergeCart=False');
      // const header = new Headers();
      // header.append(
      //     'User-Agent',
      //     'Mozilla/5.0 (Linux; U; Android 4.2.2; en-us; AFTB Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
      // );
      // console.log('PURCHASE URL', remoteBasket.PurchaseURL);
      // navigation.navigate('webView', {
      //     source: {
      //         uri: purchaseUrl,
      //     },
      // });
      // this.setState({ url: purchaseUrl });
      Linking.openURL(purchaseUrl);
    } catch (error) {
      console.log('Could not obtain remote basket url', error);
    }
  };

  handlePrice = offer => offer.salePrice || offer.price || '--';

  handleRenderBasketItem = ({ item }) => {
    const availability = item.offer.availability;
    const merchant = item.offer.merchant;
    return (
      <BasketListItem
        amount={item.amount.toString()}
        availability={this.handleAvailability(availability)}
        merchant={merchant || ''}
        price={this.handlePrice(item.offer)}
        title={item.Title}
        imageSource={{
          uri: item.imagesets.primary.ThumbnailImage,
        }}
        onPress={() => this.handlePressBasketItem(item.asin)}
        onPressRemove={this.handlePressRemoveBasketItem(item)}
        onChangeAmount={this.handleChangeAmountBasketItem(item)}
      />
    );
  };

  setFilter = text => {
    this.setState({ filter: text });
  };

  rewriteUrl = [
    function() {
      function rewriteFormUrl() {
        const form = document.querySelector(
          "form[action='/gp/cart/aws-merge.html']",
        );
        if (form) {
          form.action = '/gp/aw/cart/aws-merge.html';
          //form.submit();
        }
      }
      rewriteFormUrl();
    },
  ];

  render = () => {
    const { basket, basketItemCount, basketItems, basketTotal } = this.props;
    const { filter, url } = this.state;
    if (url) {
      return (
        <WebView
          source={{
            uri: url,
          }}
          autoHeight={true}
          injectScripts={this.rewriteUrl}
          startInLoadingState={true}
          showsHorizontalScrollIndicator={false}
          style={defaultStyles.webView}
        />
      );
    }
    return (
      <View style={defaultStyles.container}>
        <BasketView
          basketItems={basketItems}
          basketItemCount={basketItemCount}
          filter={filter}
          onChangeSearchText={this.setFilter}
          renderBasketItem={this.handleRenderBasketItem}
        />
        <BasketSummary
          basketTotal={basketTotal}
          checkoutText={`${translate(
            'ShopNowAtAmazon',
          )} (${basketItemCount} ${translate('Article')})`}
          onPressCheckout={this.handlePressCheckout}
        />
      </View>
    );
  };
}

const mapStateToProps = state => {
  const basket = getActiveSessionBasket(state);
  const basketItems = getBasketItems(state, basket);
  return {
    basket,
    basketItemCount: basketItemCount(state),
    basketItems: basketItems,
    basketTotal: getBasketTotal(basketItems),
  };
};

const mapDispatchToProps = {
  changeAmount,
  getRemoteBasket,
  removeFromBasket,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BasketContainer);
