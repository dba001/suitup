import React from 'react';
import { Animated, View } from 'react-native';
import { Icon, SwipeableCardStack } from 'components/';

import { translate } from 'helpers/';

import styles from './styles';

const iconSize = 32;

const SwipeView = ({
  article,
  lastArticleAdded,
  nextArticle,
  onSwipeLeft,
  onSwipeRight,
  onUndo,
}) => {
  const asin = article && article.asin;
  const nextAsin = nextArticle && nextArticle.asin;
  const title = article && article.title;
  const nextTitle = nextArticle && nextArticle.title;
  const articleImageSet =
    article && article.imagesets && article.imagesets.primary;
  const nextArticleImageSet =
    nextArticle && nextArticle.imagesets && nextArticle.imagesets.primary;
  return (
    <View style={styles.container}>
      <View style={styles.containerSwipeableCardStack}>
        <SwipeableCardStack
          style={styles}
          cardKey={asin}
          nextCardKey={nextAsin}
          cardSource={{
            uri: articleImageSet && articleImageSet.LargeImage,
          }}
          nextCardSource={{
            uri: nextArticleImageSet && nextArticleImageSet.LargeImage,
          }}
          cardTitle={title}
          nextCardTitle={nextTitle}
          onSwipeLeft={onSwipeLeft}
          onSwipeRight={onSwipeRight}
          swipeLeftOverlayText={translate('Dislike')}
          swipeRightOverlayText={translate('Like')}
        />
      </View>
      <View style={styles.containerControl}>
        <Icon
          style={{
            iconContainer: styles.iconContainerDislike,
            icon: styles.iconDislike,
          }}
          type={'fontawesome'}
          name={'times'}
          onPress={onSwipeLeft}
        />
        <Icon
          style={
            lastArticleAdded
              ? {
                  iconContainer: styles.iconContainerUndo,
                  icon: styles.iconUndo,
                }
              : {
                  iconContainer: styles.iconContainerUndo,
                  icon: styles.iconUndoInactive,
                }
          }
          type={'fontawesome'}
          name={'undo'}
          onPress={onUndo}
        />
        <Icon
          style={{
            iconContainer: styles.iconContainerLike,
            icon: styles.iconLike,
          }}
          type={'fontawesome'}
          name={'heart'}
          onPress={onSwipeRight}
        />
      </View>
    </View>
  );
};

export default SwipeView;
