import { Dimensions, Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const iconElevation = 1;
const iconSize = 32;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
  containerCard: {
    flex: 1,
    justifyContent: 'center',
  },
  containerControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  containerSwipeableCardStack: {
    flex: 3,
  },
  iconContainerLike: {
    backgroundColor: colors.background,
    marginTop: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
    borderRadius: 64,
    borderColor: '#DADADA',
    ...Platform.select({
      android: {
        elevation: iconElevation,
      },
      ios: {
        shadowOpacity: 0.45 * iconElevation + 0.18,
        shadowRadius: iconElevation,
        shadowOffset: {
          height: 0.8 * iconElevation,
        },
      },
    }),
  },
  iconContainerDislike: {
    backgroundColor: colors.background,
    marginTop: 8,
    paddingHorizontal: 18,
    paddingVertical: 14,
    borderRadius: 64,
    borderColor: '#DADADA',
    ...Platform.select({
      android: {
        elevation: iconElevation,
      },
      ios: {
        shadowOpacity: 0.45 * iconElevation + 0.18,
        shadowRadius: iconElevation,
        shadowOffset: {
          height: 0.8 * iconElevation,
        },
      },
    }),
  },
  iconContainerUndo: {
    backgroundColor: colors.background,
    margin: 24,
    alignSelf: 'center',
    paddingHorizontal: 14,
    paddingVertical: 12,
    borderRadius: 64,
    borderColor: '#DADADA',
    ...Platform.select({
      android: {
        elevation: iconElevation,
      },
      ios: {
        shadowOpacity: 0.45 * iconElevation + 0.18,
        shadowRadius: iconElevation,
        shadowOffset: {
          height: 0.8 * iconElevation,
        },
      },
    }),
  },
  iconLike: {
    fontSize: iconSize,
    fontWeight: fontStyles.fontWeight.emphasized,
    color: colors.action.like,
  },
  iconDislike: {
    fontSize: iconSize + 4,
    fontWeight: fontStyles.fontWeight.emphasized,
    color: colors.action.dislike,
  },
  iconUndo: {
    fontSize: 24,
    fontWeight: fontStyles.fontWeight.emphasized,
    color: colors.action.undo,
  },
  iconUndoInactive: {
    fontSize: 24,
    fontWeight: fontStyles.fontWeight.emphasized,
    color: colors.contrast,
  },
});

export default styles;
