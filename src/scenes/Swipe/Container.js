import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';

import {
  likeCurrentArticle,
  dislikeCurrentArticle,
  undoCurrentArticle,
} from 'articles/';
import {
  currentArticleSelector,
  nextArticleSelector,
  getLastArticleAddedSelector,
} from 'selectors/';
import { CONSTANTS } from 'constants/';

import SwipeView from './View';
import EmptyView from './EmptyView';

class SwipeContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      article,
      nextArticle,
      lastArticleAdded,
      likeCurrentArticle,
      dislikeCurrentArticle,
      undoCurrentArticle,
    } = this.props;
    if (article) {
      return (
        <SwipeView
          article={article}
          nextArticle={nextArticle}
          lastArticleAdded={lastArticleAdded}
          onSwipeLeft={dislikeCurrentArticle}
          onSwipeRight={likeCurrentArticle}
          onUndo={undoCurrentArticle}
        />
      );
    } else {
      return <View />;
    }
  };
}

SwipeContainer.propTypes = {
  article: PropTypes.object,
  nextArticle: PropTypes.object,
  lastArticleAdded: PropTypes.string,
  likeCurrentArticle: PropTypes.func.isRequired,
  dislikeCurrentArticle: PropTypes.func.isRequired,
  undoCurrentArticle: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  article: currentArticleSelector(state),
  nextArticle: nextArticleSelector(state),
  lastArticleAdded: getLastArticleAddedSelector(state),
});

const mapDispatchToProps = {
  likeCurrentArticle,
  dislikeCurrentArticle,
  undoCurrentArticle,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SwipeContainer);
