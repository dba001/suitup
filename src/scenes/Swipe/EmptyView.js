import React from 'react';
import { Text, View } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import { translate } from 'helpers/';

import styles from './styles';

const EmptyView = () => (
  <View style={styles.container}>
    <View>
      <Icon name="md-people" />
      <Text>{'Placeholder'}</Text>
    </View>
  </View>
);

export default EmptyView;
