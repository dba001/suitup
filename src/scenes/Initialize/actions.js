import { CONSTANTS } from 'constants/';
import { isFirstAppUsage } from 'selectors/';
import { setupUser } from 'scenes/SetupUser/actions';
import { fillQueues } from '../../redux/categoryQueues';

/** STATE SELECTOR FUNCTIONS **/
/** SERVER REQUEST FUNCTIONS **/
const initializeAction = () => (dispatch, getState) => {
  if (isFirstAppUsage(getState())) {
    dispatch(setupUser(CONSTANTS.AUDIENCE.men));
  }
  return Promise.resolve();
};

export default initializeAction;
