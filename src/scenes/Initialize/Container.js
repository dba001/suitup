import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './styles';
import InitializeView from './View';

export class InitializeContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render = () => <InitializeView />;
}

InitializeContainer.propTypes = {};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(InitializeContainer);
