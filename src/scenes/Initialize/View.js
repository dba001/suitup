import React from 'react';
import { Dimensions, Image, Text, View } from 'react-native';

import { translate } from 'helpers/';

import styles from './styles';

const InitializeView = props => (
  <View style={[styles.container]}>
    <Image
      resizeMode={'contain'}
      source={require('images/suitup_512x512.png')}
      style={{
        width: Dimensions.get('window').width - 8 * 8,
      }}
    />
  </View>
);

export default InitializeView;
