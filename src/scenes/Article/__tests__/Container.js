import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import ConnectedArticleContainer, { ArticleContainer } from './../Container';

describe('ArticleContainer --- Shallow Render', () => {
  const initialState = {};

  const mockStore = configureStore();
  let store, container, instance;
  const article = {
    asin: 'someAsin',
    imagesets: { primary: { ThumbnailImage: 'somePictureUrl' } },
    variations: {
      blue: {
        imagesets: {
          primary: { ThumbnailImage: 'somePictureUrl' },
        },
      },
    },
  };

  beforeEach(() => {
    store = mockStore(initialState);
    container = renderer.create(
      <ArticleContainer variationInfo={{ asins: {}, colors: {}, sizes: {} }} />,
    );
    instance = container.root.instance;
  });

  it('+++ SNAPSHOT EmptyView +++', () => {
    expect(container.toJSON()).toMatchSnapshot();
  });

  it('+++ SNAPSHOT ArticleView', () => {
    instance.setState({ color: 'blue' });
    container.update(<ArticleContainer article={article} />);
    expect(container.toJSON()).toMatchSnapshot();
  });

  it('+++ SNAPSHOT ArticleView choosing', () => {
    instance.setState({ choosing: true });
    container.update(<ArticleContainer article={{ asin: '1234' }} />);
    expect(container.toJSON()).toMatchSnapshot();
  });
});

describe('ArticleContainer --- Instance.checkVariation', () => {
  const initialState = {
    articles: {
      someAsin: {
        asin: 'someAsin',
        imagesets: {
          primary: 'someUrl',
        },
      },
    },
  };
  const variationInfo = {
    asins: {
      1234: {
        color: 'blue',
        size: 'small',
        offer: {
          availability: 'available',
          price: { FormattedPrice: '10$' },
        },
      },
      2345: { color: 'blue', size: 'large' },
      3463: { color: 'red', size: 'small' },
    },
    colors: {
      blue: {
        id: 'blue',
        sizes: { small: '1234', large: '2345' },
        imagesets: {
          primary: 'someUrl',
        },
      },
    },
    sizes: {
      small: {
        id: 'small',
        colors: { blue: '1234', red: '3463' },
      },
    },
  };

  const mockStore = configureStore();
  let store, instance;

  beforeEach(() => {
    store = mockStore(initialState);
    instance = renderer.create(
      <ArticleContainer variationInfo={variationInfo} />,
    ).root.instance;
  });

  it('+++ ArticleContainer.checkSizeAvailableForColor initial +++', () => {
    instance.setState({ size: undefined, color: undefined });
    const result = instance.checkSizeAvailableForColor('blue');
    expect(result).toEqual(true);
  });
  it('+++ ArticleContainer.checkSizeAvailableForColor initial not existing color +++', () => {
    instance.setState({ size: undefined, color: undefined });
    const result = instance.checkSizeAvailableForColor('pink');
    expect(result).toEqual(false);
  });
  it('+++ ArticleContainer.checkSizeAvailableForColor not available size +++', () => {
    instance.setState({ size: 'medium', color: undefined });
    const result = instance.checkSizeAvailableForColor('blue');
    expect(result).toEqual(false);
  });
  it('+++ ArticleContainer.checkSizeAvailableForColor available size +++', () => {
    instance.setState({ size: 'small', color: undefined });
    const result = instance.checkSizeAvailableForColor('blue');
    expect(result).toEqual(true);
  });

  it('+++ ArticleContainer.checkColorAvailableForSize initial existing size +++', () => {
    instance.setState({ size: undefined, color: undefined });
    const result = instance.checkColorAvailableForSize('small');
    expect(result).toEqual(true);
  });

  it('+++ ArticleContainer.checkColorAvailableForSize initial not exisiting size +++', () => {
    instance.setState({ size: undefined, color: undefined });
    const result = instance.checkColorAvailableForSize('medium');
    expect(result).toEqual(false);
  });

  it('+++ ArticleContainer.checkColorAvailableForSize not available color +++', () => {
    instance.setState({ size: undefined, color: 'pink' });
    const result = instance.checkColorAvailableForSize('small');
    expect(result).toEqual(false);
  });

  it('+++ ArticleContainer.checkColorAvailableForSize available color +++', () => {
    instance.setState({ size: undefined, color: 'blue' });
    const result = instance.checkColorAvailableForSize('small');
    expect(result).toEqual(true);
  });
});
