import {
  getImageSet,
  getVariantAsin,
  getVariantAsinsByColors,
  getVariantAsinsBySizes,
  getVariantInfo,
  wasLookedUp,
} from './../actions';

describe('Article actions', () => {
  const article = {
    asin: 'someAsin',
    imagesets: {
      primary: {
        ThumbnailImage: 'thumbnailImageUri',
        LargeImage: 'largeImageUri',
      },
    },
  };
  const articleWithVariation = {
    asin: 'someAsin',
    imagesets: {
      primary: {
        ThumbnailImage: 'thumbnailImageUri',
        LargeImage: 'largeImageUri',
      },
    },
    variations: {
      blue: {
        imagesets: {
          primary: {
            ThumbnailImage: 'thumbnailImageVariantPrimaryUri',
            LargeImage: 'largeImageVariantPrimaryUri',
          },
          variant: [
            {
              ThumbnailImage: 'thumbnailImageVariantUri',
              LargeImage: 'largeImageVariantUri',
            },
          ],
        },
      },
    },
  };

  it('+++ actions.getImageSet selector +++', () => {
    const result = getImageSet(undefined, undefined);
    expect(result).toEqual([]);
    const sameResult = getImageSet(undefined, undefined);
    expect(result).toBe(sameResult);
    expect(getImageSet.recomputations()).toBe(1);

    const articleResult = getImageSet(article, undefined);
    const expectedResult = [
      {
        thumbnail: { uri: 'thumbnailImageUri' },
        image: { uri: 'largeImageUri' },
      },
    ];
    expect(articleResult).toEqual(expectedResult);
    const sameArticleResult = getImageSet(article, undefined);
    expect(sameArticleResult).toBe(articleResult);
    expect(getImageSet.recomputations()).toBe(2);

    const articleColorNoVariation = getImageSet(article, 'blue');
    expect(articleColorNoVariation).toEqual(expectedResult);
    expect(getImageSet.recomputations()).toBe(3);

    const articleResultWithColor = getImageSet(articleWithVariation, 'blue');
    const expectedResultWithVariation = [
      {
        thumbnail: { uri: 'thumbnailImageUri' },
        image: { uri: 'largeImageUri' },
      },
    ];
    expect(articleResultWithColor).toEqual(expectedResultWithVariation);
    expect(getImageSet.recomputations()).toBe(4);
    const sameArticleResultWithColor = getImageSet(
      articleWithVariation,
      'blue',
    );
    expect(articleResultWithColor).toBe(sameArticleResultWithColor);
    expect(getImageSet.recomputations()).toBe(4);
  });

  /* Setter, Getter for variations */
  const variationMap = {
    asins: {
      1234: {
        color: 'blue',
        size: 'small',
        offer: { someOffer: 'andThePrice' },
        review: { IFrameURL: 'reviewURL' },
      },
      2345: {
        color: 'blue',
        size: 'large',
        offer: { someOffer: 'andThePrice' },
      },
      2131: { color: 'red', size: 'medium' },
      3463: {
        color: 'red',
        size: 'small',
      },
      15442: { color: 'red', size: 'medium' },
      15443: {
        color: 'red',
        size: 'large',
        offer: { someOffer: 'andThePrice' },
      },
    },
    colors: {
      blue: {
        id: 'blue',
        sizes: {
          small: '1234',
          large: '2345',
        },
      },
      red: {
        id: 'red',
        sizes: {
          small: '3463',
          medium: '15442',
          large: '15443',
        },
      },
    },
    sizes: {
      small: {
        id: 'small',
        colors: { blue: '1234', red: '3463' },
      },
      medium: {
        id: 'medium',
        colors: {
          blue: '2131',
          red: '15442',
        },
      },
      large: {
        id: 'large',
        colors: {
          blue: '2345',
          red: '15443',
        },
      },
    },
  };

  const setVariationMap = {
    asins: {
      1234: { color: 'blue', size: 'small' },
      2345: { color: 'blue', size: 'large' },
    },
    colors: {
      blue: {
        id: 'blue',
        sizes: {
          small: {
            asin: '1234',
          },
        },
      },
    },
    sizes: {
      small: {
        id: 'small',
        colors: { blue: '1234', red: '3463' },
      },
    },
  };

  const infos = {
    1234: { offer: { someOffer: 'value' }, review: { HasReviews: true } },
    3463: { offer: { someOffer: 'value' }, review: { HasReviews: true } },
  };

  it('+++ actions.getVariantAsin +++', () => {
    let result = getVariantAsin();
    expect(result).toEqual(undefined);
    result = getVariantAsin(variationMap, undefined, undefined);
    expect(result).toEqual(undefined);
    result = getVariantAsin(variationMap, 'blue', undefined);
    expect(result).toEqual(undefined);
    result = getVariantAsin(variationMap, undefined, 'small');
    expect(result).toEqual(undefined);
    result = getVariantAsin(variationMap, 'blue', 'small');
    expect(result).toEqual('1234');
    result = getVariantAsin(variationMap, 'aasd', 'small');
    expect(result).toEqual(undefined);
    result = getVariantAsin(variationMap, 'blue', 'sadasf');
    expect(result).toEqual(undefined);
  });

  it('+++ actions.getVariantAsinsByColors +++', () => {
    let result = getVariantAsinsByColors();
    expect(result).toEqual(undefined);
    result = getVariantAsinsByColors(variationMap, undefined);
    expect(result).toEqual(undefined);
    result = getVariantAsinsByColors(variationMap, 'nonsense');
    expect(result).toEqual(undefined);
    result = getVariantAsinsByColors(variationMap, 'blue');
    expect(result).toEqual(['1234', '2345']);
  });

  it('+++ actions.getVariantAsinsBySizes +++', () => {
    let result = getVariantAsinsBySizes();
    expect(result).toEqual(undefined);
    result = getVariantAsinsBySizes(variationMap, undefined);
    expect(result).toEqual(undefined);
    result = getVariantAsinsBySizes(variationMap, 'nonsense');
    expect(result).toEqual(undefined);
    result = getVariantAsinsBySizes(variationMap, 'small');
    expect(result).toEqual(['1234', '3463']);
  });

  it('+++ actions.getVariantInfo +++', () => {
    let result = getVariantInfo();
    expect(result).toEqual({
      variantAsin: undefined,
      offer: undefined,
      review: undefined,
    });
    result = getVariantInfo(variationMap, undefined, undefined);
    expect(result).toEqual({
      variantAsin: undefined,
      offer: undefined,
      review: undefined,
    });
    result = getVariantInfo(variationMap, 'blue', undefined);
    expect(result).toEqual({
      variantAsin: undefined,
      offer: undefined,
      review: undefined,
    });
    result = getVariantInfo(variationMap, undefined, 'small');
    expect(result).toEqual({
      variantAsin: undefined,
      offer: undefined,
      review: undefined,
    });
    result = getVariantInfo({ ...variationMap }, 'blue', 'small');
    expect(result).toEqual({
      variantAsin: '1234',
      offer: { someOffer: 'andThePrice' },
    });
    result = getVariantInfo(variationMap, 'aasd', 'small');
    expect(result).toEqual({ variantAsin: undefined, offer: undefined });

    result = getVariantInfo(variationMap, 'blue', 'sadasf');
    expect(result).toEqual({ variantAsin: undefined, offer: undefined });
  });

  it('+++ actions.wasLookedUp +++', () => {
    let result = wasLookedUp();
    expect(result).toEqual(false);
    result = wasLookedUp(variationMap, undefined);
    expect(result).toEqual(false);
    result = wasLookedUp(variationMap, ['abcde']);
    expect(result).toEqual(false);
    result = wasLookedUp(variationMap, ['1234', 'abcde']);
    expect(result).toEqual(false);
    result = wasLookedUp(variationMap, ['1234']);
    expect(result).toEqual(true);
    result = wasLookedUp(variationMap, ['1234', '2345', '15443']);
    expect(result).toEqual(true);
  });
});
