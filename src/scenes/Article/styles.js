import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const elevation = 4;

const styles = StyleSheet.create({
  articleBrandText: {
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.regular,
  },
  articleTitleText: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.regular,
  },
  articleReviewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 8,
  },
  articleReviewText: {
    color: colors.highlight,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.regular,
  },
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
  imageViewerContainer: {
    flex: 1,
    minHeight: 250,
    paddingLeft: 8,
    paddingRight: 8,
  },
  flatListSeparator: {
    backgroundColor: colors.separator,
    height: StyleSheet.hairlineWidth,
  },
  listItemSubtitle: {
    color: '#b12703',
  },
  selectArticleVariationContainer: {
    flex: 1,
    backgroundColor: colors.contrast,
    paddingHorizontal: 16,
  },
  selectArticleVariationTitle: {
    color: colors.text,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.emphasized,
    paddingVertical: 8,
  },
  selectionList: {
    elevation: 24,
  },
  selectionListItemContainer: {
    backgroundColor: colors.background,
    borderColor: colors.separator,
  },
  selectionListItemIcon: {
    color: colors.text,
  },
  selectionListItemIconSelected: {
    color: colors.highlight,
  },
  textLoadingContainer: {
    width: 100,
    height: 100,
  },
  topContainer: {
    flex: 1,
    backgroundColor: colors.background,
    ...Platform.select({
      android: {
        elevation: elevation,
      },
      ios: {
        shadowOpacity: 0.0015 * elevation + 0.18,
        shadowRadius: 0.54 * elevation,
        shadowOffset: {
          height: 0.6 * elevation,
        },
      },
    }),
  },
  topContainerTouchable: {
    backgroundColor: colors.contrast,
    padding: 8,
  },
  webView: {
    marginTop: 8,
  },
});

export default styles;
