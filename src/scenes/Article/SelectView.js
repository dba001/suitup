import React from 'react';
import { FlatList, RefreshControl, View, Text } from 'react-native';

import { SelectionListItem } from 'components/';
import styles from './styles';

class SelectView extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const { items, selectValue, title, onPressItem } = this.props;
    return (
      <View style={styles.selectViewContainer}>
        <Text style={styles.selectViewTitle}>{title}</Text>
        <FlatList
          style={styles.selectionList}
          data={items}
          ItemSeparatorComponent={() => (
            <View style={styles.flatListSeparator} />
          )}
          keyExtractor={item => item.value}
          renderItem={({ item }) => {
            return (
              <SelectionListItem
                title={item.value}
                subtitle={'EUR 2.68'}
                onPress={onPressItem(item)}
                selected={selectValue === item.value ? true : false}
                style={styles}
              />
            );
          }}
        />
      </View>
    );
  };
}

export default SelectView;
