import mapValues from 'lodash/mapValues';
import memoize from 'lodash/memoize';
import pickBy from 'lodash/pickBy';
import { createSelector } from 'reselect';
import { selectArticle, getActiveSessionBasketId } from 'selectors/';

import { addToBasket } from 'baskets/';

export const createVariationMap = memoize(variations => {
  if (!variations) {
    return { colors: [], sizes: [] };
  }

  let variationSizes = {};
  mapValues(variations, (variation, color) => {
    mapValues(variation.sizes, (asin, size) => {
      if (variationSizes[size]) {
        variationSizes[size].colors[color] = { asin };
      } else {
        variationSizes[size] = {
          id: size,
          colors: { [color]: { asin } },
        };
      }
    });
  });
  let variationColors = {};
  mapValues(variations, (variation, color) => {
    variationColors[color] = {
      ...variation,
      id: color,
      sizes: mapValues(variation.sizes, asin => ({
        asin,
      })),
    };
  });
  return { colors: variationColors, sizes: variationSizes };
});

export const addArticleToBasket = (parentAsin, articleInfo) => (
  dispatch,
  getState,
) => {
  const asin = articleInfo.variantAsin;
  if (!asin) {
    console.log('addArticleToBasket: No asin given');
    return;
  }
  const basketId = getActiveSessionBasketId(getState());
  if (!basketId) {
    console.log('addArticleToBasket: No active basket');
    return;
  }
  dispatch(addToBasket(basketId, parentAsin, asin));
};

export const getArticle = (state, props) =>
  selectArticle(state, props.navigation.getParam('asin', null));

const extractImage = imageSet => ({
  thumbnail: { uri: imageSet.ThumbnailImage },
  image: { uri: imageSet.LargeImage },
});

export const getImageSet = createSelector(
  article => article,
  (article, variationInfo) => variationInfo,
  (article, variationInfo, color) => color,
  (article, variationInfo, color) => {
    if (!article && !variationInfo) {
      return [];
    }
    let primary;
    let variants;
    if (color) {
      if (variationInfo?.colors?.[color]) {
        primary = variationInfo.colors[color]?.imagesets.primary;
        variants = variationInfo.colors[color]?.imagesets.variant || [];
      }
    }
    if (!primary) {
      if (article?.imagesets?.primary) {
        primary = article.imagesets.primary;
        variants = article.imagesets.variant || [];
      } else {
        return [];
      }
    }

    return [
      extractImage(primary),
      ...variants.map(variant => extractImage(variant)),
    ];
  },
);

export const getReviewUrl = article => article?.review;

export const getVariantAsin = (variationMap, color, size) => {
  if (variationMap && color && size) {
    return variationMap.colors[color] && variationMap.colors[color].sizes[size];
  }
  if (variationMap?.colors?.[color]?.asin) {
    return variationMap.colors[color].asin;
  }
  return undefined;
};

export const getVariantAsinsByColors = (variationInfo, color) => {
  if (variationInfo) {
    if (color) {
      if (variationInfo?.colors?.[color]?.sizes) {
        const sizes = variationInfo.colors[color].sizes;
        return Object.keys(sizes).reduce((asins, size) => {
          asins.push(sizes[size]);
          return asins;
        }, []);
      }
    } else {
      // is this a variation without sizes?
      if (!variationInfo?.sizes) {
        return Object.keys(variationInfo.asins);
      }
    }
  }
  return undefined;
};
export const getVariantAsinsBySizes = (variationInfo, size) => {
  if (variationInfo && size) {
    if (variationInfo?.sizes?.[size]?.colors) {
      const colors = variationInfo.sizes[size].colors;
      return Object.keys(colors).reduce((asins, color) => {
        asins.push(colors[color]);
        return asins;
      }, []);
    }
  }
  return undefined;
};

export const getVariantInfo = memoize(
  (variationInfo, color, size) => {
    const variantAsin = getVariantAsin(variationInfo, color, size);
    const info = variationInfo?.asins?.[variantAsin];
    let offer;
    if (info) {
      offer = info.offer;
    }
    return {
      variantAsin,
      offer,
    };
  },
  (...args) => JSON.stringify(args),
);

export const wasLookedUp = (variationMap, asins) => {
  if (variationMap && asins) {
    return asins?.every(asin => !!variationMap?.asins?.[asin]?.offer);
  }
  return false;
};

export const getColors = variationInfo => {
  if (variationInfo && variationInfo.colors) {
    return Object.values(variationInfo.colors);
  }
  return undefined;
};

export const getSizes = variationInfo => {
  if (variationInfo && variationInfo.sizes) {
    return Object.values(variationInfo.sizes);
  }
  return undefined;
};

export const getAvailableColors = (size, variationInfo) => {
  let colors;
  if (variationInfo?.sizes) {
    colors = variationInfo?.sizes?.[size]?.colors;
  } else {
    colors = mapValues(variationInfo?.colors, variation => variation.asin);
  }
  if (colors) {
    return pickBy(colors, variationAsin => {
      const offer = selectOffer(variationInfo, variationAsin);
      return isValidOffer(offer);
    });
  }
  return undefined;
};

export const getAvailableSizes = (color, variationInfo) => {
  return (
    variationInfo &&
    variationInfo.sizes &&
    variationInfo.colors[color] &&
    variationInfo.colors[color].sizes
  );
};

export const selectOffer = (variationInfo, variationAsin) => {
  const variation = variationInfo.asins[variationAsin];
  if (variation) {
    return variation.offer;
  }
  return undefined;
};

export const isValidOffer = offer =>
  !!(offer && offer.availability && offer.price && offer.price.FormattedPrice);
