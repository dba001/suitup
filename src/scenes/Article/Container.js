import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Logger } from '@aws-amplify/core';

import itemAPI from 'aws/itemAPI';
import { translate } from 'helpers/';

import {
  addArticleToBasket,
  getArticle,
  getAvailableColors,
  getAvailableSizes,
  getColors,
  getImageSet,
  getReviewUrl,
  getSizes,
  getVariantInfo,
  selectOffer,
  isValidOffer,
} from './actions';

import { TextLoading } from 'components/';
import {
  basketItemCount as getBasketItemCount,
  selectVariationInfo,
} from 'selectors/';
import { setVariationInfo } from 'variationInfo';

import ArticleView from './View';
import EmptyView from './EmptyView';

import AddBasketButton from './components/AddBasketButton';
import SelectArticleVariationView from './components/SelectArticleVariation';
import WebView from './../WebView';

const logger = new Logger('ArticleContainer', 'INFO');

import defaultStyles from './styles';

export class ArticleContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      color: undefined,
      loading: false,
      size: undefined,
      showSelectArticleVariation: false,
    };
  }

  componentDidMount = async () => {
    this.getInfoForArticle();
  };

  removeLogo = [
    function () {
      function removeAmazonLogo() {
        let iFrame = document.getElementsByClassName('crIFrame');
        if (iFrame.length === 1) {
          iFrame = iFrame[0];
          let logo = document.getElementsByClassName('crIFrameLogo');
          if (logo.length === 1) {
            logo = logo[0];
          }
          if (iFrame && logo) {
            iFrame.removeChild(logo);
          }
        }
      }
      removeAmazonLogo();
    },
  ];

  handlePressAddToBasket = async () => {
    const {
      addArticleToBasket,
      article,
      navigation,
      variationInfo,
    } = this.props;
    const { color, size } = this.state;

    const variantInfo = getVariantInfo(variationInfo, color, size);
    if (variantInfo) {
      addArticleToBasket(article.asin, variantInfo);
      navigation.goBack();
    }
  };

  handlePressChooseColor = () => {
    const { sizes } = this.props;
    // when there are no sizes this has to be a color only article
    if (!sizes) {
      this.getInfoForArticle();
    }
    this.setState({ choosing: true });
  };

  handlePressDone = () => {
    this.setState({ choosing: false });
  };

  getInfoForArticle = async () => {
    const { article, setVariationInfo } = this.props;

    this.setState({ loading: true });
    const info = await itemAPI.lookupItemInfo(article.asin);
    setVariationInfo(article.asin, info.variationInfo);
    this.setState({
      loading: false,
    });
  };

  checkSizeAvailableForColor = color => {
    const { variationInfo } = this.props;
    const { size } = this.state;

    if (!variationInfo.colors[color]) return false;
    if (!size) return true;

    const variationAsin = variationInfo.colors[color].sizes[size];
    if (variationAsin) {
      const offer = selectOffer(variationInfo, variationAsin);
      return isValidOffer(offer);
    }
    return false;
  };

  checkColorAvailableForSize = size => {
    const { variationInfo } = this.props;
    const { color } = this.state;

    if (!variationInfo.sizes[size]) return false;
    if (!color) return true;

    return !!variationInfo.sizes[size].colors[color];
  };

  handlePressColorTile = item => () => {
    const { variationInfo } = this.props;
    let _state;
    const color = item.id;
    const isSizeAvailable = this.checkSizeAvailableForColor(color);
    if (isSizeAvailable) {
      _state = {
        color: color,
      };
      this.setState(_state);
    } else {
      const sizes = Object.keys(variationInfo.colors[color].sizes);
      if (sizes && sizes.length > 0) {
        _state = {
          color: color,
          size: sizes[0],
        };
        this.setState(_state);
      } else {
        _state = {
          color: color,
          size: undefined,
        };
        this.setState(_state);
      }
    }
  };

  handlePressSizeTile = item => () => {
    const { variationInfo } = this.props;
    let _state;
    const size = item.id;
    const isColorAvailable = this.checkColorAvailableForSize(size);
    if (isColorAvailable)
      _state = {
        size: size,
      };
    else {
      const colors = Object.keys(variationInfo.sizes[size].colors);
      if (colors && colors.length > 0)
        _state = {
          color: colors[0],
          size: size,
        };
      else
        _state = {
          color: undefined,
          size: size,
        };
    }
    this.setState(_state);
  };

  renderWebViewLoading = () => {
    return <TextLoading withImage={true} imageSize={12} rows={5} />;
  };

  render = () => {
    const {
      article,
      basketItemCount,
      colors,
      review,
      variationInfo,
      sizes,
    } = this.props;
    const { choosing, color, loading, size } = this.state;
    const imageSet = getImageSet(article, variationInfo, color);
    const { variantAsin, offer } = getVariantInfo(variationInfo, color, size);
    const availableColors = getAvailableColors(size, variationInfo);
    const availableSizes = getAvailableSizes(color, variationInfo);
    if (article) {
      return (
        <View style={defaultStyles.container}>
          <ArticleView
            article={article}
            imageSet={imageSet}
            onPressChooseColor={this.handlePressChooseColor}
            color={color}>
            <WebView
              source={{
                uri: review,
              }}
              autoHeight={true}
              injectScripts={this.removeLogo}
              renderLoading={this.renderWebViewLoading}
              startInLoadingState={true}
              showsHorizontalScrollIndicator={false}
              style={defaultStyles.webView}
            />
            {choosing ? (
              <SelectArticleVariationView
                colors={colors}
                sizes={sizes}
                availableColors={availableColors}
                availableSizes={availableSizes}
                article={article}
                color={color}
                loading={loading}
                onPressDone={this.handlePressDone}
                onPressColorTile={this.handlePressColorTile}
                onPressSizeTile={this.handlePressSizeTile}
                size={size}
                variationInfo={variationInfo}
                visible={choosing}
              />
            ) : (
                undefined
              )}
          </ArticleView>
          {variantAsin && offer ? (
            <AddBasketButton
              basketItemCount={basketItemCount}
              onPress={this.handlePressAddToBasket}
              price={offer.price && offer.price.FormattedPrice}
              salePrice={offer.salePrice && offer.salePrice.FormattedPrice}
              availability={offer.availability && offer.availability.text}
              IsEligibleForPrime={offer.IsEligibleForPrime}
            />
          ) : (
              undefined
            )}
        </View>
      );
    } else {
      return <EmptyView />;
    }
  };
}

ArticleContainer.defaultProps = {};

ArticleContainer.propTypes = {
  variationInfo: PropTypes.shape({
    asins: PropTypes.object.isRequired,
    colors: PropTypes.object.isRequired,
    sizes: PropTypes.object,
  }),
  article: PropTypes.object,
  basketItemCount: PropTypes.number,
  setVariationInfo: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
  const article = getArticle(state, ownProps);
  const review = getReviewUrl(article);
  const variationInfo = selectVariationInfo(state, article ?.asin);
  const colors = getColors(variationInfo);
  const sizes = getSizes(variationInfo);
  const basketItemCount = getBasketItemCount(state);
  return {
    article,
    colors,
    variationInfo,
    review,
    sizes,
    basketItemCount,
  };
};

const mapDispatchToProps = {
  addArticleToBasket,
  setVariationInfo,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ArticleContainer);
