import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme';

const styles = StyleSheet.create({
  actionSheetContainer: {
    backgroundColor: colors.backgroundColor,
    borderRadius: 8,
  },
  cancelActionContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: colors.transparent,
    marginHorizontal: 8,
  },
  cancelActionLabel: {
    backgroundColor: colors.transparent,
    color: colors.highlight,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    flex: 1,
    justifyContent: 'flex-end',
  },
});

export default styles;
