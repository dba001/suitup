import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Text, View } from 'react-native';

import { Touchable } from 'components/';

import { translate } from 'helpers/';

import styles from './styles';

const ActionSheet = ({ children, onRequestClose, labelClose, visible }) => {
  return (
    <Modal
      animationType={'slide'}
      onRequestClose={onRequestClose}
      transparent
      visible={visible}>
      <View style={styles.container}>
        <View style={styles.cancelActionContainer}>
          <Touchable onPress={onRequestClose}>
            <Text numberOfLines={1} style={styles.cancelActionLabel}>
              {labelClose || translate('Done')}
            </Text>
          </Touchable>
        </View>
        <View style={styles.actionSheetContainer}>{children}</View>
      </View>
    </Modal>
  );
};

ActionSheet.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  onRequestClose: PropTypes.func.isRequired,
  labelClose: PropTypes.string,
  visible: PropTypes.bool.isRequired,
};

export default ActionSheet;
