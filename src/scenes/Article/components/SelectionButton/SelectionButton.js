import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { FlexImage, Icon } from 'components/';

import defaultStyle from './styles';

class SelectionButton extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      disabled,
      title,
      subTitle,
      icon,
      image,
      onPress,
      style,
    } = this.props;
    return (
      <TouchableOpacity
        style={[
          defaultStyle.selectionButtonContainer,
          style && style.selectionButtonContainer,
        ]}
        onPress={onPress}
        disabled={disabled}>
        <View
          style={[
            defaultStyle.selectionButtonTitleContainer,
            style && style.selectionButtonTitleContainer,
          ]}>
          <Text
            style={[
              defaultStyle.selectionButtonTitleText,
              style && style.selectionButtonTitleText,
              disabled && defaultStyle.selectionButtonDisabledTitleText,
              disabled && style && style.selectionButtonDisabledTitleText,
            ]}>{`${title}`}</Text>
          <Text
            style={[
              defaultStyle.selectionButtonSubTitleText,
              style && style.selectionButtonSubTitleText,
              disabled && defaultStyle.selectionButtonDisabledSubTitleText,
              disabled && style && style.selectionDisabledButtonSubTitleText,
            ]}>{`${subTitle}`}</Text>
        </View>
        <View
          style={[
            defaultStyle.selectionButtonIconContainer,
            style && style.selectionButtonIconContainer,
          ]}>
          {image ? (
            <View
              style={[
                defaultStyle.selectionButtonImageContainer,
                style && style.selectionButtonImageContainer,
              ]}>
              <FlexImage resizeMode={'contain'} source={image} />
            </View>
          ) : null}
          <Icon
            name="ios-arrow-forward"
            style={
              style && style.arrowIcon
                ? {
                    icon: disabled ? style.arrowIconDisabled : style.arrowIcon,
                  }
                : {
                    icon: disabled
                      ? defaultStyle.arrowIconDisabled
                      : defaultStyle.arrowIcon,
                  }
            }
          />
        </View>
      </TouchableOpacity>
    );
  };
}

export default SelectionButton;
