import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const disabled = '#808080';

const selectionButtonTitleText = {
  color: colors.text,
  fontSize: fontStyles.fontSize.bodySmall,
  fontWeight: fontStyles.fontWeight.regular,
};

const selectionButtonSubTitleText = {
  color: colors.text,
  fontSize: fontStyles.fontSize.body,
  fontWeight: fontStyles.fontWeight.emphasized,
};

const arrowIcon = {
  backgroundColor: colors.transparent,
  color: colors.highlight,
  fontSize: 16,
};

const styles = StyleSheet.create({
  arrowIcon: {
    ...arrowIcon,
  },
  arrowIconDisabled: {
    ...arrowIcon,
    color: disabled,
  },
  selectionButtonIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  selectionButtonImageContainer: {
    width: 40,
    height: 40,
    backgroundColor: colors.background,
    borderColor: colors.highlight,
    borderWidth: StyleSheet.hairlineWidth,
    marginHorizontal: 16,
  },
  selectionButtonContainer: {
    flexDirection: 'row',
    backgroundColor: colors.contrast,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.text,
    padding: 8,
    marginHorizontal: 16,
    marginTop: 16,
  },
  selectionButtonTitleContainer: {},
  selectionButtonTitleText: {
    ...selectionButtonTitleText,
  },
  selectionButtonSubTitleText: {
    ...selectionButtonSubTitleText,
  },
  selectionButtonDisabledTitleText: {
    ...selectionButtonTitleText,
    color: disabled,
  },
  selectionButtonDisabledSubTitleText: {
    ...selectionButtonSubTitleText,
    color: disabled,
  },
});

export default styles;
