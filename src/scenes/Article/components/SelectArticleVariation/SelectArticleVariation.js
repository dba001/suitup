import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import { isValidOffer, selectOffer } from '../../actions';

import ActionSheet from './../ActionSheet';
import ArticleVariationBar from './ArticleVariationBar';
import ArticleSizeBar from './ArticleSizeBar';

import { translate } from 'helpers/';

import styles from './styles';

class SelectArticleVariation extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      article,
      color,
      loading,
      onPressDone,
      onPressSizeTile,
      onPressColorTile,
      size,
      variationInfo,
      visible,
      colors,
      sizes,
      availableColors,
      availableSizes,
    } = this.props;

    return (
      <ActionSheet
        onRequestClose={onPressDone}
        labelClose={translate('Done')}
        visible={visible}>
        <View style={styles.container}>
          {sizes ? (
            <React.Fragment>
              <Text style={styles.title}>{`${translate('Size')}: ${
                size ? size : ''
              }`}</Text>
              <ArticleSizeBar
                horizontal={true}
                onPressTile={onPressSizeTile}
                availableSizes={availableSizes}
                selectedSize={size}
                sizes={sizes}
                showsHorizontalScrollIndicator={false}
              />
            </React.Fragment>
          ) : (
            undefined
          )}
          <Text style={styles.title}>{`${translate('Color')}: ${
            color ? color : ''
          }`}</Text>
          <ArticleVariationBar
            horizontal={true}
            loading={loading}
            onPressTile={onPressColorTile}
            availableVariations={availableColors}
            selectedSize={size}
            selectedVariation={color}
            title={article.Title}
            variations={colors}
            variationInfo={variationInfo}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </ActionSheet>
    );
  };
}

SelectArticleVariation.propTypes = {
  article: PropTypes.object.isRequired,
  onPressDone: PropTypes.func.isRequired,
  onPressSizeTile: PropTypes.func.isRequired,
  onPressColorTile: PropTypes.func.isRequired,
  variationInfo: PropTypes.shape({
    colors: PropTypes.object,
    sizes: PropTypes.object,
  }),
  color: PropTypes.string,
  loading: PropTypes.bool,
  size: PropTypes.string,
  visible: PropTypes.bool,
};

export default SelectArticleVariation;
