import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Text,
  TouchableOpacity,
  ViewPropTypes,
  View,
} from 'react-native';

import { TextLoading, Touchable } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

class ArticleTile extends React.PureComponent {
  renderShipping = availability => {
    let text;
    if (availability) {
      if (availability.type === 'now') {
        text = (
          <Text style={defaultStyle.positiveHighlightText}>
            {translate('InStock')}
          </Text>
        );
      } else {
        text = (
          <Text style={defaultStyle.attributeLabel}>{availability.type}</Text>
        );
      }
    } else {
      text = (
        <Text style={defaultStyle.attributeLabel}>{'Not available.'}</Text>
      );
    }
    return <View style={defaultStyle.attributeContainer}>{text}</View>;
  };

  render = () => {
    const {
      available,
      image,
      isPrimeEligible,
      loading,
      message,
      onPress,
      price,
      selected,
      shipping,
      style,
      title,
    } = this.props;
    return (
      <Touchable onPress={onPress}>
        <View style={[defaultStyle.container, style && style.container]}>
          <View
            style={
              selected
                ? [defaultStyle.tileSelected, style && style.tileSelected]
                : available
                ? [defaultStyle.tileAvailable, style && style.tileAvailable]
                : [defaultStyle.tile, style && style.tile]
            }>
            <Image
              resizeMode={'contain'}
              source={image}
              style={[defaultStyle.image, style && style.image]}
            />
            <Text
              numberOfLines={1}
              style={[
                defaultStyle.title,
                style && style.title,
              ]}>{`${title}`}</Text>
            {loading ? (
              <TextLoading rows={3} />
            ) : (
              <View>
                {message ? (
                  <Text
                    numberOfLines={2}
                    style={[
                      defaultStyle.message,
                      style && style.message,
                    ]}>{`${message}`}</Text>
                ) : price ? (
                  <View style={defaultStyle.offerContainer}>
                    <Text
                      numberOfLines={1}
                      style={[
                        defaultStyle.price,
                        style && style.price,
                      ]}>{`${price}`}</Text>
                    {isPrimeEligible ? (
                      <Image
                        resizeMode={'contain'}
                        source={require('images/checkPrime.png')}
                        style={defaultStyle.checkPrimeImage}
                      />
                    ) : (
                      <View style={defaultStyle.checkPrimeImgae} />
                    )}
                    {this.renderShipping(shipping)}
                  </View>
                ) : (
                  undefined
                )}
              </View>
            )}
          </View>
        </View>
      </Touchable>
    );
  };
}

ArticleTile.propTypes = {
  image: PropTypes.object.isRequired,
  selected: PropTypes.bool.isRequired,
  available: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  style: ViewPropTypes.style,
  title: PropTypes.string,
};

export default ArticleTile;
