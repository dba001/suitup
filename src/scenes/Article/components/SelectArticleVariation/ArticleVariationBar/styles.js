import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const width = 128;
const padding = 8;
const disabledOpacity = 0.5;

const tile = {
  width: width,
  height: 192,
  padding: padding,
  backgroundColor: colors.transparent,
  borderWidth: StyleSheet.hairlineWidth,
  borderColor: colors.subText,
};

const styles = StyleSheet.create({
  attributeLabel: {
    color: '#c45500',
    fontSize: fontStyles.fontSize.caption,
    fontWeight: fontStyles.fontWeight.regular,
    marginRight: 8,
  },
  checkPrimeImage: {
    width: 32,
    height: 12,
  },
  container: {
    padding: 4,
  },
  image: {
    width: width - 2 * padding,
    height: 96,
  },
  message: {
    marginTop: 8,
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
  offerContainer: {
    marginTop: 8,
  },
  positiveHighlightText: {
    color: '#008a00',
    fontSize: fontStyles.fontSize.caption,
    fontWeight: fontStyles.fontWeight.regular,
  },
  price: {
    color: colors.text,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
  tile: {
    ...tile,
    opacity: disabledOpacity,
  },
  tileAvailable: {
    ...tile,
    borderWidth: 1,
  },
  tileSelected: {
    ...tile,
    borderWidth: 1,
    borderColor: colors.highlight,
  },
  tileImage: {
    padding: 8,
  },
  title: {
    marginTop: 8,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  titleSelected: {
    marginTop: 8,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
});

export default styles;
