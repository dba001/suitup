import isEmpty from 'lodash/isEmpty';

import React from 'react';
import PropTypes from 'prop-types';
import { FlatList, View, Text } from 'react-native';

import { translate } from 'helpers/';
import { getVariationImage, selectOffer } from './actions';

import ArticleTile from './ArticleTile';

import defaultStyle from './styles';

class ArticleVariationBar extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  isSelectedAvailable = variationId => {
    const { selectedVariation, selectedSize, variationInfo } = this.props;

    const isSelected = this.isSelected(variationId);
    if (isSelected) {
      return (
        selectedVariation &&
        selectedSize &&
        !!variationInfo.colors[variationId].sizes[selectedSize]
      );
    }
    return false;
  };

  isAvailableWithOffer = currentVariation => {
    const { availableVariations } = this.props;
    return !!(availableVariations && availableVariations[currentVariation]);
  };

  isSelected = currentVariation => {
    const { selectedVariation } = this.props;
    return selectedVariation === currentVariation;
  };

  render = () => {
    const {
      horizontal,
      loading,
      onPressTile,
      selectedSize,
      selectedVariation,
      style,
      title,
      variations,
      variationInfo,
      ...props
    } = this.props;
    return (
      <FlatList
        data={variations}
        horizontal={horizontal}
        extraData={selectedVariation}
        keyExtractor={(item, i) => `ArticleVariationBar_${item.id}_${i}`}
        renderItem={({ item }) => {
          let formattedPrice, message, isPrimeEligible, shipping;
          const image = getVariationImage(item, 'LargeImage');

          if (!image) return;

          const availableWithOffer = this.isAvailableWithOffer(item.id);

          let offer;
          const hasSizes = !!item.sizes;
          if (hasSizes) {
            if (selectedSize) {
              if (availableWithOffer) {
                offer = selectOffer(variationInfo, item.sizes[selectedSize]);
              } else {
                if (this.isSelectedAvailable(item.id)) {
                  message = translate('NoOfferAvailable');
                } else {
                  message = translate('SelectToSeeSizes');
                }
              }
            } else {
              message = translate('SelectSizeToSeePrice');
            }
          } else {
            offer = selectOffer(variationInfo, item.asin);
          }
          if (offer) {
            // this is checked by isAvailable()
            formattedPrice = offer.price.FormattedPrice;
            isPrimeEligible = offer.IsEligibleForPrime === '1';
            shipping = offer.availability;
          }

          return (
            <ArticleTile
              available={availableWithOffer}
              image={image}
              isPrimeEligible={isPrimeEligible}
              loading={loading}
              message={message}
              onPress={onPressTile(item)}
              price={formattedPrice}
              selected={this.isSelected(item.id)}
              shipping={shipping}
              style={style}
              title={title}
            />
          );
        }}
        {...props}
      />
    );
  };
}

ArticleVariationBar.propTypes = {
  variations: PropTypes.array,
  availableVariations: PropTypes.object,
  horizontal: PropTypes.bool,
  onPressTile: PropTypes.func,
  selectedVariation: PropTypes.string,
  title: PropTypes.string,
  style: PropTypes.object,
  showsHorizontalScrollIndicator: PropTypes.bool,
};

ArticleVariationBar.defaultProps = {
  horizontal: false,
};

export default ArticleVariationBar;
