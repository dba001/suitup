import memoize from 'lodash/memoize';

export { selectOffer } from '../../../actions';

export const getVariationImage = memoize(
  (variation, imageName = 'ThumbnailImage') => {
    let source;
    if (variation && variation.imagesets) {
      const imagesets = variation.imagesets;
      if (imagesets.primary && imagesets.primary[imageName]) {
        source = { uri: imagesets.primary[imageName] };
      } else {
        const variant = imagesets.variant;
        if (variant && variant.length > 0) {
          source = { uri: variant[0][imageName] };
        }
      }
    }
    return source;
  },
);
