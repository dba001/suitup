import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const disabled = '#808080';

const styles = StyleSheet.create({
  container: {
    padding: 8,
    paddingVertical: 32,
    backgroundColor: colors.background,
  },
  title: {
    color: '#000000',
    marginBottom: 4,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
});

export default styles;
