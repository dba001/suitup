import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';

import SizeTile from './SizeTile';

import defaultStyle from './styles';

class ArticleSizeBar extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  isAvailable = currentSize => {
    const { availableSizes } = this.props;
    return !!(availableSizes && availableSizes[currentSize]);
  };

  isSelected = currentSize => {
    const { selectedSize } = this.props;
    return selectedSize === currentSize;
  };

  render = () => {
    const { horizontal, onPressTile, style, sizes, ...props } = this.props;

    return (
      <FlatList
        horizontal={horizontal}
        data={sizes}
        keyExtractor={(item, i) => `ArticleSizeBar_${item.id}_${i}`}
        renderItem={({ item }) => (
          <SizeTile
            value={item.id}
            onPress={onPressTile(item)}
            available={this.isAvailable(item.id)}
            selected={this.isSelected(item.id)}
            style={style}
          />
        )}
        {...props}
      />
    );
  };
}

ArticleSizeBar.propTypes = {
  availableSizes: PropTypes.object,
  sizes: PropTypes.array.isRequired,
  horizontal: PropTypes.bool,
  onPressTile: PropTypes.func,
  selectedSize: PropTypes.string,
  style: PropTypes.object,
  showsHorizontalScrollIndicator: PropTypes.bool,
};

ArticleSizeBar.defaultProps = {
  horizontal: false,
};

export default ArticleSizeBar;
