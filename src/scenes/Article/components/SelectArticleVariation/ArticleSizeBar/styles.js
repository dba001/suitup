import { StyleSheet, Platform } from 'react-native';

import { colors, fontStyles } from 'theme/';

const tile = {
  padding: 8,
  backgroundColor: colors.background,
  borderWidth: StyleSheet.hairlineWidth,
  borderColor: colors.subText,
};
const disabledOpacity = 0.5;

const styles = StyleSheet.create({
  container: {
    padding: 4,
  },
  tile: {
    ...tile,
    opacity: disabledOpacity,
  },
  tileAvailable: {
    ...tile,
    borderWidth: 1,
  },
  tileSelected: {
    ...tile,
    borderColor: colors.highlight,
    borderWidth: 1,
  },
  text: {
    color: colors.subText,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  textSelected: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
});

export default styles;
