import React from 'react';
import { View, Text } from 'react-native';

import { Touchable } from 'components/';

import defaultStyles from './styles';

const SizeTile = ({ onPress, available, selected, value, style }) => {
  return (
    <Touchable onPress={onPress}>
      <View style={[defaultStyles.container, style && style.container]}>
        <View
          style={
            selected
              ? defaultStyles.tileSelected
              : available
              ? defaultStyles.tileAvailable
              : defaultStyles.tile
          }>
          <Text
            style={selected ? defaultStyles.textSelected : defaultStyles.text}>
            {value}
          </Text>
        </View>
      </View>
    </Touchable>
  );
};

export default SizeTile;
