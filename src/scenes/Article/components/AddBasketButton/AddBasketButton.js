import React from 'react';
import { Text, SafeAreaView, View } from 'react-native';

import { BasketIcon, Touchable } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

class AddBasketButton extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      availability,
      IsEligibleForPrime,
      basketItemCount,
      onPress,
      salePrice,
      price,
      style,
    } = this.props;
    return (
      <SafeAreaView
        style={[
          defaultStyle.addBasketButtonContainer,
          style && style.addBasketButtonContainer,
        ]}>
        <View
          style={[
            defaultStyle.priceInfoContainer,
            style && style.priceInfoContainer,
          ]}>
          <View
            style={[
              defaultStyle.salePriceInfoContainer,
              style && style.salePriceInfoContainer,
            ]}>
            <Text style={[defaultStyle.price, style && style.price]}>
              {salePrice || price}
            </Text>
            {salePrice ? (
              <Text style={[defaultStyle.salePrice, style && style.salePrice]}>
                {price}
              </Text>
            ) : (
              undefined
            )}
          </View>
          {availability ? (
            <Text
              style={[defaultStyle.availability, style && style.availability]}>
              {availability}
            </Text>
          ) : (
            undefined
          )}
        </View>
        <Touchable onPress={onPress}>
          <View
            style={[
              defaultStyle.addBasketContainer,
              style && style.addBasketContainer,
            ]}>
            <BasketIcon add basketItemCount={basketItemCount} />
          </View>
        </Touchable>
      </SafeAreaView>
    );
  };
}

export default AddBasketButton;
