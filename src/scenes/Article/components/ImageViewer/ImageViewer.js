import React from 'react';
import { View, Image } from 'react-native';

import { FlexImage } from 'components/';

import ImageViewerTileBar from './ImageViewerTileBar';

import defaultStyle from './styles';

class ImageViewer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { index: 0 };
  }

  handleOnPressTile = index => {
    this.setState({ index: index });
  };

  render = () => {
    const { index } = this.state;
    const { style, imageSet } = this.props;
    if (imageSet && imageSet.length > 0) {
      return (
        <View
          style={[
            defaultStyle.imageViewerContainer,
            style && style.imageViewerContainer,
          ]}>
          <Image
            style={[
              defaultStyle.imageViewerImage,
              style && style.imageViewerImage,
            ]}
            source={imageSet[index].image}
          />
          <ImageViewerTileBar
            horizontal={false}
            imageSet={imageSet}
            onPressTile={this.handleOnPressTile}
            selectedIndex={index}
            style={style}
          />
        </View>
      );
    }
    return (
      <View
        style={[
          defaultStyle.imageViewerContainer,
          style && style.imageViewerContainer,
        ]}
      />
    );
  };
}

export default ImageViewer;
