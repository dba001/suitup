import React from 'react';
import { FlatList, View } from 'react-native';

import { FlexImage } from 'components/';

import ImageViewerTile from './ImageViewerTile';

import defaultStyle from './styles';

class ImageViewerTileBar extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  selectTile = (selectedIndex, index) => selectedIndex === index;

  render = () => {
    const {
      imageSet,
      horizontal,
      onPressTile,
      selectedIndex,
      style,
    } = this.props;
    return (
      <View
        style={[
          defaultStyle.imageViewerTileBarContainer,
          style && style.imageViewerTileBarContainer,
        ]}>
        <FlatList
          data={imageSet}
          horizontal={horizontal}
          extraData={selectedIndex}
          keyExtractor={(imageSet, i) =>
            `ImageViewerTileBar_${imageSet.image.uri}_${i}`
          }
          renderItem={({ item, index }) => {
            return (
              <ImageViewerTile
                onPress={() => onPressTile(index)}
                selected={this.selectTile(selectedIndex, index)}
                image={item.thumbnail}
              />
            );
          }}
        />
      </View>
    );
  };
}

ImageViewerTileBar.defaultProps = {
  horizontal: false,
};

export default ImageViewerTileBar;
