import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  imageViewerContainer: {
    flex: 1,
    backgroundColor: colors.background,
    padding: 16,
  },
  imageViewerImage: {
    flex: 1,
    resizeMode: 'contain',
  },
  imageViewerTileTouchable: {
    width: 40,
    height: 40,
    marginBottom: 8,
    backgroundColor: colors.transparent,
  },
  imageViewerTileTouchableSelected: {
    width: 40,
    height: 40,
    marginBottom: 8,
    backgroundColor: colors.transparent,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.highlight,
  },
  imageViewerTileBarContainer: {
    ...StyleSheet.absoluteFillObject,
    top: 8,
  },
});

export default styles;
