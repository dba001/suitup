import React from 'react';
import { Image, TouchableOpacity } from 'react-native';

import { FlexImage } from 'components/';

import defaultStyle from './styles';

class ImageViewerTile extends React.PureComponent {
  render = () => {
    const { image, onPress, selected, style } = this.props;
    return (
      <TouchableOpacity
        style={
          selected
            ? [
                defaultStyle.imageViewerTileTouchableSelected,
                style && style.imageViewerTileTouchableSelected,
              ]
            : [
                defaultStyle.imageViewerTileTouchable,
                style && style.imageViewerTileTouchable,
              ]
        }
        onPress={onPress}>
        <FlexImage resizeMode={'contain'} source={image} />
      </TouchableOpacity>
    );
  };
}

export default ImageViewerTile;
