import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { SelectionListItem, Icon } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

const AttributeRow = ({ row, value }) => (
  <View style={defaultStyle.attributeRowContainer}>
    <Text>{row}</Text>
    <Text style={{ color: 'black' }}>{value}</Text>
  </View>
);

class ItemAttributes extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  renderPrice = price => (
    <View style={defaultStyle.attributeContainer}>
      <Text style={defaultStyle.attributeLabel}>
        {`${translate('Price')}:`}
      </Text>
      <Text style={defaultStyle.priceText}>{price || '--'}</Text>
    </View>
  );

  renderCondition = condition =>
    condition ? (
      <View style={defaultStyle.attributeContainer}>
        <Text style={defaultStyle.attributeLabel}>
          {`${translate('Condition')}:`}
        </Text>
        <Text style={defaultStyle.positiveHighlightText}>{condition}</Text>
      </View>
    ) : null;

  renderAvailability = availability => {
    if (availability) {
      const style =
        availability.type === 'now'
          ? defaultStyle.positiveHighlightText
          : defaultStyle.attributeLabel;
      return (
        <View style={defaultStyle.attributeContainer}>
          <Text style={style}>{availability.text}</Text>
        </View>
      );
    }
    return null;
  };

  render = () => {
    const { attributes, style } = this.props;

    return (
      <View style={[defaultStyle.container, style && style.container]}>
        {this.renderPrice(attributes.price)}
        {/* {this.renderCondition(attributes.condition)} */}
        {this.renderAvailability(attributes.availability)}
      </View>
    );
  };
}

export default ItemAttributes;
