import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  attributeRowContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  attributeContainer: {
    flex: 1,
    flexDirection: 'row',
    marginVertical: 4,
    alignItems: 'flex-end',
  },
  attributeLabel: {
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
    marginRight: 8,
  },
  attributeText: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  priceText: {
    color: '#b12703',
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  positiveHighlightText: {
    color: '#008a00',
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  container: {
    flex: 1,
    backgroundColor: colors.background,
    margin: 16,
  },
  contentBlock: {},
  heading: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  rightBlock: {},
  subTitle: {
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
  title: {
    color: 'red',
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
});

export default styles;
