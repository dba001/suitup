import React from 'react';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';

import ImageViewer from './components/ImageViewer';
import SelectionButton from './components/SelectionButton';

import { translate } from 'helpers/';
import styles from './styles';

class ArticleView extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render = () => {
    const {
      article,
      color,
      children,
      onPressChooseColor,
      onPressTopContainer,
      imageSet,
    } = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={styles.topContainer}>
          <TouchableOpacity
            style={styles.topContainerTouchable}
            onPress={onPressTopContainer}>
            <View style={styles.articleReviewContainer}>
              <Text
                numberOfLines={1}
                style={styles.articleBrandText}>{`${article.Brand}`}</Text>
            </View>
            <Text
              numberOfLines={1}
              style={styles.articleTitleText}>{`${article.Title}`}</Text>
          </TouchableOpacity>
          <ImageViewer style={styles} imageSet={imageSet} />
        </View>
        <View style={styles.container}>
          <SelectionButton
            title={`${translate('Color')}:`}
            subTitle={color ? color : `${translate('SelectColor')}`}
            onPress={onPressChooseColor}
            image={imageSet.length > 0 ? imageSet[0].thumbnail : null}
          />
          {children}
        </View>
      </ScrollView>
    );
  };
}

export default ArticleView;
