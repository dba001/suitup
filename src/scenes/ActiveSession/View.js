import React from 'react';
import { Animated } from 'react-native';

import { ListView, SessionListItemArticle } from 'components/';

import SessionHeader from './SessionHeader/SessionHeader';
import EmptyView from './EmptyView';

import styles from './styles';

const ActiveSessionView = ({
  session,
  articles,
  basketItemCount,
  filter,
  onChangeSearchText,
  onPressArrow,
  onPressArticle,
  onPressBasket,
  onRefresh,
  refreshing,
  transformStyle,
}) => (
  <Animated.View style={[styles.container, transformStyle()]}>
    <SessionHeader
      basketArticles={basketItemCount}
      onPressArrow={onPressArrow}
      onPressBasket={onPressBasket}
      likedArticles={session.likes.length}
      up={false}
    />
    {articles && articles.length > 0 ? (
      <ListView
        session={session}
        onChangeSearchText={onChangeSearchText}
        onRefresh={onRefresh}
        items={articles.filter(article => {
          if (!filter) {
            return true;
          }
          if (article.Title.toLowerCase().includes(filter.toLowerCase())) {
            return true;
          }
          return false;
        })}
        refreshing={refreshing}
        renderItem={({ item }) => {
          return (
            <SessionListItemArticle
              title={item.Title}
              imageSource={{
                uri: item.imagesets.primary.ThumbnailImage,
              }}
              onPress={() => onPressArticle(item.asin)}
            />
          );
        }}
      />
    ) : (
      <EmptyView />
    )}
  </Animated.View>
);

export default ActiveSessionView;
