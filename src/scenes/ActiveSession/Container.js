import React from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';
import { connect } from 'react-redux';

import {
  getActiveSessionBasket,
  basketItemCount,
  getActiveSession,
  activeSessionArticleSelector,
} from 'selectors/';

import { getRemoteBasket } from './actions';

import ActiveSessionView from './View';
import ActiveSessionCollapsedView from './CollapsedView';
import EmptyView from './EmptyView';

class ActiveSessionContainer extends React.Component {
  constructor(props) {
    super(props);
    const isEdit = false;
    this.state = {
      edit: isEdit,
      filter: '',
      refreshing: false,
      animate: new Animated.Value(0),
    };
  }

  handlePressArrow = () => {
    const { collapsed, arrowPressCallback } = this.props;
    const { animate } = this.state;
    const toValue = collapsed ? 0 : 1;
    Animated.timing(animate, {
      toValue: toValue,
      useNativeDriver: true,
    }).start(arrowPressCallback);
  };

  handlePressArticle = asin => {
    const { navigation } = this.props;
    navigation.navigate('article', { asin });
  };

  handlePressBasket = async () => {
    const { navigation } = this.props;
    navigation.navigate('basket');
  };

  transformStyle = () => {
    const { animate } = this.state;

    return {
      transform: [
        {
          translateY: animate.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 500],
          }),
        },
      ],
    };
  };

  handleRefresh = () => {};

  setFilter = text => {
    this.setState({ filter: text });
  };

  render = () => {
    const {
      basketItemCount,
      handlePressArrow,
      collapsed,
      activeSession,
      articles,
    } = this.props;
    const { filter, refreshing } = this.state;
    const _handlePressArrow = handlePressArrow
      ? handlePressArrow
      : this.handlePressArrow;
    if (activeSession) {
      if (collapsed) {
        return (
          <ActiveSessionCollapsedView
            session={activeSession}
            articles={articles}
            basketItemCount={basketItemCount}
            filter={filter}
            onChangeSearchText={this.setFilter}
            onPressArrow={_handlePressArrow}
            onPressBasket={this.handlePressBasket}
            onRefresh={this.handleRefresh}
            refreshing={refreshing}
          />
        );
      } else {
        return (
          <ActiveSessionView
            session={activeSession}
            articles={articles}
            basketItemCount={basketItemCount}
            filter={filter}
            onChangeSearchText={this.setFilter}
            onPressArrow={_handlePressArrow}
            onPressArticle={this.handlePressArticle}
            onPressBasket={this.handlePressBasket}
            onRefresh={this.handleRefresh}
            refreshing={refreshing}
            transformStyle={this.transformStyle}
          />
        );
      }
    } else {
      return <EmptyView />;
    }
  };
}

ActiveSessionContainer.propTypes = {
  activeSession: PropTypes.object.isRequired,
  articles: PropTypes.array.isRequired,
  basketItemCount: PropTypes.number,
  collapsed: PropTypes.bool,
  handlePressArrow: PropTypes.func,
};

ActiveSessionContainer.defaultProps = {
  collapsed: false,
};

const mapStateToProps = state => ({
  activeSession: getActiveSession(state),
  articles: activeSessionArticleSelector(state),
  basketItemCount: basketItemCount(state),
});

export default connect(mapStateToProps)(ActiveSessionContainer);
