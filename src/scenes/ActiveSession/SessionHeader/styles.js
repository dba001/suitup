import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  arrowIcon: {
    backgroundColor: colors.transparent,
    color: colors.highlight,
    fontSize: 24,
  },
  arrowIconContainer: {
    backgroundColor: colors.contrast,
    alignItems: 'center',
    borderRadius: 16,
    height: 32,
    justifyContent: 'center',
    width: 32,
  },
  iconContainer: {
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
  sessionHeaderContainer: {
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
  },
  subtitle: {
    backgroundColor: colors.transparent,
    color: colors.subText,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontWeight.regular,
  },
  textContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.regular,
  },
  onPressArrowTouchable: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  onPressBasketTouchable: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingHorizontal: 8,
  },
});

export default styles;
