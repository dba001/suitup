import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import { BasketIcon, Touchable, Icon } from 'components/';
import { translate } from 'helpers/';

import styles from './styles';

const SessionHeader = ({
  onPressArrow,
  onPressBasket,
  likedArticles,
  basketArticles,
  up,
}) => (
  <View style={styles.sessionHeaderContainer}>
    <Touchable style={styles.onPressArrowTouchable} onPress={onPressArrow}>
      <View style={styles.onPressArrowTouchable}>
        <View style={styles.iconContainer}>
          <Icon
            name={up ? 'ios-arrow-up' : 'ios-arrow-down'}
            style={{
              icon: styles.arrowIcon,
              iconContainer: styles.arrowIconContainer,
            }}
          />
        </View>
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={styles.title}>
            {`${translate('CurrentSession')}`}
          </Text>
          <Text numberOfLines={1} style={styles.subtitle}>
            {`${translate('ArticlesInSession', {
              count: likedArticles,
            })}`}
          </Text>
        </View>
      </View>
    </Touchable>
    <Touchable style={styles.onPressBasketTouchable} onPress={onPressBasket}>
      <View style={styles.onPressBasketTouchable}>
        <BasketIcon
          disabled={basketArticles === 0}
          basketBadge={basketArticles}
        />
      </View>
    </Touchable>
  </View>
);

SessionHeader.propTypes = {
  basketArticles: PropTypes.number,
  onPressArrow: PropTypes.func.isRequired,
  onPressBasket: PropTypes.func.isRequired,
  likedArticles: PropTypes.number,
  up: PropTypes.bool.isRequired,
};

SessionHeader.defaultProps = {
  likedArticles: 0,
  up: true,
};

export default SessionHeader;
