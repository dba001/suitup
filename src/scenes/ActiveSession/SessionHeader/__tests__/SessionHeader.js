import React from 'react';
import renderer from 'react-test-renderer';

import SessionHeader from './../SessionHeader';

describe('> SessionHeader component using snapshots', () => {
  let wrapper, instance;

  beforeEach(() => {
    wrapper = renderer.create(
      <SessionHeader onPressArrow={jest.fn} onPressBasket={jest.fn} />,
    );
    instance = wrapper.root.instance;
  });

  test('- renders default correctly', () => {
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
  test('- renders with session likes', () => {
    wrapper.update(<SessionHeader likedArticles={5} />);
    expect(wrapper.toJSON()).toMatchSnapshot();
  });
});
