import React from 'react';
import { Animated } from 'react-native';

import { Button, Icon, Touchable } from 'components/';

import { translate } from 'helpers/';

import SessionHeader from './SessionHeader';

import styles from './styles';

const ActiveSessionView = ({
  session,
  articles,
  basketItemCount,
  filter,
  onChangeSearchText,
  onPressArrow,
  onPressBasket,
  onRefresh,
  refreshing,
  transformStyle,
}) => (
  <Animated.View style={styles.collapsedContainer}>
    <SessionHeader
      basketArticles={basketItemCount}
      onPressArrow={onPressArrow}
      onPressBasket={onPressBasket}
      likedArticles={session.likes.length}
      up={true}
    />
  </Animated.View>
);

export default ActiveSessionView;
