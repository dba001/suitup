import React from 'react';
import { Animated, Dimensions, SafeAreaView, View } from 'react-native';
import { RedirectComponent } from 'react-native-oidc-client';
import PropTypes from 'prop-types';

import SwipeContainer from 'scenes/Swipe/Container';
import ActiveSessionContainer from 'scenes/ActiveSession/Container';

import styles from './styles';
const Screen = Dimensions.get('window');
const animationHeight = Screen.height * 0.8;

import Amplify from '@aws-amplify/core';
import aws_exports from '../../aws-exports';

Amplify.configure(aws_exports);

class RootContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      animate: new Animated.Value(1),
      showSwipeView: true,
    };
  }

  /** LIFECYCLE METHODS **/
  /** HANDLER METHODS **/
  arrowPressCallback = () => {
    this.setState({
      showSwipeView: !this.state.showSwipeView,
    });
  };

  handlePressDownArrow = () => {
    const { showSwipeView, animate } = this.state;
    const toValue = showSwipeView ? 0 : 1;
    Animated.timing(animate, {
      toValue: toValue,
      duration: 500,
      useNativeDriver: true,
    }).start(this.arrowPressCallback);
  };

  handlePressUpArrow = () => {
    const { showSwipeView, animate } = this.state;
    const toValue = showSwipeView ? 0 : 1;
    this.setState(
      {
        showSwipeView: !showSwipeView,
      },
      () =>
        Animated.timing(animate, {
          toValue: toValue,
          duration: 500,
          useNativeDriver: true,
        }).start(),
    );
  };

  transformStyle = () => {
    const { animate } = this.state;

    return {
      transform: [
        {
          translateY: animate.interpolate({
            inputRange: [0, 1],
            outputRange: [0, animationHeight],
          }),
        },
      ],
    };
  };

  /** RENDER METHODS **/
  renderSwipeContainer = showSwipeView => (
    <View style={styles.container}>
      <View style={styles.SwipeViewContainer}>
        <SwipeContainer navigation={this.props.navigation} />
      </View>
      <Animated.View style={styles.ActiveSessionCollapsedContainer}>
        <ActiveSessionContainer
          navigation={this.props.navigation}
          handlePressArrow={this.handlePressUpArrow}
          collapsed={showSwipeView}
        />
      </Animated.View>
    </View>
  );

  renderSessionContainer = showSwipeView => (
    <View style={styles.container}>
      <View style={styles.SwipeViewContainer}>
        <SwipeContainer navigation={this.props.navigation} />
      </View>
      <View style={styles.ActiveSessionCollapsedContainer} />
      <Animated.View
        style={[styles.ActiveSessionContainer, this.transformStyle()]}>
        <ActiveSessionContainer
          navigation={this.props.navigation}
          handlePressArrow={this.handlePressDownArrow}
          collapsed={showSwipeView}
        />
      </Animated.View>
    </View>
  );

  render = () => {
    const { showSwipeView } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        {showSwipeView
          ? this.renderSwipeContainer(showSwipeView)
          : this.renderSessionContainer(showSwipeView)}
        <RedirectComponent />
      </SafeAreaView>
    );
  };
}

RootContainer.propTypes = {};

export default RootContainer;
