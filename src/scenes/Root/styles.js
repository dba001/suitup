import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
  SwipeViewContainer: {
    flex: 1,
  },
  ActiveSessionContainer: {
    ...StyleSheet.absoluteFillObject,
  },
  ActiveSessionCollapsedContainer: {
    height: 64,
    borderColor: colors.separator,
    borderTopWidth: StyleSheet.hairlineWidth,
  },
});

export default styles;
