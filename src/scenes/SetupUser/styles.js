import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.background,
  },
  setupContainer: {
    flex: 1,
    backgroundColor: colors.background,
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
  },
  audienceContainer: {
    flexDirection: 'row',
  },
  audienceTouchable: {
    alignItems: 'center',
    width: 128,
    padding: '2%',
    margin: '5%',
    borderWidth: 2,
    borderColor: colors.contrast,
    borderRadius: 10,
    //backgroundColor: colors.contrast,
  },
  audienceText: {
    color: colors.highlight,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.regular,
    textAlign: 'center',
  },
  introContainer: {
    marginBottom: '8%',
  },
  introContainerText: {
    color: colors.subText,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.regular,
    textAlign: 'center',
  },
  welcomeText: {
    marginBottom: '2%',
    color: colors.subText,
    fontSize: fontStyles.fontSize.heading,
    fontWeight: fontStyles.fontWeight.emphasized,
    textAlign: 'center',
  },
});

export default styles;
