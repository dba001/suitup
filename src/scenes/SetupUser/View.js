import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Touchable, AmazonLoginButton } from 'components/';

import { CONSTANTS } from 'constants/';
import { translate } from 'helpers/';

import styles from './styles';

const renderAudienceTouchable = (onPressAudience, audience) => (
  <Touchable onPress={() => onPressAudience(CONSTANTS.AUDIENCE.men)}>
    <View style={styles.audienceTouchable}>
      <Text style={styles.audienceText}>{audience}</Text>
    </View>
  </Touchable>
);

const SetupUserView = ({ onPressAudience, onPressLogin }) => (
  <View style={styles.container}>
    <View style={styles.setupContainer}>
      <View style={styles.introContainer}>
        <Text style={styles.welcomeText}>{translate('desc_welcome')}</Text>
        <Text style={styles.introContainerText}>
          {translate('desc_choose')}
        </Text>
      </View>
      <View style={styles.audienceContainer}>
        {renderAudienceTouchable(onPressAudience, translate('Men'))}
        {renderAudienceTouchable(onPressAudience, translate('Women'))}
      </View>
    </View>
  </View>
);

export default SetupUserView;
