import React from 'react';
import { View } from 'react-native';

import { RedirectComponent } from 'react-native-oidc-client';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isFirstAppUsage } from 'selectors/';
import { setupUser } from './actions';
import { login } from 'users/';

import SetupUserView from './View';
import styles from './styles';

class SetupUserContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  /** LIFECYCLE METHODS **/
  /** HANDLER METHODS **/
  handlePressAudience = async audience => {
    const { setupUser, navigation } = this.props;
    await setupUser(audience);
    navigation.navigate('app');
  };

  /** RENDER METHODS **/
  render = () => (
    <View style={styles.container}>
      <SetupUserView onPressAudience={this.handlePressAudience} />
    </View>
  );
}

SetupUserContainer.propTypes = {};

const mapStateToProps = state => ({
  isFirstAppUsage: isFirstAppUsage(state),
});

const mapDispatchToProps = {
  setupUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SetupUserContainer);
