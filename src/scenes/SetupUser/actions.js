import { createNewSession } from 'sessions/';
import { setFirstAppUsage } from 'settings/';
import { createInitialUser } from 'users/';
import { createQueues } from 'categoryQueues/';

/** STATE SELECTOR FUNCTIONS **/
export const setupUser = audience => async dispatch => {
  const userId = dispatch(createInitialUser(audience));
  dispatch(createNewSession(userId, audience));
  dispatch(createQueues());
  dispatch(setFirstAppUsage());
};

/** SERVER REQUEST FUNCTIONS **/
