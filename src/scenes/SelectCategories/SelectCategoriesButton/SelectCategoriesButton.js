import React from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { withNavigation } from 'react-navigation';
import { Icon } from 'components';

import { colors } from 'theme';

const styles = StyleSheet.create({
  iconContainer: {
    width: 64,
    height: 64,
    padding: 16,
  },
});

class SelectCategoriesButton extends React.Component {
  constructor(props) {
    super(props);
  }

  handlePress = () => {
    const { goBack, navigation } = this.props;
    if (goBack) navigation.goBack();
    else navigation.navigate('selectCategories');
  };

  render = () => {
    return (
      <Icon
        type={'material'}
        name={'apps'}
        color={colors.highlight}
        onPress={this.handlePress}
        size={24}
        style={{ iconContainer: styles.iconContainer }}
      />
    );
  };
}

SelectCategoriesButton.defaultProps = {
  goBack: false,
};

SelectCategoriesButton.propTypes = {
  goBack: PropTypes.bool.isRequired,
  navigation: PropTypes.object.isRequired,
};

export default withNavigation(SelectCategoriesButton);
