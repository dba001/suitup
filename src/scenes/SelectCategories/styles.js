import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const swipeElevation = 4;

const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  innerContainer: {
    flex: 1,
    padding: 8,
  },
  row: {
    // alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tile: {
    backgroundColor: colors.background,
    flex: 1,
    margin: 8,
    ...Platform.select({
      android: {
        elevation: swipeElevation,
      },
      ios: {
        shadowOpacity: 0.0015 * swipeElevation + 0.18,
        shadowRadius: 0.54 * swipeElevation,
        shadowOffset: {
          height: 0.6 * swipeElevation,
        },
      },
    }),
  },
  tileDeSelected: {
    ...StyleSheet.absoluteFill,
    backgroundColor: colors.scim,
  },
  tileTitle: {
    alignItems: 'center',
    backgroundColor: colors.background,
    height: '15%',
    justifyContent: 'center',
  },
  title: {
    color: colors.text,
    fontSize: fontStyles.fontSize.body,
    fontWeight: fontStyles.fontWeight.emphasized,
  },
  titleDeSelected: {
    color: colors.subText,
  },
});

export default styles;
