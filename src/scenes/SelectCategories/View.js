import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import { FlexImage } from 'components';

import styles from './styles';

class SelectCategoriesView extends React.PureComponent {
  render = () => {
    const { categories, onToggleCategory } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          {categories.map((categoryChunk, i) => (
            <View key={i} style={[styles.row]}>
              {categoryChunk.map((chunk, i) => {
                const category = categoryChunk?.[i];
                if (!category) return undefined;
                const isSelected = category.selected;
                return (
                  <TouchableOpacity
                    key={category.browseNode}
                    onPress={onToggleCategory(category)}
                    style={[styles.tile]}>
                    <FlexImage source={category.source} />
                    <View style={styles.tileTitle}>
                      <Text
                        style={[
                          styles.title,
                          !isSelected && styles.titleDeSelected,
                        ]}>
                        {category.title}
                      </Text>
                    </View>
                    {!isSelected ? (
                      <View style={[styles.tileDeSelected]} />
                    ) : (
                      undefined
                    )}
                  </TouchableOpacity>
                );
              })}
            </View>
          ))}
        </View>
      </View>
    );
  };
}

SelectCategoriesView.propTypes = {
  categories: PropTypes.array.isRequired,
  onToggleCategory: PropTypes.func,
};

export default SelectCategoriesView;
