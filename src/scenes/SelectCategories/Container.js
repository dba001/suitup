import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { toggleCategory } from 'categories';
import { getChunkedCategories } from 'selectors';

import SelectCategoriesView from './View';

export class SelectCategoriesContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  handleToggleCategory = category => () => {
    const { toggleCategory } = this.props;
    toggleCategory(category.browseNode);
  };

  render = () => {
    const { categories } = this.props;
    return (
      <SelectCategoriesView
        categories={categories}
        onToggleCategory={this.handleToggleCategory}
      />
    );
  };
}

SelectCategoriesContainer.propTypes = {
  categories: PropTypes.array.isRequired,
  toggleCategory: PropTypes.func,
};

const mapStateToProps = state => ({
  categories: getChunkedCategories(state),
});

const mapDispatchToProps = {
  toggleCategory,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectCategoriesContainer);
