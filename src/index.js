/* global __DEV__ */
import React from 'react';
import { MenuProvider } from 'react-native-popup-menu';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/lib/integration/react';
import FSStorage from 'redux-persist-fs-storage';
import { createEpicMiddleware } from 'redux-observable';

import { rootReducer, rootEpic } from './redux';

// Container to initialize App data and show the splash screen
import InitializeContainer from 'scenes/Initialize/Container';
import initializeAction from 'scenes/Initialize/actions';

// Navigator app which includes all nested navigators
import AppContainerWithNavigation from './navigation';

import Amplify from '@aws-amplify/core';
import aws_exports from './aws-exports';

Amplify.configure(aws_exports);

// activate Amplify Logging
//global.LOG_LEVEL = 'DEBUG';

// create middleware for redux-observable
const epicMiddleware = createEpicMiddleware();

// Load middleware
let middleware = [
  thunk, // Allows action creators to return functions (not just plain objects)
  epicMiddleware,
];

if (__DEV__) {
  // Dev-only middleware
  middleware = [
    ...middleware,
    logger, // Logs state changes to the dev console
  ];
}

// redux persist config
const config = {
  key: 'root', // key is required
  keyPrefix: '',
  // blacklist: ['navigator'],
  storage: FSStorage(),
  debug: true,
  autoRehydrate: false,
  blacklist: ['requests'],
  timeout: null,
};

const persistRootReducer = persistReducer(config, rootReducer);

// Init redux store (using the given reducer & middleware)
const store = createStore(persistRootReducer, applyMiddleware(...middleware));
epicMiddleware.run(rootEpic);
const persistor = persistStore(store);

/* Component ==================================================================== */
// Wrap App in Redux provider (makes Redux available to all sub-components)
const AppContainer = () => (
  <Provider store={store}>
    <PersistGate
      persistor={persistor}
      loading={<InitializeContainer />}
      onBeforeLift={() => store.dispatch(initializeAction())}>
      <MenuProvider>
        {AppContainerWithNavigation ? (
          <AppContainerWithNavigation />
        ) : (
          undefined
        )}
      </MenuProvider>
    </PersistGate>
  </Provider>
);

export default AppContainer;
