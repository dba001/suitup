// @flow
// this is an auto generated file. This will be overwritten

export const onCreateArticle = `subscription OnCreateArticle {
  onCreateArticle {
    asin
    browsenode
  }
}
`;
export const onUpdateArticle = `subscription OnUpdateArticle {
  onUpdateArticle {
    asin
    browsenode
  }
}
`;
export const onDeleteArticle = `subscription OnDeleteArticle {
  onDeleteArticle {
    asin
    browsenode
  }
}
`;
export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    sub
    sessions {
      items {
        id
      }
      nextToken
    }
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    sub
    sessions {
      items {
        id
      }
      nextToken
    }
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    sub
    sessions {
      items {
        id
      }
      nextToken
    }
  }
}
`;
export const onCreateSession = `subscription OnCreateSession {
  onCreateSession {
    id
    user {
      id
      sub
      sessions {
        nextToken
      }
    }
  }
}
`;
export const onUpdateSession = `subscription OnUpdateSession {
  onUpdateSession {
    id
    user {
      id
      sub
      sessions {
        nextToken
      }
    }
  }
}
`;
export const onDeleteSession = `subscription OnDeleteSession {
  onDeleteSession {
    id
    user {
      id
      sub
      sessions {
        nextToken
      }
    }
  }
}
`;
