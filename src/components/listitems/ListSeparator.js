/**
 * ListSeparator component
 *
 * style
 *   listSeparatorTitle: Text
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text } from 'react-native';

const defaultStyles = StyleSheet.create({
  listSeparatorTitle: {
    backgroundColor: '#515151',
    color: 'white',
    fontSize: 14,
    height: 24,
    paddingHorizontal: 16,
  },
});

export default class ListSeparator extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  render = () => {
    const { title, style } = this.props;
    return (
      <Text
        style={[
          defaultStyles.listSeparatorTitle,
          style && style.listSeparatorTitle,
        ]}>
        {title}
      </Text>
    );
  };
}
