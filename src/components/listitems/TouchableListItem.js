/**
 * TouchableListItem component
 *
 * style
 *   touchableListItemContainer: View
 *   touchableListItemIconBox: View
 *   touchableListItemListItemContainer: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import ListItem from './ListItem';

const defaultStyles = StyleSheet.create({
  touchableListItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  touchableListItemIconBox: {
    paddingRight: 16,
  },
  touchableListItemListItemContainer: {
    flex: 1,
  },
});

export default class TouchableListItem extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.element,
  };

  static defaultProps = {
    icon: <Icon name={'ios-arrow-forward'} size={24} color={'grey'} />,
  };

  handleOnPress = () => {
    const { onPress } = this.props;
    onPress && onPress();
  };

  render = () => {
    const { icon, style } = this.props;
    return (
      <TouchableOpacity onPress={this.handleOnPress}>
        <View
          style={[
            defaultStyles.touchableListItemContainer,
            style && style.touchableListItemContainer,
          ]}>
          <View
            style={[
              defaultStyles.touchableListItemListItemContainer,
              style && style.touchableListItemListItemContainer,
            ]}>
            <ListItem {...this.props} />
          </View>
          <View
            style={[
              defaultStyles.touchableListItemIconBox,
              style && style.touchableListItemIconBox,
            ]}>
            {icon}
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}
