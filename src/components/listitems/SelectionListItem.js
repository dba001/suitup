/**
 * selectionListItem component
 *
 * style
 *   selectionListItemContainer: View
 *   selectionListItemIconBox: View
 *   selectionListItemListItemContainer: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, View } from 'react-native';

import Icon from './../Icon';

import ListItem from './ListItem';

const defaultStyles = StyleSheet.create({
  selectionListItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectionListItemIconBox: {
    paddingRight: 16,
  },
  selectionListItemListItemContainer: {
    flex: 1,
  },
  selectionListItemIcon: {
    color: 'red',
  },
  selectionListItemIconSelected: {
    color: 'green',
  },
});

export default class SelectionListItem extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
  };

  static defaultProps = {
    icon: style => (
      <Icon
        type="materialcommunity"
        name="checkbox-blank-circle-outline"
        size={24}
        style={{
          iconContainer: [
            defaultStyles.selectionListItemIconContainer,
            style && style.selectionListItemIconContainer,
          ],
          icon: [
            defaultStyles.selectionListItemIcon,
            style && style.selectionListItemIcon,
          ],
        }}
      />
    ),
    iconSelected: style => (
      <Icon
        type="materialcommunity"
        name="check-circle-outline"
        size={24}
        style={{
          iconContainer: [
            defaultStyles.selectionListItemIconContainer,
            style && style.selectionListItemIconContainer,
          ],
          icon: [
            defaultStyles.selectionListItemIconSelected,
            style && style.selectionListItemIconSelected,
          ],
        }}
      />
    ),
  };

  constructor(props) {
    super(props);

    this.state = { selected: false };
  }

  handleOnPress = () => {
    const { onPress } = this.props;
    onPress && onPress();
  };

  render = () => {
    const { icon, iconSelected, selected, style, onPress } = this.props;
    return (
      <TouchableOpacity onPress={this.handleOnPress}>
        <View
          style={[
            defaultStyles.selectionListItemContainer,
            style && style.selectionListItemContainer,
          ]}>
          <View
            style={[
              defaultStyles.selectionListItemListItemContainer,
              style && style.selectionListItemListItemContainer,
            ]}>
            <ListItem {...this.props} />
          </View>
          <View
            style={[
              defaultStyles.selectionListItemIconBox,
              style && style.selectionListItemIconBox,
            ]}>
            {selected ? iconSelected(style) : icon(style)}
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}
