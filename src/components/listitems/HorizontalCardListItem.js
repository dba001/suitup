/**
 * HorizontalCardListItem component
 *
 * style
 *   horizontalCardListItemContainer: View
 *   horizontalCardListItemTitle: Text
 *   horizontalCardListItemTitleContainer: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const defaultStyles = StyleSheet.create({
  horizontalCardListItemContainer: {
    borderRadius: 5,
    marginRight: 8,
    marginTop: 16,
    marginBottom: 16,
    backgroundColor: '#1F1F1F',
    width: 130,
    height: 150,
  },
  horizontalCardListItemTitle: {
    color: '#FFF',
    fontSize: 16,
    marginLeft: 16,
    marginTop: 8,
  },
  horizontalCardListItemTitleContainer: {
    height: 70,
    width: 130,
    backgroundColor: '#1F1F1F',
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
  },
});

export default class HorizontalCardListItem extends Component {
  static propTypes = {
    image: PropTypes.node.isRequired,
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
  };

  handlePress = () => {
    const { onPress } = this.props;
    onPress && onPress();
  };

  render = () => {
    const { image, style, title } = this.props;
    return (
      <TouchableOpacity onPress={this.handlePress}>
        <View
          style={[
            defaultStyles.horizontalCardListItemContainer,
            style && style.horizontalCardListItemContainer,
          ]}>
          {image}
          <View
            style={[
              defaultStyles.horizontalCardListItemTitleContainer,
              style && style.horizontalCardListItemTitleContainer,
            ]}>
            <Text
              style={[
                defaultStyles.horizontalCardListItemTitle,
                style && style.horizontalCardListItemTitle,
              ]}>
              {title}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}
