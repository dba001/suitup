/**
 * CollapsibleListItem component
 *
 * style
 *   collapsibleListItemContainer: View
 *   collapsibleListItemHeaderContainer: View
 *   collapsibleListItemIconBox: View
 *   collapsibleListItemListItemContainer: View
 *   collapsibleListItemSeparator: View
 *   collapsibleListItemText: Text
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import ListItem from './ListItem';

const defaultStyles = StyleSheet.create({
  collapsibleListItemContainer: {
    flexDirection: 'column',
  },
  collapsibleListItemHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  collapsibleListItemIconBox: {
    paddingRight: 16,
  },
  collapsibleListItemListItemContainer: {
    flex: 1,
  },
  collapsibleListItemSeparator: {
    height: 1,
    backgroundColor: 'black',
    marginLeft: 16,
    opacity: 0.12,
  },
  collapsibleListItemText: {
    color: 'black',
    fontSize: 14,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
});

export default class CollapsibleListItem extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    iconClose: PropTypes.element,
    iconOpen: PropTypes.element,
    numberOfLines: PropTypes.number,
    separator: PropTypes.bool,
  };

  static defaultProps = {
    iconClose: <Icon name={'ios-arrow-up'} size={24} color={'grey'} />,
    iconOpen: <Icon name={'ios-arrow-down'} size={24} color={'grey'} />,
    numberOfLines: 2,
    separator: false,
  };

  state = {
    collapsed: true,
  };

  toggleCollapsed = () => {
    const { collapsed } = this.state;
    this.setState({ collapsed: !collapsed });
  };

  render = () => {
    const {
      text,
      iconClose,
      iconOpen,
      numberOfLines,
      separator,
      style,
    } = this.props;
    const { collapsed } = this.state;
    const icon = collapsed ? iconOpen : iconClose;
    const nol = collapsed ? numberOfLines : 0;
    return (
      <TouchableOpacity onPress={this.toggleCollapsed}>
        <View
          style={[
            defaultStyles.collapsibleListItemContainer,
            style && style.collapsibleListItemContainer,
          ]}>
          <View
            style={[
              defaultStyles.collapsibleListItemHeaderContainer,
              style && style.collapsibleListItemHeaderContainer,
            ]}>
            <View
              style={[
                defaultStyles.collapsibleListItemListItemContainer,
                style && style.collapsibleListItemListItemContainer,
              ]}>
              <ListItem {...this.props} />
            </View>
            <View
              style={[
                defaultStyles.collapsibleListItemIconBox,
                style && style.collapsibleListItemIconBox,
              ]}>
              {icon}
            </View>
          </View>
          {separator && (
            <View
              style={[
                defaultStyles.collapsibleListItemSeparator,
                style && style.collapsibleListItemSeparator,
              ]}
            />
          )}
          <View>
            <Text
              style={[
                defaultStyles.collapsibleListItemText,
                style && style.collapsibleListItemText,
              ]}
              numberOfLines={nol}>
              {text}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}
