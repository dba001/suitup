/**
 * MatchCardListItem component
 *
 * style
 *   matchCardListItemAwayTeam: Text
 *   matchCardListItemAwayTeamContainer: View
 *   matchCardListItemAwayTeamLogoContainer: View
 *   matchCardListItemHomeTeam: Text
 *   matchCardListItemHomeTeamContainer: View
 *   matchCardListItemHomeTeamLogoContainer: View
 *   matchCardListItemInfo: Text
 *   matchCardListItemInfoContainer: View
 *   matchCardListItemMatchCardContainer: View
 *   matchCardListItemMatchupContainer: View
 *   matchCardListItemScore: Text
 *   matchCardListItemSeparator: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';

import TouchableListItem from './TouchableListItem';

const defaultStyles = StyleSheet.create({
  matchCardListItemAwayTeam: {
    color: 'black',
    fontSize: 16,
    paddingHorizontal: 16,
    textAlign: 'left',
  },
  matchCardListItemAwayTeamContainer: {
    flex: 1,
  },
  matchCardListItemAwayTeamLogoContainer: {
    width: 40,
    height: 40,
    borderRadius: 5,
    backgroundColor: 'lightgrey',
  },
  matchCardListItemHomeTeam: {
    color: 'black',
    fontSize: 16,
    paddingHorizontal: 16,
    textAlign: 'right',
  },
  matchCardListItemHomeTeamContainer: {
    flex: 1,
  },
  matchCardListItemHomeTeamLogoContainer: {
    width: 40,
    height: 40,
    borderRadius: 5,
    backgroundColor: 'lightgrey',
  },
  matchCardListItemInfo: {
    color: 'black',
    fontSize: 14,
    paddingHorizontal: 16,
    textAlign: 'center',
  },
  matchCardListItemInfoContainer: {
    paddingBottom: 8,
  },
  matchCardListItemMatchCardContainer: {
    flexDirection: 'column',
  },
  matchCardListItemMatchupContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 8,
  },
  matchCardListItemScore: {
    color: 'black',
    fontSize: 24,
    lineHeight: 26,
    fontWeight: 'bold',
    paddingHorizontal: 8,
  },
  matchCardListItemSeparator: {
    height: 1,
    marginLeft: 16,
    backgroundColor: 'black',
    opacity: 0.12,
  },
});

/* Component ==================================================================== */
export default class MatchCardListItem extends Component {
  static propTypes = {
    awayTeam: PropTypes.string.isRequired,
    awayTeamScore: PropTypes.number.isRequired,
    homeTeam: PropTypes.string.isRequired,
    homeTeamScore: PropTypes.number.isRequired,
    awayTeamLogo: PropTypes.element,
    homeTeamLogo: PropTypes.element,
    info: PropTypes.arrayOf(PropTypes.string),
  };

  renderItem = () => {
    const {
      awayTeam,
      awayTeamScore,
      homeTeam,
      homeTeamScore,
      awayTeamLogo,
      homeTeamLogo,
      info,
      style,
    } = this.props;
    return (
      <View
        style={[
          defaultStyles.matchCardListItemMatchCardContainer,
          style && style.matchCardListItemMatchCardContainer,
        ]}>
        <View
          style={[
            defaultStyles.matchCardListItemMatchupContainer,
            style && style.matchCardListItemMatchupContainer,
          ]}>
          <View
            style={[
              defaultStyles.matchCardListItemHomeTeamContainer,
              style && style.matchCardListItemHomeTeamContainer,
            ]}>
            {/* homeTeam name */}
            <Text
              style={[
                defaultStyles.matchCardListItemHomeTeam,
                style && style.matchCardListItemHomeTeam,
              ]}>
              {homeTeam}
            </Text>
          </View>
          <View>
            <View
              style={[
                defaultStyles.matchCardListItemHomeTeamLogoContainer,
                style && style.matchCardListItemHomeTeamLogoContainer,
              ]}>
              {/* homeTeam logo/emblem */}
              {homeTeamLogo}
            </View>
          </View>
          <View>
            {/* score */}
            <Text
              style={[
                defaultStyles.matchCardListItemScore,
                style && style.matchCardListItemScore,
              ]}>
              {`${homeTeamScore}:${awayTeamScore}`}
            </Text>
          </View>
          <View>
            <View
              style={[
                defaultStyles.matchCardListItemAwayTeamLogoContainer,
                style && style.matchCardListItemAwayTeamLogoContainer,
              ]}>
              {/* awayTeam logo/emblem */}
              {awayTeamLogo}
            </View>
          </View>
          <View
            style={[
              defaultStyles.matchCardListItemAwayTeamContainer,
              style && style.matchCardListItemAwayTeamContainer,
            ]}>
            {/* awayTeam name */}
            <Text
              style={[
                defaultStyles.matchCardListItemAwayTeam,
                style && style.matchCardListItemAwayTeam,
              ]}>
              {awayTeam}
            </Text>
          </View>
        </View>
        {info && (
          <View
            style={[
              defaultStyles.matchCardListItemInfoContainer,
              style && style.matchCardListItemInfoContainer,
            ]}>
            {info.map((line, index) => (
              <Text
                key={`info_${index}`}
                style={[
                  defaultStyles.matchCardListItemInfo,
                  style && style.matchCardListItemInfo,
                ]}>
                {line}
              </Text>
            ))}
          </View>
        )}
      </View>
    );
  };

  render = () => {
    const { onPress, style, title } = this.props;
    if (onPress) {
      return (
        <View>
          {this.renderItem()}
          <View
            style={[
              defaultStyles.matchCardListItemSeparator,
              style && style.matchCardListItemSeparator,
            ]}
          />
          <TouchableListItem title={title} onPress={onPress} style={style} />
        </View>
      );
    }
    return this.renderItem();
  };
}
