/**
 * RichListItem component
 *
 * style
 *   richListItemContentContainer: View
 *   richListItemOverlayContainer: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

import ListItem from './ListItem';
import TouchableListItem from './TouchableListItem';

const defaultStyles = StyleSheet.create({
  richListItemContentContainer: {
    aspectRatio: 2,
  },
  richListItemOverlayContainer: {
    backgroundColor: 'white',
    opacity: 0.9,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
});

export default class RichListItem extends Component {
  static propTypes = {
    content: PropTypes.element.isRequired,
    onPress: PropTypes.func,
  };

  renderContent = () => {
    const { content, style } = this.props;
    if (content) {
      return (
        <View
          style={[
            defaultStyles.richListItemContentContainer,
            style && style.richListItemContentContainer,
          ]}>
          {content}
        </View>
      );
    }
    return <View />;
  };

  render = () => {
    const { onPress, style } = this.props;
    return (
      <View>
        {this.renderContent()}
        <View
          style={[
            defaultStyles.richListItemOverlayContainer,
            style && style.richListItemOverlayContainer,
          ]}>
          {onPress ? (
            <TouchableListItem {...this.props} style={style} />
          ) : (
            <ListItem {...this.props} style={style} />
          )}
        </View>
      </View>
    );
  };
}
