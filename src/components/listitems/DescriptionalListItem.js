/**
 * DescriptionalListItem component
 *
 * style
 *   descriptionalListItemDescriptionContainer: View
 *   descriptionalListItemDescriptionSubtitle: Text
 *   descriptionalListItemDescriptionTitle: Text
 *   descriptionalListItemLabelContainer: View
 *   descriptionalListItemLabelSpacer: View
 *   descriptionalListItemSeparator: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';

import ListItem from './ListItem';
import TouchableListItem from './TouchableListItem';

const defaultStyles = StyleSheet.create({
  descriptionalListItemDescriptionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  descriptionalListItemDescriptionSubtitle: {
    color: 'grey',
    fontSize: 14,
  },
  descriptionalListItemDescriptionTitle: {
    color: 'black',
    fontSize: 16,
  },
  descriptionalListItemLabelContainer: {
    flexDirection: 'column',
  },
  descriptionalListItemLabelSpacer: {
    flex: 1,
    maxWidth: 24,
  },
  descriptionalListItemSeparator: {
    height: 1,
    marginLeft: 16,
    backgroundColor: 'black',
    opacity: 0.12,
  },
});

export default class DescriptionalListItem extends Component {
  static propTypes = {
    labels: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  renderItem = () => {
    const { labels, style } = this.props;
    if (labels) {
      const last = labels.length - 1;
      const spacedLabels = [];
      labels.forEach((label, index) => {
        spacedLabels.push(
          <View
            key={`label_${index}`}
            style={[
              defaultStyles.descriptionalListItemLabelContainer,
              style && style.descriptionalListItemLabelContainer,
            ]}>
            <Text
              style={[
                defaultStyles.descriptionalListItemDescriptionTitle,
                style && style.descriptionalListItemDescriptionTitle,
              ]}>
              {label.title}
            </Text>
            <Text
              style={[
                defaultStyles.descriptionalListItemDescriptionSubtitle,
                style && style.descriptionalListItemDescriptionSubtitle,
              ]}>
              {label.subtitle}
            </Text>
          </View>,
        );
        if (index !== last) {
          spacedLabels.push(
            <View
              key={`spacer_${index}.5`}
              style={[
                defaultStyles.descriptionalListItemLabelSpacer,
                style && style.descriptionalListItemLabelSpacer,
              ]}
            />,
          );
        }
      });
      return (
        <View>
          <View
            style={[
              defaultStyles.descriptionalListItemSeparator,
              style && style.descriptionalListItemSeparator,
            ]}
          />
          <View
            style={[
              defaultStyles.descriptionalListItemDescriptionContainer,
              style && style.descriptionalListItemDescriptionContainer,
            ]}>
            {spacedLabels}
          </View>
        </View>
      );
    }
    return <View />;
  };

  render = () => {
    const { onPress } = this.props;
    return (
      <View>
        {onPress ? (
          <TouchableListItem {...this.props} />
        ) : (
          <ListItem {...this.props} />
        )}
        {this.renderItem()}
      </View>
    );
  };
}
