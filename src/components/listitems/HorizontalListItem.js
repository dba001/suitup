/**
 * HorizontalListItem component
 *
 * style
 *   horizontalListItemFlatList: FlatList
 *   horizontalListItemSeparator: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FlatList, StyleSheet, View } from 'react-native';

import ListItem from './ListItem';
import TouchableListItem from './TouchableListItem';

const defaultStyles = StyleSheet.create({
  horizontalListItemSeparator: {
    height: 1,
    marginLeft: 16,
    backgroundColor: 'rgba(0, 0, 0, 0.12)',
  },
});

export default class HorizontalListItem extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    ListHeaderComponent: PropTypes.func,
    renderItem: PropTypes.func.isRequired,
    onPress: PropTypes.func,
  };

  handleOnPress = () => {
    const { data, onPress } = this.props;
    onPress && onPress(data);
  };

  renderItem = () => {
    const { data, ListHeaderComponent, renderItem, style } = this.props;
    if (data) {
      return (
        <View>
          <View
            style={[
              defaultStyles.horizontalListItemSeparator,
              style && style.horizontalListItemSeparator,
            ]}
          />
          <FlatList
            style={[style && style.horizontalListItemFlatList]}
            data={data}
            horizontal
            ListHeaderComponent={ListHeaderComponent}
            renderItem={renderItem}
          />
        </View>
      );
    }
    return <View />;
  };

  render = () => {
    const { onPress } = this.props;
    return (
      <View>
        {onPress ? (
          <TouchableListItem {...this.props} onPress={this.handleOnPress} />
        ) : (
          <ListItem {...this.props} />
        )}
        {this.renderItem()}
      </View>
    );
  };
}
