/**
 * ListItem component
 *
 * style
 *   listItemBadge: Text
 *   listItemBadgeImageContainer: View
 *   listItemContainerSingleLine: View
 *   listItemContainerTwoLine: View
 *   listItemImageBox: View
 *   listItemRightSubtitle: Text
 *   listItemRightTitle: Text
 *   listItemRightContainer: View
 *   listItemSubtitle: Text
 *   listItemTextContainer: View
 *   listItemTitle: Text
 *   listItemTitleContainer: View
 * }
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';

const defaultStyles = StyleSheet.create({
  listItemBadge: {
    backgroundColor: '#515151',
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    color: 'white',
    fontSize: 14,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  listItemBadgeAbsolute: {
    position: 'absolute',
  },
  listItemBadgeImageContainer: {
    justifyContent: 'center',
  },
  listItemContainerSingleLine: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 48,
  },
  listItemContainerTwoLine: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 56,
  },
  listItemImageBox: {
    paddingLeft: 16,
  },
  listItemRightSubtitle: {
    color: 'grey',
    fontSize: 14,
    textAlign: 'right',
  },
  listItemRightTitle: {
    color: 'black',
    fontSize: 16,
    textAlign: 'right',
  },
  listItemRightContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  listItemRightIconContainer: {
    marginRight: 16,
  },
  listItemRightTextContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    marginRight: 16,
  },
  listItemSubtitle: {
    color: 'grey',
    fontSize: 14,
    paddingLeft: 16,
    textAlign: 'left',
  },
  listItemTextContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  listItemTitle: {
    color: 'black',
    fontSize: 16,
    paddingLeft: 16,
    textAlign: 'left',
  },
  listItemTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

export default class ListItem extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired, // primary text
    badge: PropTypes.string,
    image: PropTypes.element,
    rightIcon: PropTypes.element,
    rightSubtitle: PropTypes.string, // info label
    rightTitle: PropTypes.string, // info content
    subtitle: PropTypes.string, // secondary text
  };

  static defaultProps = {
    badge: null,
    image: null,
    rightIcon: null,
    rightSubtitle: null,
    rightTitle: null,
    subtitle: null,
  };

  renderBadgeImage = () => {
    const { badge, image, style } = this.props;
    if (image) {
      return (
        <View style={[defaultStyles.listItemBadgeImageContainer]}>
          <View
            style={[
              defaultStyles.listItemImageBox,
              style && style.listItemImageBox,
            ]}>
            {image}
          </View>
          {!!badge && (
            <Text
              style={[
                defaultStyles.listItemBadgeAbsolute,
                defaultStyles.listItemBadge,
                style && style.listItemBadge,
              ]}>
              {badge}
            </Text>
          )}
        </View>
      );
    }
    if (badge) {
      return (
        <Text
          style={[defaultStyles.listItemBadge, style && style.listItemBadge]}>
          {badge}
        </Text>
      );
    }
    return <View />;
  };

  renderRight = () => {
    const { rightIcon, rightSubtitle, rightTitle, style } = this.props;
    if (rightTitle || rightSubtitle) {
      return (
        <View
          style={[
            defaultStyles.listItemRightContainer,
            style && style.listItemRightContainer,
          ]}>
          <View
            style={[
              defaultStyles.listItemRightTextContainer,
              style && style.listItemRightTextContainer,
            ]}>
            {!!rightTitle && (
              <Text
                numberOfLines={1}
                style={[
                  defaultStyles.listItemRightTitle,
                  style && style.listItemRightTitle,
                ]}>
                {rightTitle}
              </Text>
            )}
            {!!rightSubtitle && (
              <Text
                numberOfLines={1}
                style={[
                  defaultStyles.listItemRightSubtitle,
                  style && style.listItemRightSubtitle,
                ]}>
                {rightSubtitle}
              </Text>
            )}
          </View>
          {!!rightIcon && (
            <View
              style={[
                defaultStyles.listItemRightIconContainer,
                style && style.listItemRightIconContainer,
              ]}>
              {rightIcon}
            </View>
          )}
        </View>
      );
    }
    if (rightIcon) {
      return (
        <View
          style={[
            defaultStyles.listItemRightContainer,
            style && style.listItemRightContainer,
          ]}>
          <View
            style={[
              defaultStyles.listItemRightIconContainer,
              style && style.listItemRightIconContainer,
            ]}>
            {rightIcon}
          </View>
        </View>
      );
    }
  };

  render = () => {
    const {
      title,
      image,
      rightSubtitle,
      rightTitle,
      style,
      subtitle,
    } = this.props;
    let defaultContainerStyle = defaultStyles.listItemContainerSingleLine;
    if (image || subtitle) {
      defaultContainerStyle = defaultStyles.listItemContainerTwoLine;
    }
    if (!!rightSubtitle && !!rightTitle) {
      defaultContainerStyle = defaultStyles.listItemContainerTwoLine;
    }
    let containerStyle = style && style.listItemContainerSingleLine;
    if (image || subtitle) {
      containerStyle = style && style.listItemContainerTwoLine;
    }
    if (!!rightSubtitle && !!rightTitle) {
      containerStyle = style && style.listItemContainerTwoLine;
    }
    return (
      <View style={[defaultContainerStyle, containerStyle]}>
        {this.renderBadgeImage()}
        <View
          style={[
            defaultStyles.listItemTextContainer,
            style && style.listItemTextContainer,
          ]}>
          <View
            style={[
              defaultStyles.listItemTitleContainer,
              style && style.listItemTitleContainer,
            ]}>
            <Text
              numberOfLines={1}
              style={[
                defaultStyles.listItemTitle,
                style && style.listItemTitle,
              ]}>
              {title}
            </Text>
            {!!subtitle && (
              <Text
                numberOfLines={1}
                style={[
                  defaultStyles.listItemSubtitle,
                  style && style.listItemSubtitle,
                ]}>
                {subtitle}
              </Text>
            )}
          </View>
          {this.renderRight()}
        </View>
      </View>
    );
  };
}
