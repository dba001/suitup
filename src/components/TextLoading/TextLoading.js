import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';

import defaultStyles from './styles';

const createImage = (imageSize, style) => (
  <View
    style={[
      defaultStyles.textLoadingImageContainer,
      style && style.textLoadingImageContainer,
      { flex: imageSize },
    ]}>
    <View
      style={[
        defaultStyles.textLoadingImageContainerRow,
        style && style.textLoadingImageContainerRow,
      ]}>
      <View
        style={[
          defaultStyles.textLoadingImage,
          style && style.textLoadingImage,
        ]}
      />
      <View
        style={[
          defaultStyles.textLoadingImageSpacer,
          style && style.textLoadingImageSpacer,
        ]}
      />
    </View>
  </View>
);

const createRows = (rows, style) => {
  let children = [];
  for (let i = 0; i < rows; i++) {
    children.push(
      <View
        key={`textloading_row_${i}`}
        style={[defaultStyles.textLoadingRow, style && style.textLoadingRow]}
      />,
    );
  }
  return children;
};

const TextLoading = ({ imageSize, rows, style, withImage }) => (
  <View
    style={[
      defaultStyles.textLoadingContainer,
      style && style.textLoadingContainer,
    ]}>
    {withImage ? createImage(imageSize, style) : undefined}
    {createRows(rows, style)}
  </View>
);

TextLoading.defaultProps = {
  rows: 1,
  withImage: false,
  imageSize: 5,
};

TextLoading.propTypes = {
  rows: PropTypes.number.isRequired,
  imageSize: PropTypes.number,
  withImage: PropTypes.bool,
  style: ViewPropTypes.style,
};

export default TextLoading;
