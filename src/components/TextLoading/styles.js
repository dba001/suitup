import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  textLoadingImageContainer: {
    flex: 5,
  },
  textLoadingImageContainerRow: {
    flex: 1,
    flexDirection: 'row',
  },
  textLoadingImage: {
    flex: 2,
    backgroundColor: colors.contrast,
  },
  textLoadingImageSpacer: {
    flex: 1,
    backgroundColor: colors.background,
  },
  textLoadingRow: {
    flex: 1,
    backgroundColor: colors.contrast,
    marginTop: 8,
  },
  textLoadingContainer: {
    flex: 1,
    flexDirection: 'column',
  },
});

export default styles;
