import { StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  loading: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.regular,
  },
  modalContainer: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    flex: 1,
    justifyContent: 'center',
  },
});

export default styles;
