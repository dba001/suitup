import React from 'react';
import { ActivityIndicator, Modal, Text, View } from 'react-native';

import { translate } from 'helpers/';
import { colors } from 'theme/';

import styles from './styles';

const ModalLoading = ({ visible }) => (
  <Modal onRequestClose={() => {}} transparent visible={visible}>
    <View style={styles.modalContainer}>
      <ActivityIndicator size={'large'} color={colors.primary.color} />
      <Text style={styles.loading}>{`${translate('Loading')} ...`}</Text>
    </View>
  </Modal>
);

export default ModalLoading;
