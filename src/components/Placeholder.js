import React from 'react';
import { Text, View } from 'react-native';

import { translate } from 'helpers/';
import { colors } from 'theme/';

const Placholder = () => (
  <View>
    <Text>{`${translate('Loading')} ...`}</Text>
  </View>
);

export default Placholder;
