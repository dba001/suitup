import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Dimensions,
  Image,
  Text,
  Animated,
  StyleSheet,
} from 'react-native';
import Interactable from 'react-native-interactable';

import { FlexImage } from 'components/';
import { translate } from 'helpers/';

import defaultStyle from './styles';

const CONSTANT = {
  SWIPELEFT: 'SWIPELEFT',
  SWIPERIGHT: 'SWIPERIGHT',
};

const SwipeableCard = ({
  source,
  title,
  onSwipeLeft,
  onSwipeRight,
  swipeLeftOverlayText,
  swipeRightOverlayText,
  style,
}) => {
  const swipeable = !!onSwipeRight || !!onSwipeLeft;
  let _deltaX = new Animated.Value(0);
  const renderContent = (
    <Animated.View
      style={[
        defaultStyle.swipeableCardCard,
        style && style.swipeableCardCard,
        !swipeable && defaultStyle.notSwipeableCardCard,
        !swipeable && style && style.notSwipeableCardCard,
        {
          transform: [
            {
              rotate: _deltaX.interpolate({
                inputRange: [-250, 0, 250],
                outputRange: ['10deg', '0deg', '-10deg'],
              }),
            },
          ],
        },
      ]}>
      <FlexImage
        resizeMode={'contain'}
        source={source}
        style={[
          defaultStyle.swipeAbleCardFlexImage,
          style && style.swipeAbleCardFlexImage,
        ]}
      />
      <Animated.View
        style={[
          defaultStyle.swipeAbleCardItemAttributeContainer,
          style && style.swipeAbleCardItemAttributeContainer,
        ]}>
        <View
          style={[
            defaultStyle.swipeAbleCardItemAttributes,
            style && style.swipeAbleCardItemAttributes,
          ]}>
          <Text
            style={[
              defaultStyle.swipeAbleCardItemAttributesText,
              style && style.swipeAbleCardItemAttributesText,
            ]}
            numberOfLines={3}>
            {title}
          </Text>
        </View>
      </Animated.View>
      <Animated.View
        style={[
          defaultStyle.swipeAbleCardOverlay,
          style && style.swipeAbleCardOverlay,
          defaultStyle.swipeAbleCardDislike,
          style && style.swipeAbleCardDislike,
          {
            opacity: _deltaX.interpolate({
              inputRange: [-120, 0],
              outputRange: [0.8, 0],
              extrapolateLeft: 'clamp',
              extrapolateRight: 'clamp',
            }),
          },
        ]}>
        <Text
          style={[
            defaultStyle.swipeAbleCardOverlayText,
            style && style.swipeAbleCardOverlayText,
          ]}>
          {swipeLeftOverlayText}
        </Text>
      </Animated.View>
      <Animated.View
        style={[
          defaultStyle.swipeAbleCardOverlay,
          style && style.swipeAbleCardOverlay,
          defaultStyle.swipeAbleCardLike,
          style && style.swipeAbleCardLike,
          {
            opacity: _deltaX.interpolate({
              inputRange: [0, 120],
              outputRange: [0, 0.8],
              extrapolateLeft: 'clamp',
              extrapolateRight: 'clamp',
            }),
          },
        ]}>
        <Text
          style={[
            defaultStyle.swipeAbleCardOverlayText,
            style && style.swipeAbleCardOverlayText,
          ]}>
          {swipeRightOverlayText}
        </Text>
      </Animated.View>
    </Animated.View>
  );
  if (swipeable) {
    const _onSnap = event => {
      const swipeId = event.nativeEvent.id;
      if (swipeId === CONSTANT.SWIPELEFT) {
        onSwipeLeft();
      } else if (swipeId === CONSTANT.SWIPERIGHT) {
        onSwipeRight();
      }
    };
    return (
      <Interactable.View
        ref={component => (this._interactableView = component)}
        style={[
          defaultStyle.swipeableCardContainer,
          style && style.swipeableCardContainer,
        ]}
        snapPoints={[
          { x: 390, id: CONSTANT.SWIPERIGHT },
          { x: 0, damping: 0.7 },
          { x: -390, id: CONSTANT.SWIPELEFT },
        ]}
        animatedNativeDriver={true}
        animatedValueX={_deltaX}
        onSnap={_onSnap}>
        {renderContent}
      </Interactable.View>
    );
  } else {
    return (
      <View
        style={[
          defaultStyle.swipeableCardContainer,
          style && style.swipeableCardContainer,
          defaultStyle.notSwipeableCardContainer,
          style && style.notSwipeableCardContainer,
        ]}>
        {renderContent}
      </View>
    );
  }
};

SwipeableCard.propTypes = {
  source: PropTypes.object,
  onSnap: PropTypes.func,
};

export default SwipeableCard;
