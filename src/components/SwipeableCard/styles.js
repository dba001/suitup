import { Dimensions, StyleSheet, Platform } from 'react-native';

import { colors, fontStyles } from 'theme/';

const elevation = 4;
const borderRadius = 16;

const styles = StyleSheet.create({
  notSwipeableCardContainer: {},
  swipeableCardContainer: {},
  notSwipeableCardCard: {},
  swipeableCardCard: {},
  swipeAbleCardOverlay: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  swipeAbleCardOverlayText: {
    fontSize: 60,
    color: colors.contrast,
  },
  swipeAbleCardItemAttributeContainer: {
    backgroundColor: colors.background,
  },
  swipeAbleCardItemAttributes: {
    margin: 16,
  },
  swipeAbleCardItemAttributesText: {
    color: colors.highlight,
    fontSize: fontStyles.fontSize.title,
    fontWeight: fontStyles.fontWeight.regular,
  },
  swipeAbleCardLike: {
    backgroundColor: colors.action.like,
  },
  swipeAbleCardDislike: {
    backgroundColor: colors.action.dislike,
  },
});

export default styles;
