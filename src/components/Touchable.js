import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';

const AndroidTouchable = ({ borderless, children, color, ...others }) => (
  <TouchableNativeFeedback
    background={
      !!color && Platform.Version >= 21
        ? TouchableNativeFeedback.Ripple(color, borderless)
        : TouchableNativeFeedback.SelectableBackground()
    }
    {...others}>
    {children}
  </TouchableNativeFeedback>
);

AndroidTouchable.propTypes = {
  borderless: PropTypes.bool,
  children: PropTypes.any,
  color: PropTypes.string,
};
AndroidTouchable.defaultProps = {
  borderless: false,
  children: null,
  color: null,
};

const IosTouchable = ({ activeOpacity, children, ...others }) => (
  <TouchableOpacity activeOpacity={activeOpacity} {...others}>
    {children}
  </TouchableOpacity>
);

IosTouchable.propTypes = {
  activeOpacity: PropTypes.number,
  children: PropTypes.any,
};
IosTouchable.defaultProps = {
  activeOpacity: 0.5,
  children: null,
};

const Touchable = Platform.select({
  android: AndroidTouchable,
  ios: IosTouchable,
});

export default Touchable;
