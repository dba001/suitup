import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  flexImage: {
    flexGrow: 1,
    height: null,
    width: null,
  },
  flexImageContainer: {
    aspectRatio: 1,
    backgroundColor: 'cornflowerblue',
    flex: 1,
    flexDirection: 'column',
  },
});

export default styles;
