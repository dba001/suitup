/**
 * FlexImage component
 *
 * style
 *   flexImage: Image
 *   flexImageContainer: View
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';

import defaultStyles from './styles';

/* Component ==================================================================== */
class FlexImage extends Component {
  constructor(props) {
    super(props);

    this.state = { containerHeight: null };
  }

  render() {
    const { children, resizeMode, rounded, source, style } = this.props;
    const { containerHeight } = this.state;
    const borderRadius = containerHeight ? containerHeight / 2 : 50;
    if (rounded) {
      return (
        <View
          style={[
            defaultStyles.flexImageContainer,
            style && style.flexImageContainer,
            {
              borderRadius,
            },
          ]}
          onLayout={async e => {
            const { height } = e.nativeEvent.layout;
            await this.setState({ containerHeight: height });
          }}>
          <Image
            resizeMode={resizeMode}
            source={source}
            style={[
              defaultStyles.flexImage,
              style && style.flexImage,
              { borderRadius },
            ]}>
            {children}
          </Image>
        </View>
      );
    }
    return (
      <Image
        resizeMode={resizeMode}
        source={source}
        style={[defaultStyles.flexImage, style && style.flexImage]}>
        {children}
      </Image>
    );
  }
}

FlexImage.propTypes = {
  source: PropTypes.oneOfType([PropTypes.number, PropTypes.object]).isRequired,
  rounded: PropTypes.bool,
};

FlexImage.defaultProps = {};

/* Export Component ==================================================================== */
export default FlexImage;
