import { Dimensions, StyleSheet, Platform } from 'react-native';

import { colors, fontStyles } from 'theme/';

const swipeElevation = 4;

const styles = StyleSheet.create({
  swipeableCardStackContainer: {
    flex: 1,
    maxHeight: '85%',
  },
  notSwipeableCardContainer: {
    zIndex: -1,
  },
  swipeableCardContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'center',
    paddingTop: 16,
  },
  swipeableCardCard: {
    aspectRatio: 1 / 1.25,
    backgroundColor: colors.background,
    ...Platform.select({
      android: {
        elevation: swipeElevation,
      },
      ios: {
        shadowOpacity: 0.0085 * swipeElevation + 0.18,
        shadowRadius: 0.84 * swipeElevation,
        shadowOffset: {
          height: 0.84 * swipeElevation,
        },
      },
    }),
  },
  notSwipeableCardCard: {
    top: 2,
    right: 2,
  },
});

export const fakeCardStyle = {
  ...styles,
  notSwipeableCardCard: {
    right: 4,
    top: 4,
  },
};

export default styles;
