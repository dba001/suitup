import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions, Image, Text, Animated } from 'react-native';
import Interactable from 'react-native-interactable';

import { FlexImage } from 'components/';
import { translate } from 'helpers/';

import SwipeableCard from '../SwipeableCard/SwipeableCard';

import defaultStyle, { fakeCardStyle } from './styles';

let renderId = 0;

const SwipeableCardStack = ({
  cardKey,
  nextCardKey,
  cardSource,
  nextCardSource,
  cardTitle,
  nextCardTitle,
  onSwipeLeft,
  onSwipeRight,
  swipeLeftOverlayText,
  swipeRightOverlayText,
  style,
}) => {
  // reset interactable view after every render
  renderId = renderId + 1;
  return (
    <View
      style={[
        defaultStyle.swipeableCardStackContainer,
        style && style.swipeableCardStackContainer,
      ]}>
      <SwipeableCard
        style={fakeCardStyle}
        source={nextCardSource}
        title={nextCardTitle}
      />
      <SwipeableCard
        key={nextCardKey}
        style={defaultStyle}
        source={nextCardSource}
        title={nextCardTitle}
      />
      <SwipeableCard
        key={cardKey + renderId}
        style={defaultStyle}
        source={cardSource}
        title={cardTitle}
        onSwipeLeft={onSwipeLeft}
        onSwipeRight={onSwipeRight}
        swipeLeftOverlayText={translate('Dislike')}
        swipeRightOverlayText={translate('Like')}
      />
    </View>
  );
};

SwipeableCardStack.propTypes = {
  source: PropTypes.object,
  onSnap: PropTypes.func,
};

export default SwipeableCardStack;
