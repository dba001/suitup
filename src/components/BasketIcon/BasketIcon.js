import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, ViewPropTypes } from 'react-native';

import { Icon } from 'components/';

import defaultStyles from './styles';

class BasketIcon extends React.PureComponent {
  static defaultProps = {
    add: false,
    disabled: false,
    iconAddName: 'cart-plus',
    iconName: 'shopping-cart',
    iconType: 'fontawesome',
  };

  static propTypes = {
    add: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
    iconAddName: PropTypes.string.isRequired,
    iconName: PropTypes.string.isRequired,
    iconType: PropTypes.string.isRequired,
    basketBadge: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    BasketBadgeComponent: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.element,
    ]),
    // style: ViewPropTypes.style,
  };

  render = () => {
    const {
      add,
      basketBadge,
      BasketBadgeComponent,
      disabled,
      iconAddName,
      iconName,
      iconType,
      style,
    } = this.props;
    return (
      <View style={[defaultStyles.container, style && style.container]}>
        <Icon
          name={add ? iconAddName : iconName}
          type={iconType}
          style={
            disabled
              ? {
                  iconContainer: [
                    defaultStyles.iconContainer,
                    (style && style.iconDisabledContainer) ||
                      defaultStyles.iconDisabledContainer,
                  ],
                  icon: [
                    defaultStyles.icon,
                    (style && style.iconDisabled) || defaultStyles.iconDisabled,
                  ],
                }
              : {
                  iconContainer:
                    (style && style.iconContainer) ||
                    defaultStyles.iconContainer,
                  icon: (style && style.icon) || defaultStyles.icon,
                }
          }
        />
        <View
          style={[defaultStyles.countContainer, style && style.countContainer]}>
          {!disabled ? (
            BasketBadgeComponent ? (
              <BasketBadgeComponent basketBadge={basketBadge} />
            ) : basketBadge ? (
              <Text style={[defaultStyles.count, style && style.count]}>
                {basketBadge}
              </Text>
            ) : (
              undefined
            )
          ) : (
            undefined
          )}
        </View>
      </View>
    );
  };
}

export default BasketIcon;
