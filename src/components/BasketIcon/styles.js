import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  count: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: fontStyles.fontSize.emphasized,
    color: colors.background,
    backgroundColor: colors.transparent,
    height: 16,
    width: 16,
  },
  countContainer: {
    ...StyleSheet.absoluteFillObject,
    bottom: 7,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon: {
    backgroundColor: colors.transparent,
    color: colors.highlight,
    fontSize: 32,
    marginRight: 4,
  },
  iconContainer: {
    backgroundColor: colors.contrast,
    alignItems: 'center',
    borderRadius: 24,
    height: 48,
    width: 48,
    justifyContent: 'center',
  },
  iconDisabled: {
    color: colors.subText,
  },
});

export default styles;
