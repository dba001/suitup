import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const Icon = ({ name, type, color, onPress, size, style, ...other }) => {
  const iconType = type => {
    switch (type) {
      case 'fontawesome':
        return FontAwesomeIcon;
      case 'ion':
        return IonIcon;
      case 'material':
        return MaterialIcon;
      case 'materialcommunity':
        return MaterialCommunityIcon;
      default:
        return IonIcon;
    }
  };
  const IconComponent = iconType(type);
  const icon = (
    <IconComponent
      color={color}
      name={name}
      size={size}
      style={style && style.icon}
    />
  );
  const iconContainer = (
    <View style={style && style.iconContainer} {...other}>
      {icon}
    </View>
  );
  if (onPress) {
    return (
      <TouchableOpacity onPress={() => onPress()}>
        {iconContainer}
      </TouchableOpacity>
    );
  }
  return iconContainer;
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf([
    'fontawesome',
    'ion',
    'material',
    'materialcommunity',
    'svg',
  ]),
  color: PropTypes.string,
  onPress: PropTypes.func,
  size: PropTypes.number,
};

Icon.defaultProps = {
  color: 'white',
  size: 18,
  type: 'ion',
};

/* Export Component ==================================================================== */
export default Icon;
