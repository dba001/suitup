import React from 'react';
import PropTypes from 'prop-types';
import { Image, View } from 'react-native';

import { Icon, TouchableListItem } from 'components/';

import styles from './styles';

const SessionListItemArticle = ({
  title,
  imageSource,
  onPress,
  rightIcon,
  rightSubtitle,
  subtitle,
}) => (
  <TouchableListItem
    icon={
      <Icon name="ios-arrow-forward" style={{ icon: styles.listArrowIcon }} />
    }
    image={
      imageSource ? (
        <View style={styles.listImageContainer}>
          <Image source={imageSource} style={styles.listImage} />
        </View>
      ) : (
        <Icon
          name="md-person"
          style={{
            icon: styles.listPlaceholderIcon,
            iconContainer: styles.listPlaceholderIconContainer,
          }}
        />
      )
    }
    onPress={onPress}
    rightIcon={rightIcon}
    rightSubtitle={rightSubtitle}
    style={styles}
    subtitle={subtitle}
    title={title}
  />
);

SessionListItemArticle.propTypes = {
  title: PropTypes.string.isRequired,
  imageSource: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
  rightIcon: PropTypes.object,
  rightSubtitle: PropTypes.string,
  subtitle: PropTypes.string,
};

export default SessionListItemArticle;
