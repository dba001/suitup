/**
 * TextInput component
 *
 * style
 *   textInput: TextInput
 *   textInputContainer: View
 *   textInputDivider: View
 *   textInputIcon: Icon
 *   textInputIconSpacing: Icon
 *   textInputPlaceholder: { color: SOMECOLOR }
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

const defaultStyles = StyleSheet.create({
  textInput: {
    flex: 1,
    // fixes Android behaviour (i.e. Android adds default vertical padding to TextInput)
    paddingVertical: 0,
  },
  textInputContainer: {
    alignItems: 'center',
    backgroundColor: 'cornflowerblue',
    borderRadius: 8,
    flex: 1,
    flexDirection: 'row',
    height: 48,
    paddingHorizontal: 8,
  },
  textInputDivider: {
    backgroundColor: 'yellow',
    height: 32,
    marginHorizontal: 8,
    width: StyleSheet.hairlineWidth,
  },
  textInputIcon: {
    backgroundColor: 'transparent',
    color: 'yellow',
  },
  textInputIconSpacing: {
    paddingRight: 8,
  },
  textInputPlaceholder: {
    color: 'grey',
  },
});

/* Component ==================================================================== */
class CustomTextInput extends Component {
  static propTypes = {
    animated: PropTypes.bool,
    animationDuration: PropTypes.number,
    autoFocus: PropTypes.bool,
    clearButton: PropTypes.bool,
    iconClass: PropTypes.func,
    iconName: PropTypes.string,
    keyboardType: PropTypes.oneOf([
      'default',
      'numeric',
      'email-address',
      'phone-pad',
    ]),
    onChangeText: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    placeholder: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    selectionColor: PropTypes.string,
    value: PropTypes.string,
  };

  static defaultProps = {
    animated: false,
    animationDuration: 400,
    autoFocus: false,
    clearButton: false,
    iconClass: IonIcon,
    iconName: null,
    keyboardType: 'default', // TODO: default 'default'
    onChangeText: null,
    onSubmitEditing: null,
    placeholder: null,
    secureTextEntry: false,
    selectionColor: 'cornflowerblue',
    value: null,
  };

  constructor(props) {
    super(props);
    const animInitial = this.props.animated && !this.props.value ? 1 : 0;
    this.state = {
      AnimatedIcon: Animated.createAnimatedComponent(this.props.iconClass),
      animValue: new Animated.Value(animInitial),
      value: this.props.value,
      visible: !this.props.secureTextEntry,
    };
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.iconClass !== this.props.iconClass) {
      this.setState({
        AnimatedIcon: Animated.createAnimatedComponent(nextProps.iconClass),
      });
    }
    if (nextProps.secureTextEntry !== this.props.secureTextEntry) {
      this.setState({ visible: !nextProps.secureTextEntry });
    }
  };

  animate = (toValue, callback) => {
    const { animationDuration } = this.props;
    const { animValue } = this.state;
    Animated.timing(animValue, {
      toValue,
      duration: animationDuration,
    }).start(callback);
  };

  clear = () => {
    const { onChangeText } = this.props;
    if (this.inputRef) {
      this.setState({ value: null }, () => {
        this.inputRef.clear();
        if (onChangeText) {
          onChangeText('');
        }
      });
      if (!this.inputRef.isFocused()) {
        this.inputRef.focus();
      }
    }
  };

  focus = () => {
    this.inputRef && this.inputRef.focus();
  };

  handleChangeText = text => {
    const { onChangeText } = this.props;
    if (onChangeText) {
      this.setState({ value: text }, onChangeText(text));
    } else {
      this.setState({ value: text });
    }
  };

  handleEndEditing = ({ nativeEvent }) => {
    const { animated } = this.props;
    if (!nativeEvent.text) {
      if (animated) {
        this.animate(1);
      }
    }
    this.hidePassword();
  };

  handleFocus = () => {
    const { animated } = this.props;
    if (animated) {
      this.animate(0);
    }
  };

  hidePassword = () => {
    this.setState({ visible: false });
  };

  toggleVisibilty = () => {
    if (this.inputRef) {
      this.setState({ visible: !this.state.visible }, this.inputRef.focus);
    }
  };

  render = () => {
    const {
      autoFocus,
      clearButton,
      iconName,
      keyboardType,
      onSubmitEditing,
      placeholder,
      secureTextEntry,
      selectionColor,
      style,
    } = this.props;
    const { AnimatedIcon, animValue, value, visible } = this.state;
    const eyeIcon = visible ? 'md-eye-off' : 'md-eye';
    let keyboard = keyboardType;
    if (secureTextEntry) {
      keyboard = visible ? 'email-address' : 'default';
    }
    const empty = value ? false : true;
    return (
      <View
        style={[
          defaultStyles.textInputContainer,
          style && style.textInputContainer,
        ]}>
        {!!iconName && (
          <AnimatedIcon
            name={iconName}
            onPress={this.focus}
            style={[
              defaultStyles.textInputIcon,
              style && style.textInputIcon,
              {
                fontSize: animValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [22, 38],
                }),
              },
            ]}
          />
        )}
        {!!iconName && (
          <View
            style={[
              defaultStyles.textInputDivider,
              style && style.textInputDivider,
            ]}
          />
        )}
        <TextInput
          autoCorrect={false}
          autoFocus={autoFocus}
          keyboardType={keyboard}
          onChangeText={this.handleChangeText}
          onEndEditing={this.handleEndEditing}
          onFocus={this.handleFocus}
          onSubmitEditing={onSubmitEditing}
          placeholder={placeholder}
          placeholderTextColor={
            StyleSheet.flatten([
              defaultStyles.textInputPlaceholder,
              style && style.textInputPlaceholder,
            ]).color
          }
          ref={c => (this.inputRef = c)}
          secureTextEntry={secureTextEntry && !visible}
          selectionColor={selectionColor}
          selectTextOnFocus={true}
          style={[defaultStyles.textInput, style && style.textInput]}
          underlineColorAndroid="transparent"
          value={value}
        />
        {!empty && clearButton && (
          <TouchableOpacity onPress={this.clear}>
            <IonIcon
              name="md-close-circle"
              size={24}
              style={[
                defaultStyles.textInputIcon,
                style && style.textInputIcon,
                secureTextEntry && defaultStyles.textInputIconSpacing,
                secureTextEntry && style && style.textInputIconSpacing,
              ]}
            />
          </TouchableOpacity>
        )}
        {secureTextEntry && (
          <TouchableOpacity onPress={this.toggleVisibilty}>
            <View>
              <IonIcon
                name={eyeIcon}
                size={24}
                style={[
                  defaultStyles.textInputIcon,
                  style && style.textInputIcon,
                ]}
              />
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  };
}

/* Export Component ==================================================================== */
export default CustomTextInput;
