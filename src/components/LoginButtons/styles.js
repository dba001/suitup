import { colors, fontStyles } from 'theme/';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  loginButtonContainer: {
    alignItems: 'center',
    borderRadius: 2,
    flexDirection: 'row',
    height: 40,
    padding: 2,
  },
  loginButtonContainerFacebook: {
    backgroundColor: '#3b5998',
  },
  loginButtonContainerAmazon: {
    backgroundColor: '#4285F4',
  },
  loginButtonImageAmazon: {
    width: 200,
    height: 46,
  },
  loginButtonLabel: {
    backgroundColor: colors.transparent,
    color: colors.text,
    fontSize: fontStyles.fontSize.bodySmall,
    fontWeight: '500',
    paddingLeft: 24,
    paddingRight: 14,
  },
  logoutButton: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.contrast,
    alignItems: 'center',
    minHeight: 36,
    maxHeight: 37,
    width: 194,
    alignSelf: 'center',
    borderRadius: 3,
    padding: 1,
  },
});

export default styles;
