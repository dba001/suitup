import React from 'react';
import { View, Text } from 'react-native';
import { Touchable, Icon } from 'components/';
import styles from './styles';

const LogoutButton = ({ onPress }) => (
  <Touchable onPress={() => handleLogout(onPress)}>
    <View style={styles.logoutButton}>
      <Icon
        name="md-log-out"
        style={{
          icon: styles.listPlaceholderIcon,
          iconContainer: styles.listPlaceholderIconContainer,
        }}
      />
      <View style={styles.textContainer}>
        <Text style={styles.loginText}>Logout</Text>
      </View>
    </View>
  </Touchable>
);
const handleLogout = onPress => {
  onPress();
};
export default LogoutButton;
