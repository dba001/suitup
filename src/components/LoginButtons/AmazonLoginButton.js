import React from 'react';
import { Image, Text, View } from 'react-native';

import { Touchable } from 'components/';
import { translate } from 'helpers/';

import defaultStyles from './styles';

class AmazonLoginButton extends React.PureComponent {
  handleLogin = () => {
    const { onPressLogin } = this.props;
    onPressLogin();
  };

  render() {
    const { style } = this.props;
    return (
      <Touchable onPress={this.handleLogin}>
        <Image
          style={[
            defaultStyles.loginButtonImageAmazon,
            style && style.loginButtonImageAmazon,
          ]}
          source={require('images/LWA/btnlwa_gold_loginwithamazon.png')}
        />
      </Touchable>
    );
  }
}

export default AmazonLoginButton;
