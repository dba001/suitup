import { Platform, StyleSheet } from 'react-native';

import { colors, fontStyles } from 'theme/';

const styles = StyleSheet.create({
  buttonSpacer: {
    height: 76,
  },
  container: {
    backgroundColor: colors.background,
    flex: 1,
  },
  flatListSeparator: {
    backgroundColor: colors.separator,
    height: StyleSheet.hairlineWidth,
  },
  inputContainer: {
    alignItems: 'center',
    borderColor: colors.separator,
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    padding: 16,
  },
  textInput: {
    color: colors.text,
  },
  textInputContainer: {
    backgroundColor: colors.input.background,
    height: 36,
  },
  textInputDivider: {
    width: 0,
  },
  textInputIcon: {
    backgroundColor: colors.transparent,
    color: colors.input.placeholderText,
  },
});

export default styles;
