import React from 'react';
import { FlatList, RefreshControl, View } from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';

import { TextInput } from 'components/';

import { translate } from 'helpers/';
import { colors } from 'theme/';

import styles from './styles';

const ListView = ({
  session,
  items,
  isEdit,
  onChangeSearchText,
  onPressArticle,
  onRefresh,
  refreshing,
  renderItem,
}) => (
  <View style={styles.container}>
    <View style={styles.inputContainer}>
      <TextInput
        clearButton
        iconClass={IonIcon}
        iconName="ios-search"
        onChangeText={onChangeSearchText}
        placeholder={`${translate('SearchArticle')} ...`}
        style={styles}
      />
    </View>
    <FlatList
      data={items}
      ItemSeparatorComponent={() => <View style={styles.flatListSeparator} />}
      keyExtractor={article => article.asin}
      ListFooterComponent={() => (
        <View>
          <View style={styles.flatListSeparator} />
          <View style={styles.buttonSpacer} />
        </View>
      )}
      refreshControl={
        refreshing ? (
          <RefreshControl
            colors={[colors.highlight]}
            onRefresh={onRefresh}
            progressBackgroundColor={colors.background}
            refreshing={refreshing}
            size={RefreshControl.SIZE.LARGE}
          />
        ) : (
          undefined
        )
      }
      renderItem={renderItem}
    />
  </View>
);

export default ListView;
