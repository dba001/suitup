import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import mapValues from 'lodash/mapValues';

import styles from './styles';

const AttributeGroup = ({ attributes }) => {
  return (
    <View style={styles.attributeContainer}>
      {mapValues(attributes, (v, k) => {})}
    </View>
  );
};

AttributeGroup.propTypes = {};

export default AttributeGroup;
