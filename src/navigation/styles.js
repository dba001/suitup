import { StyleSheet } from 'react-native';
import { colors, fontStyles } from 'theme/';

const titleStyle = {
  fontSize: fontStyles.fontSize.title,
  fontWeight: fontStyles.fontWeight.emphasized,
  color: colors.text,
};

const styles = StyleSheet.create({
  headerTitleContainerStyle: {
    ...StyleSheet.absoluteFill,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerTitleStyle: {
    ...titleStyle,
  },
});

export default styles;
