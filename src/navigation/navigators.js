import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import PopupMenu from 'scenes/PopupMenu';

import RootContainer from 'scenes/Root/Container';
import SelectCategoriesContainer from 'scenes/SelectCategories/Container';
import SetupUserContainer from 'scenes/SetupUser/Container';
import ArticleContainer from 'scenes/Article/Container';
import BasketContainer from '../scenes/Basket/Container';
import WebViewContainer from 'scenes/WebView';

import SelectCategoriesButton from 'scenes/SelectCategories/SelectCategoriesButton';

import { translate } from 'helpers/';

import { createStackNavigator } from 'react-navigation-stack';
import styles from './styles';

const setupStack = createStackNavigator({
  setupuser: {
    screen: SetupUserContainer,
    navigationOptions: {
      header: null,
    },
  },
});

const appStack = createStackNavigator(
  {
    root: {
      screen: RootContainer,
      navigationOptions: {
        title: 'SuitUp',
        // headerRight: <PopupMenu />,
        headerLeft: <SelectCategoriesButton />,
      },
    },
    selectCategories: {
      screen: SelectCategoriesContainer,
      navigationOptions: {
        title: translate('Categories'),
      },
    },
    article: {
      screen: ArticleContainer,
      navigationOptions: {
        title: translate('Article'),
      },
    },
    basket: {
      screen: BasketContainer,
      navigationOptions: {
        title: translate('Basket'),
      },
    },
    webView: {
      screen: WebViewContainer,
      navigationOptions: {
        title: translate('Amazon'),
      },
    },
  },
  {
    initialRouteName: 'root',
    defaultNavigationOptions: {
      headerTitleContainerStyle: styles.headerTitleContainerStyle,
      headerTitleStyle: styles.headerTitleStyle,
    },
  },
);

const RootNavigator = createSwitchNavigator(
  {
    setup: setupStack,
    app: appStack,
  },
  {
    initialRouteName: 'app',
    defaultNavigationOptions: {
      headerTitleContainerStyle: styles.headerTitleContainerStyle,
      headerTitleStyle: styles.headerTitleStyle,
    },
  },
);

export default createAppContainer(RootNavigator);
