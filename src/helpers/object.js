import { clone, curry, setWith, updateWith } from 'lodash/fp';
import get from 'lodash/get';
import unset from 'lodash/unset';
import isEmpty from 'lodash/isEmpty';
import { CONFIG } from 'constants/';
import uuidv1 from 'uuid/v1';

export const getId = object => object && object[CONFIG.idName];

/**
 * convert objectList
 * @param objectList - [{'@id': 'id1'}, {'@id': 'id2'}]
 * @param key - '@id'
 * @returns {*} - {'id1': {'@id':'id1'}, 'id2': {'@id':'id2'}}
 */
export const objectListToIdMap = (objectList, key = CONFIG.idName) => {
  const objects = objectList.map(item => [item[key], item]);
  return Object.assign(
    {},
    ...objects.map(([key, value]) => ({ [key]: value })),
  );
};

/**
 * convert objectList
 * @param objectList - [{'@id': 'id1'}, {'@id': 'id2'}, 'id3']
 * @param key - '@id'
 * @returns [] - ['id1', 'id2', 'id3']
 */
export const objectListToIdList = (objectList, key = CONFIG.idName) =>
  objectList.map(o => (typeof o === 'string' ? o : o[key]));

export const addObjects = (state, payload) => {
  if (Array.isArray(payload)) {
    const payloadState = objectListToIdMap(payload);
    return { ...state, ...payloadState };
  } else {
    const id = getId(payload);
    return { ...state, ...{ [id]: payload } };
  }
};

export const removeObjects = (state, ids) => {
  let stateCopy = Object.assign({}, state);
  if (Array.isArray(ids)) {
    ids.map(id => delete stateCopy[id]);
  } else {
    delete stateCopy[ids];
  }
  return stateCopy;
};

export const uuid = () => uuidv1();

export const getIn = curry((path, state) => get(state, path));

export const setIn = curry((path, value, state) =>
  path ? setWith(clone, path, value, clone(state)) : state,
);

export const updateIn = curry((path, updater, state) =>
  path ? updateWith(clone, path, updater, clone(state)) : state,
);

export const deleteIn = (path, state) => {
  if (isEmpty(path)) return state;
  const valueAtPath = get(state, path);
  if (valueAtPath) {
    const stateWithClonedPath = setIn(path, valueAtPath, state);
    unset(stateWithClonedPath, path);
    return stateWithClonedPath;
  }
  return state;
};
