import { NativeModules, Platform } from 'react-native';

const replaceUnderscore = localeString => localeString.replace('_', '-');

export const getLocale = Platform.select({
  android: () =>
    replaceUnderscore(NativeModules.I18nManager.localeIdentifier || ''),
  ios: () =>
    replaceUnderscore(NativeModules.SettingsManager.settings.AppleLocale || ''),
});

export const getLocaleFallback = () => getLocale().substring(0, 2) || 'en';
