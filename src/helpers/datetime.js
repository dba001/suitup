const getTimeString = timeMS => {
  const date = new Date(timeMS);
  const ms = zeroPad(date.getMilliseconds().toString(), 3);
  const secs = zeroPad(date.getSeconds().toString(), 2);
  const mins = zeroPad(date.getMinutes().toString(), 2);
  return `${mins}:${secs}.${ms.slice(0, 2)}`;
};

const zeroPad = (str, desiredMinLength) => {
  if (str.length < desiredMinLength) {
    return zeroPad(`0${str}`, desiredMinLength);
  }
  return str;
};

export { getTimeString, zeroPad };
