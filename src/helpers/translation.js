import I18n from 'react-native-i18n';

import { getLocaleFallback } from './locale';
import translations from '../static/translations';

I18n.fallbacks = true;
I18n.translations = translations;

export const translate = (str, obj) => I18n.t(str, obj);

export const cdbTranslateWithLocale = locale => cdbObject => attributeName =>
  cdbObject[`${attributeName}_${locale}`] || '';

export const cdbTranslate = cdbTranslateWithLocale(getLocaleFallback());
