const comparePlayersByBestResult = (player, other) => {
  const playerBest = player.bestAttempt.result_value;
  const otherBest = other.bestAttempt.result_value;
  if (playerBest < otherBest) {
    return -1;
  }
  if (playerBest > otherBest) {
    return 1;
  }
  return 0;
};

const comparePlayersByName = (player, other) => {
  const playerName = `${player.firstname} ${player.lastname}`;
  const otherName = `${other.firstname} ${other.lastname}`;
  if (playerName < otherName) {
    return -1;
  }
  if (playerName > otherName) {
    return 1;
  }
  return 0;
};

export { comparePlayersByBestResult, comparePlayersByName };
