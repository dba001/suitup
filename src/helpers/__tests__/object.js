import { deleteIn, setIn, updateIn } from './../object';

describe('>>> H E L P E R S --- REDUX STATE TRANSFORMATION', () => {
  const state = {
    branch1: { subBranch1: 'someValue' },
    branch2: { subBranch2: 'otherValue' },
  };

  test('+++ deleteIn', () => {
    const resultState = deleteIn('branch2.subBranch2', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: {},
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ deleteIn no path', () => {
    const resultState = deleteIn('branch2.subBranch3', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
    });
    expect(resultState).toBe(state);
  });

  test('+++ setIn first level', () => {
    const resultState = setIn('branch3', 'anyValue', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
      branch3: 'anyValue',
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ setIn', () => {
    const resultState = setIn('branch2.subBranch2', 'anyValue', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'anyValue' },
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ setIn nothing', () => {
    const resultState = setIn('', '', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
    });
    expect(resultState).toBe(state);
  });
  test('+++ setIn no path', () => {
    const resultState = setIn(undefined, '', state);
    expect(resultState).toBe(state);
  });
  test('+++ setIn undefined value', () => {
    const resultState = setIn('', undefined, state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
    });
    expect(resultState).toBe(state);
  });
  test('+++ setIn new path', () => {
    const resultState = setIn('branch2.subBranch3', 'newValue', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue', subBranch3: 'newValue' },
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ setIn new path in subbranch', () => {
    const resultState = setIn(
      'branch2.subBranch3.subSubBranch',
      'newValue',
      state,
    );
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: {
        subBranch2: 'otherValue',
        subBranch3: { subSubBranch: 'newValue' },
      },
    });
    expect(resultState).not.toBe(state);
  });

  test('+++ updateIn new path', () => {
    const resultState = updateIn('branch3', v => 'anyValue', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
      branch3: 'anyValue',
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ updateIn new path set object', () => {
    const resultState = updateIn(
      'branch3',
      v => ({
        ...v,
        subBranch3: 'newValue',
      }),
      state,
    );
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
      branch3: { subBranch3: 'newValue' },
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ updateIn add value to object', () => {
    const resultState = updateIn(
      'branch2',
      v => ({
        ...v,
        subBranch3: 'newValue',
      }),
      state,
    );
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue', subBranch3: 'newValue' },
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ updateIn', () => {
    const resultState = updateIn('branch2.subBranch2', v => 'anyValue', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'anyValue' },
    });
    expect(resultState).not.toBe(state);
  });
  test('+++ updateIn nothing', () => {
    const resultState = updateIn('', '', state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
    });
    expect(resultState).toBe(state);
  });
  test('+++ updateIn no path', () => {
    const resultState = updateIn(undefined, '', state);
    expect(resultState).toBe(state);
  });
  test('+++ setIn undefined value', () => {
    const resultState = updateIn('', undefined, state);
    expect(resultState).toEqual({
      branch1: { subBranch1: 'someValue' },
      branch2: { subBranch2: 'otherValue' },
    });
    expect(resultState).toBe(state);
  });
});
