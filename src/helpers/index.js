import { getTimeString, zeroPad } from './datetime';
import { getLocale, getLocaleFallback } from './locale';
import { normalize } from './scaling';
import { comparePlayersByBestResult, comparePlayersByName } from './sorting';
import { cdbTranslate, cdbTranslateWithLocale, translate } from './translation';
import {
  uuid,
  getId,
  objectListToIdMap,
  objectListToIdList,
  addObjects,
  removeObjects,
  deleteIn,
  getIn,
  setIn,
  updateIn,
} from './object';

export {
  cdbTranslate,
  cdbTranslateWithLocale,
  comparePlayersByBestResult,
  comparePlayersByName,
  getLocale,
  getLocaleFallback,
  getTimeString,
  normalize,
  translate,
  zeroPad,
  // object helpers
  uuid,
  getId,
  objectListToIdMap,
  objectListToIdList,
  addObjects,
  removeObjects,
  deleteIn,
  getIn,
  setIn,
  updateIn,
};
