import { colors } from './colors';
import fontStyles from './fontStyles';

export { colors, fontStyles };
