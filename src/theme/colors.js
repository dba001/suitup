const highlight = '#FF6D00';
const highlightRGBA = 'rgba(255, 109, 0, 0.6)';
const background = '#FFFFFF';
const contrast = '#E0E0E0';
const scim = '#FFFFFFA0';
const text = '#1d1d1d';
const subText = '#5D5D5D';
const transparent = 'transparent';

export const colors = Object.freeze({
  highlight,
  highlightRGBA,
  background,
  contrast,
  scim,
  text,
  subText,
  transparent,
  input: {
    background: contrast,
    text: text,
    placeholderText: subText,
  },
  separator: '#999999',
  action: {
    like: '#4caf50',
    dislike: '#de6d77',
    undo: '#f6bf26',
  },
});
