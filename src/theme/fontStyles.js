/**
 * Heading - 24px
 * Title - 21px (semibold)
 * Body - 16px (regular)
 * Body-Emphasized - 16px (semibold)
 * Body-Small - 14px (regular)
 * Body-Small-Emphasized - 14px (semibold)
 * Caption - 12px (regular)
 */

import { normalize } from 'helpers/';

const fontSize = Object.freeze({
  heading: 24,
  title: 21,
  subTitle: 16,
  body: 14,
  bodySmall: 12,
  caption: 10,
});

const fontWeight = Object.freeze({
  regular: '400',
  emphasized: '600',
});

const fontStyles = { fontSize, fontWeight };

export default fontStyles;
