import filter from 'lodash/filter';
import invert from 'lodash/invert';
import browseNodes from '../aws/browseNodes';
import { updateIn } from 'helpers';

import { currentArticleSelector, nextArticleSelector } from './selectors';
import { setSessionArticle } from './sessions';
import { REQUEST_ARTICLES_FULLFILLED } from './articles';

// Actions
export const TOGGLE_CATEGORY = 'TOGGLE_CATEGORY';

const inverseBrowseNodes = invert(browseNodes);
export const initialState = {
  [browseNodes.SUITS]: {
    browseNode: browseNodes.SUITS,
    browseKey: inverseBrowseNodes[browseNodes.SUITS],
    itemPage: 1,
    selected: true,
    source: require('images/category_suits.jpg'),
  },
  [browseNodes.SHIRTS]: {
    browseNode: browseNodes.SHIRTS,
    browseKey: inverseBrowseNodes[browseNodes.SHIRTS],
    itemPage: 1,
    selected: true,
    source: require('images/category_shirts.jpg'),
  },
  [browseNodes.BAGS]: {
    browseNode: browseNodes.BAGS,
    browseKey: inverseBrowseNodes[browseNodes.BAGS],
    itemPage: 1,
    selected: true,
    source: require('images/category_bags.jpg'),
  },
  [browseNodes.PANTS]: {
    browseNode: browseNodes.PANTS,
    browseKey: inverseBrowseNodes[browseNodes.PANTS],
    itemPage: 1,
    selected: true,
    source: require('images/category_pants.jpg'),
  },
  [browseNodes.COATS]: {
    browseNode: browseNodes.COATS,
    browseKey: inverseBrowseNodes[browseNodes.COATS],
    itemPage: 1,
    selected: true,
    source: require('images/category_coats.jpg'),
  },
  [browseNodes.JEANS]: {
    browseNode: browseNodes.JEANS,
    browseKey: inverseBrowseNodes[browseNodes.JEANS],
    itemPage: 1,
    selected: true,
    source: require('images/category_jeans.jpg'),
  },
  [browseNodes.UNDERWEAR]: {
    browseNode: browseNodes.UNDERWEAR,
    browseKey: inverseBrowseNodes[browseNodes.UNDERWEAR],
    itemPage: 1,
    selected: true,
    source: require('images/category_underwear.jpg'),
  },
  [browseNodes.SHOES]: {
    browseNode: browseNodes.SHOES,
    browseKey: inverseBrowseNodes[browseNodes.SHOES],
    itemPage: 1,
    selected: true,
    source: require('images/category_shoes.jpg'),
  },
};

// Reducer
// reducer name = categories
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TOGGLE_CATEGORY: {
      // one category has to be toggled
      if (
        filter(Object.values(state), category => category.selected).length ===
          1 &&
        state[action.payload].selected
      )
        return state;
      return updateIn(
        [action.payload],
        category => ({ ...category, selected: !category.selected }),
        state,
      );
    }
    case REQUEST_ARTICLES_FULLFILLED: {
      const browseNode = action.payload.browseNode;
      return updateIn(
        [browseNode],
        category => ({ ...category, itemPage: category.itemPage + 1 }),
        state,
      );
    }
    default:
      return state;
  }
}

// Action Creators
export const _toggleCategory = category => ({
  type: TOGGLE_CATEGORY,
  payload: category,
});

export const toggleCategory = category => (dispatch, getState) => {
  const currentArticle = currentArticleSelector(getState());
  const nextArticle = nextArticleSelector(getState());
  if (
    currentArticle?.browseNode === category ||
    nextArticle?.browseNode === category
  ) {
    dispatch(setSessionArticle());
  }
  dispatch(_toggleCategory(category));
};
