import mapValues from 'lodash/mapValues';
import { setIn, updateIn } from 'helpers/';
import { selectVariationInfo, selectArticle } from 'selectors/';

import { REQUEST_ARTICLES_FULLFILLED } from './articles';

// Actions
export const SET_VARIATION_INFO = 'SET_VARIATION_INFO';
export const REQUEST_ARTICLE_INFO = 'REQUEST_ARTICLE_INFO';
export const REQUEST_ARTICLE_INFO_FULLFILLED =
  'REQUEST_ARTICLE_INFO_FULLFILLED';

const initialState = {};

// Reducer
// reducer name = variationInfo
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_VARIATION_INFO:
      return setIn(`${action.meta.asin}`, action.payload, state);
    case REQUEST_ARTICLE_INFO_FULLFILLED:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

// Action Creators
export const variationInfoSet = (asin, info) => ({
  type: SET_VARIATION_INFO,
  payload: info,
  meta: {
    asin,
  },
});

export const setVariationInfo = (asin, info) => dispatch =>
  dispatch(variationInfoSet(asin, info));
