import { Logger } from '@aws-amplify/core';
import { filter, map } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import { fillQueues, POP_ARTICLE } from '../categoryQueues';
import { SET_FIRST_APP_USAGE } from '../settings';
import {
  getCategoryQueuesBelowThresholdById,
  categoryQueuesAreFilled,
  isCategoryFetching,
} from '../selectors';

export const watchCategoryQueues = (action$, state$) =>
  action$.pipe(
    ofType(POP_ARTICLE, SET_FIRST_APP_USAGE),
    filter(() => !isCategoryFetching(state$.value)),
    filter(() => getCategoryQueuesBelowThresholdById(state$.value)?.length > 0),
    filter(() => !categoryQueuesAreFilled(state$.value)),
    map(() => fillQueues()),
  );
