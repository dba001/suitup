import cloneDeep from 'lodash/cloneDeep';
import random from 'lodash/random';
import { Logger } from '@aws-amplify/core';
import { ofType } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, filter, map, take } from 'rxjs/operators';

import { LIKE_ARTICLE, DISLIKE_ARTICLE } from '../articles';
import {
  likeSessionArticle,
  dislikeSessionArticle,
  setSessionArticle,
} from '../sessions';
import {
  areArticlesSet,
  categoryQueuesAreFilled,
  getCategoryQueues,
} from '../selectors';

const logger = new Logger('articleQueue', 'INFO');

export const selectRandomArticles = (state, numArticle) => {
  const categoryQueues = cloneDeep(getCategoryQueues(state));
  const numCategoryQueues = categoryQueues.length - 1;

  const randoms = Array(numArticle)
    .fill()
    .map(() => random(numCategoryQueues));
  return randoms.map(rnd => categoryQueues?.[rnd].shift());
};

// We observe the state and wait until the category queues
// are filled to to set current and next article in session
export const setSessionArticleEpic = (action$, state$) =>
  state$.pipe(
    filter(categoryQueuesAreFilled),
    filter(state => !areArticlesSet(state)),
    map(() => setSessionArticle()),
    take(1),
    catchError(error => {
      logger.info(
        `setSessionArticle: An error occurred and the stream is interrupted ${error}`,
      );
      // return of(fillArticleQueueFailed(error));
      return of({ type: 'TEST' });
    }),
  );

export const likeArticleEpic = action$ =>
  action$.pipe(
    ofType(LIKE_ARTICLE),
    map(() => likeSessionArticle()),
  );

export const dislikeArticleEpic = action$ =>
  action$.pipe(
    ofType(DISLIKE_ARTICLE),
    map(() => dislikeSessionArticle()),
  );
