import { Logger } from '@aws-amplify/core';
import { ofType } from 'redux-observable';
import { from, of, throwError } from 'rxjs';
import { catchError, last, map, mergeMap } from 'rxjs/operators';

import itemAPI from '../../aws/itemAPI';

import {
  REQUEST_ARTICLES,
  requestArticlesFailed,
  requestArticlesFullfilled,
} from '../articles';
import { hasArticle, getItemPageForCategory } from '../selectors';

const logger = new Logger('articles', 'INFO');

export const requestArticlesEpic = (action$, state$) =>
  action$.pipe(
    ofType(REQUEST_ARTICLES),
    mergeMap(requestAction$ => requestAction$.payload),
    // we convert our promise to an observable to catch any error
    // in seperate stream, so our main stream will not be interrupted
    mergeMap(browseNode =>
      from(
        itemAPI.apperalSearch(
          browseNode,
          getItemPageForCategory(state$.value, browseNode),
        ),
      ).pipe(
        mergeMap(articleInfo => {
          logger.debug(
            'requestArticleEpic: getting articleInfo ->',
            articleInfo,
          );
          if (!articleInfo) {
            logger.debug(
              `requestArticleEpic: No articleInfo for browseNode ${browseNode}`,
            );
            return throwError(
              new Error(`No articleInfo for browseNode ${browseNode}`),
            );
          }
          let articles = [];
          articleInfo.forEach(({ article }) => {
            // when we find the current asin in state we dont want to
            // add it to another categoryQueue
            // Only works for

            if (!hasArticle(state$.value, article.asin)) {
              articles.push(article);
            }
          });
          const payload = of({ browseNode, articles });
          return payload;
        }),
        catchError(error => {
          return of(error);
        }),
      ),
    ),
    map(payload => {
      // The inner observable can return an error this will be caught here
      if (payload instanceof Error) return requestArticlesFailed(payload);
      return requestArticlesFullfilled(payload);
    }),
    catchError(error => {
      logger.debug(
        `requestArticleEpic: An error occurred and the stream is interrupted ${error}`,
      );
      return of(requestArticlesFailed(error));
    }),
  );
