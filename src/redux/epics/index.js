import { combineEpics } from 'redux-observable';

import { requestArticlesEpic } from './articles';
import { watchCategoryQueues } from './categoryQueues';
import {
  dislikeArticleEpic,
  likeArticleEpic,
  setSessionArticleEpic,
} from './sessions';

export default combineEpics(
  dislikeArticleEpic,
  likeArticleEpic,
  requestArticlesEpic,
  setSessionArticleEpic,
  watchCategoryQueues,
);
