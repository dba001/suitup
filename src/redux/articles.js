import filter from 'lodash/filter';
import map from 'lodash/map';
import { Logger, API, graphqlOperation } from '@aws-amplify/core';
import {
  addObjects,
  removeObjects,
  objectListToIdList,
  updateIn,
} from 'helpers/';

import * as queries from '../graphql/queries';

import { putArticle } from './categoryQueues';
import { removeFromSession } from './sessions';
import {
  getActiveSessionId,
  getActiveSessionLastArticleAddedId,
  hasArticle,
  selectArticle,
} from 'selectors';

const logger = new Logger('redux/articles', 'INFO');

// Actions
export const ADD_ARTICLE = 'ADD_ARTICLE';
export const CHANGE_ARTICLE = 'CHANGE_ARTICLE';
export const REMOVE_ARTICLE = 'REMOVE_ARTICLE';
export const LIKE_ARTICLE = 'LIKE_ARTICLE';
export const DISLIKE_ARTICLE = 'DISLIKE_ARTICLE';
export const REQUEST_ARTICLES = 'REQUEST_ARTICLES';
export const REQUEST_ARTICLES_FULLFILLED = 'REQUEST_ARTICLES_FULLFILLED';
export const REQUEST_ARTICLES_FAILED = 'REQUEST_ARTICLES_FAILED';
export const HANDLE_ARTICLE_ERROR = 'ARTICLE_ERROR';

const initialState = {};

// Reducer
// reducer name = articles
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST_ARTICLES_FULLFILLED:
      return addObjects(state, action.payload.articles);
    case ADD_ARTICLE:
      return addObjects(state, action.payload);
    case CHANGE_ARTICLE:
      return updateIn(
        `${action.payload.asin}`,
        article => ({
          ...article,
          ...action.payload.value,
        }),
        state,
      );
    case REMOVE_ARTICLE: {
      return removeObjects(state, action.payload);
    }
    default:
      return state;
  }
}

// Action Creators
export const addArticles = articles => ({
  type: ADD_ARTICLE,
  payload: articles,
});

export const changeArticle = (asin, value) => ({
  type: CHANGE_ARTICLE,
  payload: { asin, value },
});

export const removeArticles = asins => ({
  type: REMOVE_ARTICLE,
  payload: asins,
});

export const handleArticleError = msg => ({
  type: HANDLE_ARTICLE_ERROR,
  payload: msg,
});

export const requestArticles = browseNodes => ({
  type: REQUEST_ARTICLES,
  payload: browseNodes,
});

const _requestArticlesFullfilled = payload => ({
  type: REQUEST_ARTICLES_FULLFILLED,
  payload,
});

const filterDuplicateArticles = (state, articles) => {
  const uniques = filter(articles, article => !hasArticle(state, article.id));
  logger.debug(
    `filterDuplicateArticles: origin: ${articles.length}, filtered: ${uniques.length}`,
  );
  return uniques;
};

export const requestArticlesFullfilled = payload => (dispatch, getState) => {
  const uniques = filterDuplicateArticles(getState(), payload.articles);
  payload.ids = map(uniques, article => article.id);
  payload.articles = uniques;
  dispatch(_requestArticlesFullfilled(payload));
};

export const requestArticlesFailed = payload => ({
  type: REQUEST_ARTICLES_FAILED,
  payload,
});

export const likeCurrentArticle = () => ({ type: LIKE_ARTICLE });

export const dislikeCurrentArticle = () => ({ type: DISLIKE_ARTICLE });

// TODO: THERE IS A MUCH NICER WAY TO UNDO THINS LOOK AT REDUX BEST PRACTICES
export const undoCurrentArticle = () => (dispatch, getState) => {
  const state = getState();
  const sessionId = getActiveSessionId(state);
  const asin = getActiveSessionLastArticleAddedId(state);
  const article = selectArticle(state, asin);
  if (asin) {
    dispatch(putArticle(article.browseNode, asin));
    dispatch(removeFromSession(sessionId, asin));
  }
};

// GRAPHQL Example
// export const requestArticlesGraphql = () => async dispatch => {
//     const articles = await API.graphql(graphqlOperation(queries.listArticles));
//     if (articles?.data?.listArticles?.items) {
//         dispatch(addArticles(articles.data.listArticles.items));
//         const ids = objectListToIdList(articles);
//         dispatch(pushArticles(ids));
//     } else {
//         console.log('requestArticlesGraphql: No articles were fetched.');
//     }
// };
