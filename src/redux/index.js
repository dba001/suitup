import { combineReducers } from 'redux';
import articles from './articles';
import categoryQueues from './categoryQueues';
import baskets from './baskets';
import categories from './categories';
import requests from './requests';
import sessions from './sessions';
import settings from './settings';
import users from './users';
import variationInfo from './variationInfo';

export rootEpic from './epics';

// Merge reducers in one object
export const rootReducer = combineReducers({
  // reducer to handle articles
  articles,
  // Queues to store articles by category
  categoryQueues,
  // user baskets to buy articles
  baskets,
  // apparel categories to be selected
  categories,
  // reducer to handle async requests
  requests,
  // reducer to handle user rating sessions
  sessions,
  // saving settings, or current app state
  settings,
  // user information
  users,
  // All article variations we have so far
  variationInfo,
});
