import { Logger } from '@aws-amplify/core';

import union from 'lodash/union';
import mapValues from 'lodash/mapValues';
import { updateIn, uuid, removeObjects } from '../helpers';
import {
  currentArticleSelector,
  getActiveSession,
  getActiveSessionId,
} from 'selectors';

import { setActiveSession } from './settings';
import { isDefaultUser } from 'users/';
import { createBasket } from './baskets';
import { popArticle, popRandomArticle } from './categoryQueues';

// Actions
export const NEW_SESSION = 'NEW_SESSION';
export const ADD_SESSION_LIKE = 'ADD_SESSION_LIKE';
export const ADD_SESSION_DISLIKE = 'ADD_SESSION_DISLIKE';
export const REMOVE_FROM_SESSION = 'REMOVE_FROM_SESSION';
export const REMOVE_SESSION = 'REMOVE_SESSION';
export const AUTHENTICATE_SESSIONS = 'AUTHENTICATE_SESSIONS';
export const SET_NEXT_ARTICLE = 'SET_NEXT_ARTICLE';

const initialState = {};

const logger = new Logger('sessions', 'INFO');

// Reducer
// reducer name = sessions
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case NEW_SESSION: {
      const { sessionId, date, userId, audience, basket } = action.payload;
      const likes = [];
      const dislikes = [];
      const lastArticleAdded = undefined;
      return {
        ...state,
        [sessionId]: {
          userId,
          date,
          audience,
          basket,
          likes,
          dislikes,
          lastArticleAdded,
        },
      };
    }
    case ADD_SESSION_LIKE: {
      const sessionId = action.meta.sessionId;
      const { asin, nextAsin } = action.payload;
      return updateIn(
        `${sessionId}`,
        session => ({
          ...session,
          likes: union([...session.likes, asin]),
          lastArticleAdded: asin,
          currentArticle: session.nextArticle,
          nextArticle: nextAsin,
        }),
        state,
      );
    }
    case ADD_SESSION_DISLIKE: {
      const sessionId = action.meta.sessionId;
      const { asin, nextAsin } = action.payload;
      return updateIn(
        `${sessionId}`,
        session => ({
          ...session,
          dislikes: union([...session.dislikes, asin]),
          lastArticleAdded: asin,
          currentArticle: session.nextArticle,
          nextArticle: nextAsin,
        }),
        state,
      );
    }
    case REMOVE_FROM_SESSION: {
      const sessionId = action.meta.sessionId;
      const session = state[sessionId];
      const asin = action.payload;
      if (asin === session.likes[session.likes.length - 1]) {
        return {
          ...state,
          [sessionId]: {
            ...session,
            likes: session.likes.slice(0, -1),
            lastArticleAdded: undefined,
          },
        };
      } else {
        return {
          ...state,
          [sessionId]: {
            ...session,
            dislikes: session.dislikes.slice(0, -1),
            lastArticleAdded: undefined,
          },
        };
      }
    }
    case REMOVE_SESSION: {
      return removeObjects(state, action.payload);
    }
    case AUTHENTICATE_SESSIONS: {
      return mapValues(state, session => {
        if (isDefaultUser(session.userId)) {
          const s = { ...session, userId: action.payload };
          return s;
        }
        return session;
      });
    }
    case SET_NEXT_ARTICLE: {
      const sessionId = action.meta.sessionId;
      let { nextArticle, currentArticle } = action.payload;
      // When we dont provide a currentAsin state.nextAsin
      // should be there and it becomes the currentAsin
      if (!currentArticle) currentArticle = state?.nextAsin;
      return updateIn(
        `${sessionId}`,
        session => {
          return { ...session, nextArticle, currentArticle };
        },
        state,
      );
    }
    default:
      return state;
  }
}

// Action Creators
export const newSession = (sessionId, date, userId, audience, basket) => ({
  type: NEW_SESSION,
  payload: {
    sessionId,
    date,
    userId,
    audience,
    basket,
  },
});

export const addSessionLike = (sessionId, asin, nextAsin) => ({
  type: ADD_SESSION_LIKE,
  payload: { asin, nextAsin },
  meta: {
    sessionId,
  },
});

export const addSessionDislike = (sessionId, asin, nextAsin) => ({
  type: ADD_SESSION_DISLIKE,
  payload: { asin, nextAsin },
  meta: {
    sessionId,
  },
});

const likingHelper = () => (dispatch, getState) => {
  const state = getState();
  const article = currentArticleSelector(state);
  const sessionId = getActiveSessionId(state);
  try {
    const nextArticle = dispatch(popRandomArticle());

    return {
      asin: article.asin,
      browseNode: article.browseNode,
      nextAsin: nextArticle.asin,
      sessionId: sessionId,
    };
  } catch (error) {
    dispatch({
      type: 'NO_MORE_ARTICLES_IN_QUEUE',
    });
  }
};
// TODO: Do we need this or should the article popped beforehand
export const likeSessionArticle = () => dispatch => {
  const { asin, browseNode, nextAsin, sessionId } = dispatch(likingHelper());
  dispatch(addSessionLike(sessionId, asin, nextAsin));
  dispatch(popArticle(browseNode));
};
export const dislikeSessionArticle = () => dispatch => {
  const { asin, browseNode, nextAsin, sessionId } = dispatch(likingHelper());
  dispatch(addSessionDislike(sessionId, asin, nextAsin));
  dispatch(popArticle(browseNode));
};

export const removeFromSession = (sessionId, asin) => ({
  type: REMOVE_FROM_SESSION,
  payload: asin,
  meta: {
    sessionId,
  },
});

export const removeSession = sessionId => ({
  type: REMOVE_SESSION,
  payload: sessionId,
});

export const setSessionArticle = () => (dispatch, getState) => {
  const sessionId = getActiveSessionId(getState());
  try {
    const currentArticle = dispatch(popRandomArticle());
    const nextArticle = dispatch(popRandomArticle());
    dispatch({
      type: SET_NEXT_ARTICLE,
      payload: {
        nextArticle: nextArticle.asin,
        currentArticle: currentArticle.asin,
      },
      meta: { sessionId },
    });
  } catch (error) {
    dispatch({
      type: 'NO_MORE_ARTICLES_IN_QUEUE',
    });
  }
};

export const createNewSession = (userId, audience) => (dispatch, getState) => {
  const sessionId = uuid(userId);
  const date = new Date();

  const createBasketAction = dispatch(createBasket());
  dispatch(
    newSession(sessionId, date, userId, audience, createBasketAction.payload),
  );
  dispatch(setActiveSession(sessionId));
  return getActiveSession(getState());
};

//export const putSession = (userId, session) => dispatch => {};

export const authenticateSessions = userId => ({
  type: AUTHENTICATE_SESSIONS,
  payload: userId,
});
