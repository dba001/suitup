import { Logger } from '@aws-amplify/core';

import { uuid, deleteIn, setIn, updateIn } from './../helpers/';

// Actions
export const CREATE_BASKET = 'CREATE_BASKET';
export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';
export const ADD_REMOTE_BASKET = 'ADD_REMOTE_BASKET';
export const REMOVE_REMOTE_BASKET = 'REMOVE_REMOTE_BASKET';
export const CHANGE_AMOUNT = 'CHANGE_AMOUNT';

export const initialState = {};
export const initialBasket = { items: {}, remote: undefined };

const logger = new Logger('baskets', 'INFO');

// Reducer
// reducer name = baskets
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_BASKET:
      return setIn(
        `${action.payload}`,
        { ...initialBasket, id: action.payload },
        state,
      );
    case ADD_TO_BASKET:
      if (state[action.meta.basket]) {
        return setIn(
          `${action.meta.basket}.items.${action.payload}`,
          {
            asin: action.payload,
            ...action.meta,
          },
          state,
        );
      }
      return state;
    case REMOVE_FROM_BASKET:
      return deleteIn(`${action.meta.basket}.items.${action.payload}`, state);
    case ADD_REMOTE_BASKET:
      if (state[action.meta.basket]) {
        return updateIn(
          `${action.meta.basket}.remote`,
          remoteBasket => ({ ...remoteBasket, ...action.payload }),
          state,
        );
      }
      return state;
    case REMOVE_REMOTE_BASKET:
      if (state[action.meta.basket]) {
        return deleteIn(`${action.meta.basket}.remote`, state);
      }
      return state;
    case CHANGE_AMOUNT:
      if (state[action.meta.basket]) {
        return updateIn(
          `${action.meta.basket}.items.${action.meta.asin}`,
          item => ({
            ...item,
            amount: action.payload,
          }),
          state,
        );
      }
      return state;
    default:
      return state;
  }
}

// Action Creators
export const createBasket = () => ({
  type: CREATE_BASKET,
  payload: uuid(),
});

export const addToBasket = (basketId, parentAsin, asin, amount = 1) => ({
  type: ADD_TO_BASKET,
  payload: asin,
  meta: {
    amount: amount,
    basket: basketId,
    parentAsin: parentAsin,
  },
});

export const removeFromBasket = (basketId, asin) => ({
  type: REMOVE_FROM_BASKET,
  payload: asin,
  meta: {
    basket: basketId,
  },
});

export const addRemoteBasket = (basketId, remoteBasket) => ({
  type: ADD_REMOTE_BASKET,
  payload: remoteBasket,
  meta: {
    basket: basketId,
  },
});

export const removeRemoteBasket = basketId => ({
  type: REMOVE_REMOTE_BASKET,
  meta: {
    basket: basketId,
  },
});

export const changeAmount = (basketId, asin, amount) => ({
  type: CHANGE_AMOUNT,
  payload: amount,
  meta: {
    basket: basketId,
    asin: asin,
  },
});
