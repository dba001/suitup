import { CONSTANTS } from 'constants/';

// Actions
export const SET_FIRST_APP_USAGE = 'SET_FIRST_APP_USAGE';
export const SET_ACTIVE_SESSION = 'SET_ACTIVE_SESSION';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const RESET_SETTINGS = 'SET_ACTIVE_SESSION';

const initialState = {
  [CONSTANTS.SETTINGS.isFirstAppUsage]: true,
  [CONSTANTS.SETTINGS.activeSession]: '',
  [CONSTANTS.SETTINGS.currentUser]: '',
};

// Reducer
// reducer name = settings
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_FIRST_APP_USAGE:
      return { ...state, [CONSTANTS.SETTINGS.isFirstAppUsage]: false };
    case SET_ACTIVE_SESSION:
      return {
        ...state,
        [CONSTANTS.SETTINGS.activeSession]: action.payload,
      };
    case SET_CURRENT_USER:
      return {
        ...state,
        [CONSTANTS.SETTINGS.currentUser]: action.payload,
      };
    case RESET_SETTINGS:
      return { ...initialState };
    default:
      return state;
  }
}

// Action Creators
export const setFirstAppUsage = () => ({
  type: SET_FIRST_APP_USAGE,
});

export const setActiveSession = sessionId => ({
  type: SET_ACTIVE_SESSION,
  payload: sessionId,
});

export const setCurrentUser = userId => ({
  type: SET_CURRENT_USER,
  payload: userId,
});

export const resetSettings = () => ({
  type: RESET_SETTINGS,
});
