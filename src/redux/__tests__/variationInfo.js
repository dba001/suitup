import reducer, {
  SET_VARIATION_INFO,
  variationInfoSet,
} from './../variationInfo';

jest.unmock('./../variationInfo');

const helpers = require('./../../helpers');
helpers.uuid = jest.fn(() => 'uuid');

describe('>>> R E D U C E R --- Test variationInfo Reducer', () => {
  const stateSet = {
    uuid: {
      asins: {
        variantUUID: {
          color: 'blue',
          size: 'Medium',
        },
      },
      colors: {
        blue: {
          id: 'blue',
          sizes: { Medium: 'variantUUID' },
        },
      },
      sizes: {
        Medium: {
          id: 'Medium',
          colors: { blue: 'variantUUID' },
        },
      },
    },
  };

  test('+++ reducer SET_VARIATION_INFO', () => {
    const expectedState = {
      uuid: {
        variantUUID: {
          offer: { offerListingId: 'offerListingId' },
        },
      },
    };
    const actualState = reducer(stateSet, {
      type: SET_VARIATION_INFO,
      payload: {
        variantUUID: { offer: { offerListingId: 'offerListingId' } },
      },
      meta: {
        asin: 'uuid',
      },
    });
    expect(actualState).toEqual(expectedState);
  });
});

describe('>>> A C T I O N S --- Test variationInfo Action', () => {
  test('+++ actionCreator variationInfoSet', () => {
    const add = variationInfoSet();
    expect(add).toEqual({
      type: SET_VARIATION_INFO,
      payload: undefined,
      meta: {
        asin: undefined,
        size: undefined,
      },
    });
  });
  test('+++ actionCreator variationInfoSet with parameter', () => {
    const add = variationInfoSet('uuid', {});
    expect(add).toEqual({
      type: SET_VARIATION_INFO,
      payload: {},
      meta: {
        asin: 'uuid',
      },
    });
  });
});
