import reducer, {
  createBasket,
  addToBasket,
  removeFromBasket,
  chamgeAmount,
  initialBasket,
  CREATE_BASKET,
  ADD_TO_BASKET,
  REMOVE_FROM_BASKET,
  CHANGE_AMOUNT,
} from './../baskets';
import configureStore from 'redux-mock-store';

jest.unmock('./../baskets');

describe('>>> R E D U C E R --- Test basket Reducer', () => {
  const state = {};
  const stateAdd = { uuid: {} };
  const stateRemove = {
    uuid: {
      items: {
        asin: {
          asin: 'asin',
          amount: 1,
          offerListingId: 'offerListingId',
        },
      },
      remote: undefined,
    },
  };

  test('+++ reducer CREATE_BASKET', () => {
    const expectedState = { uuid: { ...initialBasket, id: 'uuid' } };
    const actualState = reducer(state, {
      type: CREATE_BASKET,
      payload: 'uuid',
    });
    expect(actualState).toEqual(expectedState);
  });
  test('+++ reducer ADD_TO_BASKET', () => {
    const expectedState = {
      uuid: {
        items: {
          asin: {
            asin: 'asin',
            basket: 'uuid',
            amount: 1,
            parentAsin: 'parentAsin',
          },
        },
      },
    };
    const actualState = reducer(stateAdd, {
      type: ADD_TO_BASKET,
      payload: 'asin',
      meta: {
        basket: 'uuid',
        amount: 1,
        parentAsin: 'parentAsin',
      },
    });
    expect(actualState).toEqual(expectedState);
  });
  test('+++ reducer REMOVE_FROM_BASKET', () => {
    const expectedState = { uuid: initialBasket };
    const actualState = reducer(stateRemove, {
      type: REMOVE_FROM_BASKET,
      payload: 'asin',
      meta: { basket: 'uuid' },
    });
    expect(actualState).toEqual(expectedState);
  });
  test('+++ reducer CHANGE_AMOUNT', () => {
    const expectedState = {
      uuid: {
        items: {
          asin: {
            asin: 'asin',
            amount: 2,
            offerListingId: 'offerListingId',
          },
        },
        remote: undefined,
      },
    };
    const actualState = reducer(stateRemove, {
      type: CHANGE_AMOUNT,
      payload: 2,
      meta: { basket: 'uuid', asin: 'asin' },
    });
    expect(actualState).toEqual(expectedState);
  });
});

describe('>>> A C T I O N S --- Test basket Action', () => {
  test('+++ actionCreator createBasket', () => {
    const create = createBasket();
    expect(create).toEqual({
      type: CREATE_BASKET,
      payload: expect.stringMatching(/.*/),
    });
  });
  test('+++ actionCreator addToBasket', () => {
    const add = addToBasket('basketId', 'parentAsin', 'asin');
    expect(add).toEqual({
      type: ADD_TO_BASKET,
      payload: 'asin',
      meta: {
        basket: 'basketId',
        amount: 1,
        parentAsin: 'parentAsin',
      },
    });
  });
  test('+++ actionCreator addToBasket with amount', () => {
    const add = addToBasket('basketId', 'parentAsin', 'asin', 2);
    expect(add).toEqual({
      type: ADD_TO_BASKET,
      payload: 'asin',
      meta: {
        basket: 'basketId',
        amount: 2,
        parentAsin: 'parentAsin',
      },
    });
  });
  test('+++ actionCreator removeFromBasket', () => {
    const remove = removeFromBasket('basketId', 'asin');
    expect(remove).toEqual({
      type: REMOVE_FROM_BASKET,
      payload: 'asin',
      meta: { basket: 'basketId' },
    });
  });
});
