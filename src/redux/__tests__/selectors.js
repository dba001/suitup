import mapValues from 'lodash/mapValues';
import { initialState } from '../categories';
import { updateIn } from 'helpers/';

import { selectRandomArticle } from '../selectors';
import expectExport from 'expect';

describe('>>> S E L E C T O R S --- categoryQueues', () => {
  const state = {
    categories: { ...initialState },
    categoryQueues: {
      ...Object.keys(initialState).reduce((acc, browseNode) => {
        acc[browseNode] = ['a'];
        return acc;
      }, {}),
    },
    articles: { a: { asin: 'a' } },
  };

  test('+++ selectRandomArticle initial state', () => {
    const article = selectRandomArticle(state);
    expect(article).toEqual({
      asin: 'a',
    });
    console.log(article);
  });
  test('+++ selectRandomArticle only one toggled category', () => {
    // deselect all categories besides one
    let untoggledCategoriesState = updateIn(
      `categories`,
      categories =>
        mapValues(categories, category => ({
          ...category,
          selected: false,
        })),
      state,
    );
    untoggledCategoriesState.categories[
      Object.keys(untoggledCategoriesState.categories)[0]
    ].selected = true;

    const article = selectRandomArticle(untoggledCategoriesState);
    expect(article).toEqual({
      asin: 'a',
    });
    console.log(article);
  });
});
