import union from 'lodash/union';
import { updateIn } from 'helpers';

import {
  getCategoryQueuesBelowThresholdById,
  selectCategories,
  selectRandomArticle,
} from './selectors';

// Actions
import { requestArticles, REQUEST_ARTICLES_FULLFILLED } from './articles';

export const CREATE_QUEUES = 'CREATE_QUEUES';
export const POP_ARTICLE = 'POP_ARTICLE';
export const PUT_ARTICLE = 'PUT_ARTICLE';

const initialState = {};

// Reducer
// reducer name = articleQueues
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_QUEUES:
      return {
        ...state,
        ...action.payload.reduce((acc, browseNode) => {
          if (!state[browseNode]) acc[browseNode] = [];
          return acc;
        }, {}),
      };
    case REQUEST_ARTICLES_FULLFILLED: {
      return updateIn(
        [action.payload.browseNode],
        queue => union(queue, action.payload.ids),
        state,
      );
    }
    case POP_ARTICLE: {
      return updateIn([action.payload], queue => queue.slice(1), state);
    }
    case PUT_ARTICLE: {
      return updateIn(
        [action.payload.browseNode],
        queue => [action.payload.asin, ...queue.slice()],
        state,
      );
    }
    default:
      return state;
  }
}

// Action Creators
export const createQueue = browseNodes => ({
  type: CREATE_QUEUES,
  payload: browseNodes,
});

export const popArticle = browseNode => ({
  type: POP_ARTICLE,
  payload: browseNode,
});

export const putArticle = (browseNode, asin) => ({
  type: PUT_ARTICLE,
  payload: { browseNode, asin },
});

export const popRandomArticle = () => (dispatch, getState) => {
  const state = getState();
  const rndArticle = selectRandomArticle(state);
  if (!rndArticle)
    throw new Error(
      'popRandomArticle: No article to pop. Probably the categoryQueues are not above threshold.',
    );
  dispatch(popArticle(rndArticle.browseNode));
  return rndArticle;
};

const mapBrowseNodeId = categories =>
  categories.map(category => category.browseNode);

export const createQueues = () => (dispatch, getState) => {
  const state = getState();
  const browseNodes = mapBrowseNodeId(Object.values(selectCategories(state)));
  dispatch(createQueue(browseNodes));
};

export const fillQueues = () => (dispatch, getState) => {
  const state = getState();
  const browseNodes = getCategoryQueuesBelowThresholdById(state);
  dispatch(requestArticles(browseNodes));
};
