import { CONSTANTS } from 'constants/';

import { loginWithAmazon, awsGetUser, awsPutUser } from 'aws/users';
import { getCurrentUserId, getUser } from './selectors';
import { setCurrentUser } from './settings';
import { authenticateSessions } from './sessions';

import { Logger } from '@aws-amplify/core';

const logger = new Logger('users', 'INFO');

// Actions
export const CREATE_USER = 'CREATE_USER';
export const SYNC_USER = 'SYNC_USER';
export const AUTHENTICATE_USER = 'AUTHENTICATE_USER';

const initialState = {};

// Reducer
// reducer name = articles
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CREATE_USER: {
      const userId = action.payload.userId;
      return {
        [userId]: {
          audience: action.payload.audience,
          created: new Date(),
          authenticated: false,
        },
      };
    }
    case SYNC_USER: {
      const localUser = action.payload.localUser;
      const remoteUser = action.payload.remoteUser;
      return {
        [localUser.userId]: {
          ...remoteUser,
          ...localUser,
        },
      };
    }
    case AUTHENTICATE_USER: {
      const authedUserId = action.payload.authedUser;
      const unauhtedUserId = action.payload.unauthedUser;
      const unauthedUser = state[unauhtedUserId];
      if (unauthedUser) {
        let newState = {
          ...state,
          [authedUserId]: {
            ...unauthedUser,
            sub: action.payload.sub,
            authenticated: true,
            userId: authedUserId,
          },
        };
        delete newState[unauhtedUserId];
        return newState;
      } else {
        throw new Error(`Users: No match for unauthedUser ${unauthedUser}`);
      }
    }
    default:
      return state;
  }
}

// Action Creators
const createUser = (userId, audience) => ({
  type: CREATE_USER,
  payload: {
    userId,
    audience,
  },
});

export const syncUser = (localUser, remoteUser) => ({
  type: SYNC_USER,
  payload: {
    localUser,
    remoteUser,
  },
});

export const isDefaultUser = userId => userId === CONSTANTS.DEFAULT_USER;

export const createInitialUser = audience => dispatch => {
  const userId = CONSTANTS.DEFAULT_USER;
  dispatch(createUser(userId, audience));
  dispatch(setCurrentUser(userId));
  return userId;
};

export const _authenticateUser = (unauthedUser, authedUser, sub) => ({
  type: AUTHENTICATE_USER,
  payload: {
    unauthedUser,
    authedUser,
    sub,
  },
});

export const authenticateDefaultUser = (userId, sub) => (
  dispatch,
  getState,
) => {
  const currentUserId = getCurrentUserId(getState());
  dispatch(authenticateSessions(userId));
  dispatch(_authenticateUser(currentUserId, userId, sub));
  dispatch(setCurrentUser(userId));
  return userId;
};

export const login = () => async (dispatch, getState) => {
  try {
    let currentUserId = getCurrentUserId(getState());

    const loginInfo = await loginWithAmazon();
    const sub = loginInfo.token.profile.user_id;
    if (isDefaultUser(currentUserId)) {
      dispatch(
        authenticateDefaultUser(loginInfo.credentials.data.IdentityId, sub),
      );
    }
    // getting current user after the default user is authenticated
    currentUserId = getCurrentUserId(getState());
    const currentUser = getUser(getState(), currentUserId);
    const remoteUser = await awsGetUser();
    if (remoteUser && remoteUser.userId) {
      dispatch(syncUser(currentUser, remoteUser));
    } else {
      console.log('CURRENT USER', currentUser);
      const response = await awsPutUser(currentUser);
      logger.debug('login: Created user...', response);
    }
    return Promise.resolve();
  } catch (err) {
    const msg = 'login:';
    logger.debug(msg, err);
    return Promise.reject(new Error(msg, err));
  }
};
