import { addObjects, removeObjects } from 'helpers/';

// Actions
import { HANDLE_ARTICLE_ERROR } from './articles';
export const REMOVE_ERROR = 'REMOVE_ERROR';

const initialState = {};

// Reducer
// reducer name = articles
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case HANDLE_ARTICLE_ERROR:
      return { ...state, ...{ [action.type]: action.payload } };
    case REMOVE_ERROR: {
      let stateCopy = { ...state };
      delete stateCopy[action.payload];
      return stateCopy;
    }
    default:
      return state;
  }
}

// Action Creators
export const removeError = key => ({
  type: REMOVE_ERROR,
  payload: key,
});
