import random from 'lodash/random';
import map from 'lodash/map';
import pickBy from 'lodash/pickBy';
import filter from 'lodash/filter';
import isEmpty from 'lodash/isEmpty';
import intersection from 'lodash/intersection';
import chunk from 'lodash/chunk';
import { createSelector } from 'reselect';

import { translate } from 'helpers';
import { CONFIG, CONSTANTS } from 'constants/';

// Settings Selectors
export const getActiveSessionId = state =>
  state.settings[CONSTANTS.SETTINGS.activeSession];
export const isFirstAppUsage = state =>
  state.settings[CONSTANTS.SETTINGS.isFirstAppUsage];
export const getCurrentUserId = state =>
  state.settings[CONSTANTS.SETTINGS.currentUser];

// Basket Selectors
export const getBasket = (state, basketId) => state.baskets[basketId];
export const getActiveSessionBasketId = state =>
  getActiveSession(state)[CONSTANTS.SETTINGS.sessionBasket];
export const getActiveSessionBasket = state =>
  getBasket(state, getActiveSession(state)[CONSTANTS.SETTINGS.sessionBasket]);
export const basketItemCount = createSelector(
  getActiveSessionBasket,
  basket => {
    if (!basket) return 0;
    return Object.keys(basket.items).length;
  },
);

// User Selectors
export const getUser = (state, userId) => state.users[userId];

// Session Selectors
export const getActiveSession = state =>
  state.sessions[getActiveSessionId(state)];
export const getActiveSessionAsins = state =>
  getActiveSession(state) ? getActiveSession(state).likes : [];
export const getActiveSessionLastArticleAddedId = state =>
  getActiveSession(state)
    ? getActiveSession(state).lastArticleAdded
    : undefined;
export const getSessions = state => state.sessions;
export const selectCurrentUserSessions = createSelector(
  getSessions,
  getCurrentUserId,
  (sessions, userId) => pickBy(sessions, session => session.userId === userId),
);
export const getCurrentArticleFromSession = createSelector(
  getActiveSession,
  session => session.currentArticle,
);
export const getNextArticleFromSession = createSelector(
  getActiveSession,
  session => session.nextArticle,
);
export const areArticlesSet = createSelector(
  getCurrentArticleFromSession,
  getNextArticleFromSession,
  (current, next) => !(isEmpty(current) && isEmpty(next)),
);

// Article & ArticleQueue Selectors
// articleQueue is a virtual construct constructed
// from categoryQueues
const articleSelector = state => state.articles;
export const selectArticle = (state, asin) => articleSelector(state)[asin];
export const hasArticle = (state, asin) => !!selectArticle(state, asin);
export const selectArticles = (state, asins) =>
  asins ? asins.map(asin => selectArticle(state, asin)) : [];

export const currentArticleSelector = createSelector(
  articleSelector,
  getCurrentArticleFromSession,
  (articles, currentArticleAsin) => articles?.[currentArticleAsin],
);

export const nextArticleSelector = createSelector(
  articleSelector,
  getNextArticleFromSession,
  (articles, nextArticleAsin) => articles?.[nextArticleAsin],
);

export const getLastArticleAddedSelector = createSelector(
  getActiveSessionLastArticleAddedId,
  lastArticleAddedAsin => lastArticleAddedAsin,
);

export const selectRandomArticle = state => {
  const categoryQueues = getCategoryQueuesById(state);
  const numCategoryQueues = categoryQueues.length - 1;
  // can happen when reloading the articles does not work
  if (numCategoryQueues < 0) return undefined;
  const randomQueue = random(numCategoryQueues);
  return selectArticle(
    state,
    selectCategoryQueue(state, categoryQueues[randomQueue])[0],
  );
};

// Categories
export const selectCategories = state => state.categories;
export const selectCategory = (state, browseNode) =>
  selectCategories(state)?.[browseNode];
export const getItemPageForCategory = (state, browseNode) =>
  selectCategory(state, browseNode).itemPage;

export const getChunkedCategories = createSelector(
  state => state.categories,
  categories =>
    chunk(
      map(Object.values(categories), category => ({
        ...category,
        title: translate(category.browseKey),
      })),
      CONFIG.numTilesPerRow,
    ),
);

export const getToggledCategories = createSelector(
  selectCategories,
  categories =>
    map(
      filter(categories, category => category.selected),
      category => category.browseNode,
    ),
);

// CategoryQueues Selectors
const selectCategoryQueues = state => state.categoryQueues;
export const selectCategoryQueue = (state, browseNode) =>
  selectCategoryQueues(state)?.[browseNode];

// select queues with less than threshold of articles stored
export const getCategoryQueuesBelowThresholdById = createSelector(
  selectCategoryQueues,
  queueState =>
    Object.keys(
      pickBy(queueState, queue => queue.length <= CONFIG.queueFilledThreshold),
    ),
);
// select queues with more than the threshold of articles stored
export const getCategoryQueuesAboveThresholdById = createSelector(
  selectCategoryQueues,
  queueState =>
    Object.keys(
      pickBy(queueState, queue => queue.length >= CONFIG.queueFilledThreshold),
    ),
);

// Get all ids of categoryQueues which are selected and
// above the threshhold
export const getCategoryQueuesById = createSelector(
  getToggledCategories,
  getCategoryQueuesAboveThresholdById,
  (selectedQueues, filledQueues) => intersection(selectedQueues, filledQueues),
);

// return true when selected categoryQueues are above the threshhold
export const categoryQueuesAreFilled = createSelector(
  getToggledCategories,
  getCategoryQueuesById,
  (toggledCategories, filledQueueIds) => {
    const numAllToggledCategories = toggledCategories.length;
    const numFilledToggledQueues = filledQueueIds.length;
    if (numFilledToggledQueues === 0) return false;
    return (
      numFilledToggledQueues / numAllToggledCategories >=
      CONFIG.queuesFilledThreshold
    );
  },
);

// get all categoryQueues which are selected
export const getCategoryQueues = createSelector(
  selectCategoryQueues,
  getCategoryQueuesById,
  (queues, categoryQueueIds) => map(categoryQueueIds, id => queues[id]),
);

// Variation info
const variationInfoSelector = state => state.variationInfo;
export const selectVariationInfo = (state, asin) => {
  return variationInfoSelector(state)?.[asin];
};
export const selectVariation = (state, parentAsin, variationAsin) =>
  selectVariationInfo(state, parentAsin).asins[variationAsin];

export const activeSessionArticleSelector = createSelector(
  state => selectArticles(state, getActiveSessionAsins(state)),
  articles => articles,
);

export const isRehydrated = state => state?.['_persist']?.rehydrated;
export const isCategoryFetching = state => state.requests.categoryRequests > 0;
