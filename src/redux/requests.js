import {
  REQUEST_ARTICLES,
  REQUEST_ARTICLES_FULLFILLED,
  REQUEST_ARTICLES_FAILED,
} from './articles';

const initialState = {
  categoryRequests: 0,
};

// Reducer
// reducer name = settings
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST_ARTICLES: {
      const requests = action?.payload?.length || 0;
      if (requests > 0)
        return {
          ...state,
          categoryRequests: requests,
        };
      return state;
    }
    case REQUEST_ARTICLES_FULLFILLED:
    case REQUEST_ARTICLES_FAILED: {
      let requests = state.categoryRequests - 1;
      if (requests < 0) requests = 0;
      return {
        ...state,
        categoryRequests: requests,
      };
    }
    default:
      return state;
  }
}
