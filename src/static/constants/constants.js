const RATING = {
  LIKE: 'LIKE',
  DISLIKE: 'DISLIKE',
};

export const SETTINGS = {
  isFirstAppUsage: 'isFirstAppUsage',
  activeSession: 'activeSession',
  currentUser: 'currentUser',
  sessionBasket: 'basket',
};

export const AUDIENCE = {
  men: 'AUDIENCE_MEN',
  women: 'AUDIENCE_WOMEN',
};

export const DEFAULT_USER = 'DEFAULT_USER';

export default {
  DEFAULT_USER,
  RATING,
  SETTINGS,
  AUDIENCE,
};
