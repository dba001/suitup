/* global __DEV__ */
import { AmazonConfig } from 'react-native-oidc-client';

export const amazonConfig = new AmazonConfig({
  response_type: 'code',
  scope: 'profile',
  client_id: 'amzn1.application-oa2-client.dab48ed2b12849b8a925c8257d457965',
  client_secret:
    '5fa59acef773f81bf7923bcf15b85503742f0659607f9954b484bf5383e2c3b7',
  redirect_uri: 'https://app.suit-up',
  acr: 'default',
  prompt: 'consent login',
  loadUserInfo: true,
});

export default {
  // App Details
  appName: 'suitUp',
  // Default language to use
  iso_lang: 'en',
  // Timeout for server requests (in seconds)
  connectionTimeout: 60,
  // Build Configuration - eg. Debug or Release?
  DEV: __DEV__,
  // name of the ID Attribute for App objects,
  idName: 'asin',
  // threshold for one categoryQueue over this their status is filled
  queueFilledThreshold: 3,
  // threshold for categorieyQueues over this the articles will get selected
  queuesFilledThreshold: 0.5,
  // Tile per category row
  numTilesPerRow: 2,
};
