import { Auth, API, Logger } from '@aws-amplify/core';
import { getCurrentUserId, getUser } from 'selectors/';
import { Client } from 'react-native-oidc-client';
import { amazonConfig } from 'constants/';

const logger = new Logger('users', 'INFO');

const federatedAmazonSignIn = async token => {
  const { access_token, expires_in } = token;
  const date = new Date();
  const expires_at = expires_in * 1000 + date.getTime();
  if (!access_token) {
    return;
  }

  const user = {
    email: token.profile.email,
    name: token.profile.name,
  };
  try {
    const credentials = await Auth.federatedSignIn(
      'amazon',
      { token: access_token, expires_at },
      user,
    );
    return Promise.resolve(credentials);
  } catch (err) {
    console.log(`federatedSignIn: failed to authenticate ${err}`);
    return Promise.reject(err);
  }
};

export const loginWithAmazon = async () => {
  let token;
  let credentials;
  const oidcClient = new Client(amazonConfig);

  token = await oidcClient.getToken();
  if (token) {
    if (!token.expired) {
      logger.debug(`loginWithAmazon: user has a valid token.`, token);
    } else {
      logger.debug(
        `loginWithAmazon: token has expired. Try to refresh...`,
        token,
      );
      token = await oidcClient.refresh();
      logger.debug(`Token refreshed.`, token);
    }
  } else {
    try {
      logger.debug(
        `loginWithAmazon: There is no token stored for current user.`,
      );
      token = await oidcClient.authorize();
    } catch (err) {
      logger.debug(`loginWithAmazon: error in authorization ${err}.`);
      return Promise.reject(err);
    }
  }
  if (!token) {
    logger.debug(`loginWithAmazon: retrieved no token for authorization.`);
    return Promise.reject(
      new Error(`loginWithAmazon: retrieved no token for authorization.`),
    );
  }
  logger.debug('Amazon token: ', token);
  try {
    credentials = await federatedAmazonSignIn(token);
  } catch (err) {
    logger.debug(`loginWithAmazon: error in federatedSignIn.`, err);
    return Promise.reject(err);
  }
  if (!credentials) {
    logger.debug(
      `loginWithAmazon: retrieved no credentials for authorization.`,
    );
    return Promise.reject(
      new Error(`loginWithAmazon: retrieved no credentials for authorization.`),
    );
  }
  logger.debug('Amazon credentials', credentials);
  if (!credentials.data || !credentials.data.IdentityId) {
    logger.debug(`loginWithAmazon: user has no identityId.`);
    return Promise.reject(
      new Error(`loginWithAmazon: user has no identityId.`),
    );
  }

  return Promise.resolve({ token, credentials });
};

export const awsUpdateUser = addtlParams => async (dispatch, getState) => {
  const state = getState();
  const currentUserId = getCurrentUserId(state);
  let currentUser = getUser(state, currentUserId);
  if (currentUser) {
    try {
      if (addtlParams) currentUser = { ...currentUser, ...addtlParams };
      const response = await awsPutUser(currentUser);
      logger.debug(`Post response`, response);
      return Promise.resolve(response);
    } catch (err) {
      logger.debug(`Failed to post user`, currentUser);
      return Promise.reject(
        new Error(`Failed to post user ${JSON.stringify(currentUser)}`),
      );
    }
  } else {
    logger.debug(`No current User with ID ${currentUserId}`);
    return Promise.reject(
      new Error(`No current User with ID ${currentUserId}`),
    );
  }
};

export const awsGetUser = async () => {
  try {
    const user = await API.get('userCRUD', '/user');
    if (user.error) {
      const msg = `awsGetUser: ${user.error}`;
      logger.debug(msg);
      return Promise.reject(new Error(msg));
    } else {
      logger.debug(`awsGetUser: received response from AWS:`, user);
      return Promise.resolve(user);
    }
  } catch (err) {
    logger.debug(`Can not retrieve user:`, err);
    return Promise.reject(err);
  }
};

export const awsPutUser = async payload => {
  try {
    return Promise.resolve(
      await API.put('userCRUD', '/user', { body: payload }),
    );
  } catch (err) {
    const msg = 'putUser: Not able to fullfill request';
    logger.debug(msg, err);
    return Promise.reject(new Error(msg, err));
  }
};
