import compact from 'lodash/compact';
import { setIn, updateIn } from 'helpers';
import { Logger } from '@aws-amplify/core';

const logger = new Logger('itemTransformer', 'INFO');

const SIZE = 'Size';
const COLOR = 'Color';
const VIABLE_DIMENSIONS = [COLOR, SIZE];

const transformArticle = (browseNode, article) => ({
  id: article.ASIN,
  asin: article.ASIN,
  browseNode: browseNode,
  ...getItemAttributes(article),
  imagesets: getImageSet(article),
  review: getArticleReview(article),
});

export const transformProductAPIResponse = (items, browseNode) => {
  if (!items)
    throw new Error(
      `transformProductAPIResponse: No items in product API response. BrowseNode -> ${browseNode}`,
    );
  return compact(
    items.map(item => {
      let variationInfo;
      try {
        variationInfo = transformArticleVariations(item?.Variations);
      } catch (error) {
        logger.debug(
          `transformProductAPIResponse: Article has no variationInfo ${article.asin}`,
        );
      }
      let article = transformArticle(browseNode, item);
      if (!article.imagesets) {
        const imageSet =
          variationInfo?.colors?.[Object.keys(variationInfo.colors)[0]]
            ?.imagesets;
        if (imageSet) {
          article.imagesets = imageSet;
        } else {
          logger.debug(
            'transformProductAPIResponse: article has no imageset. article ->',
            article,
            'variationInfo ->',
            variationInfo,
          );
          return undefined;
        }
      }
      return {
        article,
        variationInfo,
      };
    }),
  );
};

export const getItemAttributes = item => item.ItemAttributes;

export const checkViableDimension = (dimension, dimensions) =>
  dimensions?.some(v => v.toLowerCase() === dimension.toLowerCase());

export const getDimensionValue = (dimension, dimensionValues) =>
  dimensionValues?.[dimension.toLowerCase()];

export const addAsin = (info, item) => setIn(['asins', item.asin], item, info);
export const addColor = (info, item, isColorOnly = false) => {
  const colors = info.colors;
  if (!colors[item.color]) {
    return setIn(
      ['colors', item.color],
      isColorOnly
        ? { asin: item.asin, id: item.color, imagesets: item.imagesets }
        : { id: item.color, imagesets: item.imagesets },
      info,
    );
  }
  return info;
};
export const addSize = (info, item) => {
  info = updateIn(
    ['sizes', item.size],
    size =>
      size
        ? {
            ...size,
            colors: { ...size.colors, [item.color]: item.asin },
          }
        : { id: item.size, colors: { [item.color]: item.asin } },
    info,
  );
  return updateIn(
    ['colors', item.color, 'sizes'],
    sizes =>
      sizes ? { ...sizes, [item.size]: item.asin } : { [item.size]: item.asin },
    info,
  );
};

const variationItemToVariationInfo = (info, item, hasSize, isColorOnly) => {
  info = addAsin(info, item);
  info = addColor(info, item, isColorOnly);
  if (hasSize) {
    info = addSize(info, item);
  }
  return info;
};

export const transformArticleVariations = variations => {
  if (variations) {
    const variationDimensions =
      variations?.VariationDimensions?.VariationDimension;
    const dimensionNames = Array.isArray(variationDimensions)
      ? variationDimensions
          .filter(dimension =>
            checkViableDimension(dimension, VIABLE_DIMENSIONS),
          )
          .sort()
      : checkViableDimension(variationDimensions, VIABLE_DIMENSIONS)
      ? [variationDimensions]
      : [];

    if (dimensionNames.length === 0)
      throw new Error(
        'transformArticleVariations: No viable dimensions ->',
        variations,
      );
    const variationItems = Array.isArray(variations.Item)
      ? variations.Item
      : [variations.Item];

    // generate the shape of the returning object from variation dimensions
    const variationsTemplate = dimensionNames.reduce(
      (acc, name) => {
        acc[`${name.toLowerCase()}s`] = {};
        return acc;
      },
      {
        asins: {},
      },
    );
    const isColorOnly =
      checkViableDimension(COLOR, dimensionNames) &&
      dimensionNames.length === 1;
    const hasSize = checkViableDimension(SIZE, dimensionNames);
    return variationItems.reduce((variationInfo, variation) => {
      const variationItem = transformArticleVariation(
        variation,
        dimensionNames,
      );
      return variationItemToVariationInfo(
        variationInfo,
        variationItem,
        hasSize,
        isColorOnly,
      );
    }, variationsTemplate);
  }
  return undefined;
};

const extractImageSet = imageSet =>
  imageSet
    ? {
        LargeImage: imageSet.LargeImage.URL,
        MediumImage: imageSet.MediumImage.URL,
        ThumbnailImage: imageSet.LargeImage.URL,
      }
    : undefined;

export const getImageSet = item => {
  const getCategory = imageSet => imageSet?.$?.Category;
  const itemImageSets = item?.ImageSets?.ImageSet;

  if (itemImageSets) {
    if (Array.isArray(itemImageSets)) {
      const primary = itemImageSets?.find(
        set => getCategory(set) === 'primary',
      );
      if (primary) {
        const variants =
          itemImageSets?.filter(set => set.$.Category === 'variant') || [];

        return {
          primary: extractImageSet(primary),
          variant: variants?.map(variant => extractImageSet(variant)),
        };
      }
    } else {
      // const category = getCategory(itemImageSets);
      // if (category === 'primary')
      return { primary: extractImageSet(itemImageSets), variant: [] };
    }
  }
  return undefined;
};

export const transformArticleVariation = (variation, dimensionNames) => {
  logger.debug(`transformArticleVariation: input variation ->`, variation.ASIN);
  logger.debug(
    `transformArticleVariation: input dimensionNames ->`,
    dimensionNames,
  );
  const attributes = getItemAttributes(variation);

  const variationAttribute = variation?.VariationAttributes?.VariationAttribute;
  const variationAttributes = Array.isArray(variationAttribute)
    ? variationAttribute
    : variationAttribute
    ? [variationAttribute]
    : [];
  const dimensionValues = variationAttributes.reduce((acc, cur) => {
    acc[cur.Name.toLowerCase()] = cur.Value;
    return acc;
  }, {});
  const dimensionKeys = dimensionValues && Object.keys(dimensionValues);
  logger.debug(
    `transformArticleVariation: getting attributes ->`,
    !!attributes,
  );
  logger.debug(
    `transformArticleVariation: dimensionValues ->`,
    dimensionValues,
  );
  if (!dimensionValues || dimensionKeys.length <= 0)
    throw new Error(`Variation has no dimensions. Variation: ${variation}`);

  const asin = variation.ASIN;
  const hasColor = checkViableDimension(COLOR, dimensionKeys);
  const hasSize = checkViableDimension(SIZE, dimensionKeys);

  const imagesets = getImageSet(variation);
  const offer = transformVariationOffer(variation);
  logger.debug(
    `transformArticleVariation: imagesets has primary->`,
    !!imagesets?.primary,
  );
  logger.debug(
    `transformArticleVariation: hasColor -> ${hasColor}, hasSize -> ${hasSize}`,
  );
  let transformedVariation;
  if (hasColor) {
    const color = getDimensionValue(COLOR, dimensionValues);
    const size = getDimensionValue(SIZE, dimensionValues);

    transformedVariation = {
      asin,
      ...attributes,
      color,
      imagesets,
      offer,
    };

    if (hasSize) transformedVariation.size = size;
    return transformedVariation;
  }
  throw new Error(`transformArticleVariation: variation has no color.`);
};

export const hasOffers = article => {
  const offers = article?.Offers;
  if (!offers) return;

  const totalOffers = offers.TotalOffers;
  return totalOffers > 0;
};

export const transformVariationOffer = variation => {
  const offers = variation?.Offers;
  if (!offers) return undefined;

  const offer = offers && offers.Offer;
  const offerListing = offer.OfferListing;
  const condition = offer.OfferAttributes && offer.OfferAttributes.Condition;
  const offerListingId = offerListing.OfferListingId;
  const price = offerListing.Price;
  const salePrice = offerListing.SalePrice;
  const amountSaved =
    offerListing.AmountSaved && offerListing.AmountSaved.FormattedPrice;
  const availability = {
    type: offerListing.AvailabilityAttributes.AvailabilityType,
    minHours: offerListing.AvailabilityAttributes.MinimumHours,
    maxHours: offerListing.AvailabilityAttributes.MaximumHours,
    text: offerListing.Availability,
  };
  logger.debug(
    `getOffer: For ASIN: ${variation.ASIN} offerListingId: ${offerListingId}`,
  );

  return {
    offerListingId,
    merchant: offer.Merchant.Name || '',
    condition,
    price,
    salePrice,
    amountSaved,
    availability,
    IsEligibleForPrime: offerListing.IsEligibleForPrime,
  };
};

export const getArticleReview = item => item?.CustomerReviews?.IFrameURL;
