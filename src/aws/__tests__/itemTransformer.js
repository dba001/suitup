import {
  amazonItemGenerator,
  amazonItemGeneratorOnlyColor,
} from '../__data__/amazon';
import {
  addColor,
  addSize,
  getImageSet,
  transformArticleVariation,
  transformArticleVariations,
} from '../itemTransformer';

const dimensionNames = ['Color', 'Size'];
const amazonItem = amazonItemGenerator();
const amazonItemOnlyColor = amazonItemGeneratorOnlyColor();

describe('>>> ItemTransformer --- Test transformation of multiple amazon item variations', () => {
  test('+++ addColor and add Sizes', () => {
    const info = { colors: {}, sizes: {}, asins: {} };
    const expected = {
      colors: {
        blue: { id: 'blue', imagesets: {} },
      },
      sizes: {},
      asins: {},
    };
    let actual = addColor(info, {
      asin: 'asin',
      color: 'blue',
      imagesets: {},
    });
    expect(actual).toEqual(expected);
    const expectedAddedSize = {
      colors: {
        blue: { id: 'blue', imagesets: {}, sizes: { 44: 'asin' } },
      },
      sizes: { 44: { id: 44, colors: { blue: 'asin' } } },
      asins: {},
    };
    actual = addSize(actual, {
      asin: 'asin',
      color: 'blue',
      size: 44,
      imagesets: {},
    });
    expect(actual).toEqual(expectedAddedSize);
    const expectedAnotherSize = {
      colors: {
        blue: { id: 'blue', imagesets: {}, sizes: { 44: 'asin' } },
        red: { id: 'red', imagesets: {}, sizes: { 44: 'asin2' } },
      },
      sizes: { 44: { colors: { blue: 'asin', red: 'asin2' }, id: 44 } },
      asins: {},
    };
    actual = addColor(actual, {
      asin: 'asin2',
      color: 'red',
      size: 44,
      imagesets: {},
    });
    actual = addSize(actual, {
      asin: 'asin2',
      color: 'red',
      size: 44,
      imagesets: {},
    });
    expect(actual).toEqual(expectedAnotherSize);
  });
  test('+++ tranfsorm all variations of an article', () => {
    const expected = expect.objectContaining({
      colors: expect.objectContaining({
        ['Blau (Blue 5959)']: {
          id: 'Blau (Blue 5959)',
          imagesets: {
            primary: {
              LargeImage: expect.any(String),
              MediumImage: expect.any(String),
              ThumbnailImage: expect.any(String),
            },
            variant: expect.arrayContaining([
              {
                LargeImage: expect.any(String),
                MediumImage: expect.any(String),
                ThumbnailImage: expect.any(String),
              },
            ]),
          },
          sizes: expect.objectContaining({ 44: 'B077RTPRBD' }),
        },
      }),
      sizes: expect.objectContaining({
        44: {
          id: '44',
          colors: expect.objectContaining({
            ['Blau (Blue 5959)']: 'B077RTPRBD',
          }),
        },
      }),
      asins: expect.objectContaining({
        B077RTPRBD: expect.objectContaining({
          imagesets: {
            primary: {
              LargeImage: expect.any(String),
              MediumImage: expect.any(String),
              ThumbnailImage: expect.any(String),
            },
            variant: expect.arrayContaining([
              {
                LargeImage: expect.any(String),
                MediumImage: expect.any(String),
                ThumbnailImage: expect.any(String),
              },
            ]),
          },
        }),
      }),
    });
    const actual = transformArticleVariations(amazonItem.Variations);
    expect(actual).toEqual(expected);
  });
  test('+++ tranfsorm all variations of an article color only', () => {
    const expected = expect.objectContaining({
      colors: expect.objectContaining({
        ['Carbon Schwarz']: {
          asin: 'B01C3FUNGA',
          id: 'Carbon Schwarz',
          imagesets: {
            primary: {
              LargeImage: expect.any(String),
              MediumImage: expect.any(String),
              ThumbnailImage: expect.any(String),
            },
            variant: expect.arrayContaining([
              {
                LargeImage: expect.any(String),
                MediumImage: expect.any(String),
                ThumbnailImage: expect.any(String),
              },
            ]),
          },
        },
      }),
      asins: expect.objectContaining({
        B01C3FUNGA: expect.objectContaining({
          imagesets: {
            primary: {
              LargeImage: expect.any(String),
              MediumImage: expect.any(String),
              ThumbnailImage: expect.any(String),
            },
            variant: expect.arrayContaining([
              {
                LargeImage: expect.any(String),
                MediumImage: expect.any(String),
                ThumbnailImage: expect.any(String),
              },
            ]),
          },
        }),
      }),
    });
    const actual = transformArticleVariations(amazonItemOnlyColor.Variations);
    expect(actual).toEqual(expected);
  });
});

describe('>>> ItemTransformer --- Test transformation of an single amazon item Variation', () => {
  test('+++ getImageSet of an item variation', () => {
    const expected = {
      primary: {
        LargeImage: expect.any(String),
        MediumImage: expect.any(String),
        ThumbnailImage: expect.any(String),
      },
      variant: expect.arrayContaining([
        {
          LargeImage: expect.any(String),
          MediumImage: expect.any(String),
          ThumbnailImage: expect.any(String),
        },
      ]),
    };
    const actual = getImageSet(amazonItem.Variations.Item[0]);
    expect(actual).toEqual(expected);
  });
  test('+++ transform an item variation', () => {
    const expected = expect.objectContaining({
      asin: 'B077RTPRBD',
      color: 'Blau (Blue 5959)',
      size: '44',
      Title: expect.any(String),
      imagesets: {
        primary: {
          LargeImage: expect.any(String),
          MediumImage: expect.any(String),
          ThumbnailImage: expect.any(String),
        },
        variant: expect.arrayContaining([
          {
            LargeImage: expect.any(String),
            MediumImage: expect.any(String),
            ThumbnailImage: expect.any(String),
          },
        ]),
      },
    });
    const actual = transformArticleVariation(
      amazonItem.Variations.Item[0],
      dimensionNames,
    );
    expect(actual).toEqual(expected);
  });
  test('+++ transform an item variation color only', () => {
    const expected = expect.objectContaining({
      asin: 'B01C3FUNGA',
      color: 'Carbon Schwarz',
      Title: expect.any(String),
      imagesets: {
        primary: {
          LargeImage: expect.any(String),
          MediumImage: expect.any(String),
          ThumbnailImage: expect.any(String),
        },
        variant: expect.arrayContaining([
          {
            LargeImage: expect.any(String),
            MediumImage: expect.any(String),
            ThumbnailImage: expect.any(String),
          },
        ]),
      },
    });
    const actual = transformArticleVariation(
      amazonItemOnlyColor.Variations.Item[0],
      ['Color'],
    );
    expect(actual).toEqual(expected);
  });
  test('+++ transform an item empty variation', () => {
    expect(() => transformArticleVariation({}, dimensionNames)).toThrowError(
      /^Variation has no dimensions. Variation:/,
    );
  });
  test('+++ transform a variation without color', () => {
    expect(() =>
      transformArticleVariation(
        {
          ImageSets: {
            ImageSet: [
              {
                $: { Category: 'primary' },
                LargeImage: { URL: 'url' },
                MediumImage: { URL: 'url' },
                ThumbnailImage: { URL: 'url' },
              },
            ],
          },
          VariationAttributes: {
            VariationAttribute: [
              {
                Name: 'SomeDim',
                Value: 'SomeValue',
              },
            ],
          },
        },
        dimensionNames,
      ),
    ).toThrowError(/^transformArticleVariation: variation has no color./);
  });
});
