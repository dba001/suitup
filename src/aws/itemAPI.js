import chunk from 'lodash/chunk';
import isString from 'lodash/isString';
import Piranhax from 'piranhax';
import Amplify, { Logger } from '@aws-amplify/core';

import {
  transformProductAPIResponse,
  transformVariationOffer,
  getArticleReview,
} from './itemTransformer';

const logger = new Logger('ItemAPI', 'INFO');

const AWSAccessKeyId = 'AKIAI7VUU3DQZW5R5W5Q';
const AWSSecretAccessKey = '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';
const AssociateTag = 'finderai-21';

// AWS Limit for itemIds in one request
const LIMIT = 10;

class ItemAPI {
  constructor() {
    this.productClient = new Piranhax(
      AWSAccessKeyId,
      AWSSecretAccessKey,
      AssociateTag,
    );
    this.productClient.setLocale('DE');
  }

  isValidResponse = response => response.get('Request')?.IsValid;
  getResponseItems = response => response.get('Item');

  apperalSearch = async (browseNode, itemPage) => {
    const searchIndex = 'Apparel';
    const responseGroup = ['Images', 'ItemAttributes', 'Reviews'];
    try {
      const response = await this.productClient.ItemSearch(searchIndex, {
        ResponseGroup: responseGroup,
        BrowseNode: browseNode,
        ItemPage: itemPage,
      });
      logger.debug('apperalSearch resonse ->', response);
      if (this.isValidResponse(response)) {
        const transformedItems = transformProductAPIResponse(
          this.getResponseItems(response),
          browseNode,
        );

        logger.debug('apperalSearch transformed response ->', transformedItems);
        return transformedItems;
      }
      return Promise.reject(new Error(`apperealSearch: response was invalid.`));
    } catch (error) {
      logger.error(
        `apperealSearch: searching for appearel. BrowseNode -> ${browseNode}. Error -> ${error}`,
      );
    }
  };

  lookupItems = async (allItemIds, responseGroup = []) => {
    logger.info(
      `lookupItems: request ASIN: ${allItemIds} with ResponseGroup: ${responseGroup}`,
    );
    let requests;

    // make allItemIds to an array if they are not
    if (!Array.isArray(allItemIds)) {
      if (isString(allItemIds)) allItemIds = [allItemIds];
      else
        return Promise.reject(
          `lookupItems: allItemIds are messed up: ${allItemIds}`,
        );
    }

    if (allItemIds.length > LIMIT) {
      logger.debug(`lookupItems: limit exceeded splitting request.`);
      requests = chunk(allItemIds, LIMIT);
    } else {
      requests = [allItemIds];
    }
    let items = [];
    for (let itemIds of requests) {
      try {
        const response = await this.productClient.ItemLookup(itemIds, {
          ResponseGroup: responseGroup,
        });
        if (this.isValidResponse(response)) {
          // get all items from response
          items = items.concat(this.getResponseItems(response));
        } else {
          logger.debug(`lookupItems: response is invalid`);
        }
      } catch (err) {
        logger.debug(`lookupItems: response ${err}`);
      }
    }
    return items;
  };

  delay = (f, t) => new Promise(resolve => setTimeout(() => resolve(f()), t));

  getOffersAndReview = items => {
    return items.reduce((infoMap, item) => {
      const offer = this.transformVariationOffer(item);
      const review = this.getArticleReview(item);

      if (offer)
        infoMap[item.ASIN] = {
          ...infoMap[item.ASIN],
          offer: offer,
        };
      if (review)
        infoMap[item.ASIN] = {
          ...infoMap[item.ASIN],
          review: review,
        };

      return infoMap;
    }, {});
  };

  lookupOffer = async (asins, retries = 0) => {
    const responseGroup = ['ItemAttributes', 'OfferFull', 'Reviews'];

    const items = await this.lookupItems(asins, responseGroup);
    if (items) {
      return this.getOffersAndReview(items);
    } else {
      if (retries < 4) {
        return await this.delay(
          () => this.lookupOffer(asins, retries + 1),
          500,
        );
      }
    }
    return null;
  };

  lookupItemInfo = async asin => {
    const items = await this.lookupItemsInfo(asin);
    if (!items) {
      logger.info(`lookupItemInfo: No items.`);
      return undefined;
    }
    if (items.length > 1)
      logger.info(
        `lookupItemInfo: more than one item (${items.length}). Returning the first one.`,
      );

    return items[0];
  };

  lookupItemsInfo = async (asins, retries = 0) => {
    const responseGroup = [
      'ItemAttributes',
      'OfferFull',
      'Reviews',
      'Variations',
    ];

    const items = await this.lookupItems(asins, responseGroup);
    if (items && items.length > 0) {
      return transformProductAPIResponse(items);
    } else {
      if (retries < 4) {
        return await this.delay(
          () => this.lookupItemsInfo(asins, retries + 1),
          500,
        );
      }
    }
    return null;
  };

  createCartItems = items =>
    items.map(item => {
      if (item.OfferListingId) {
        return this.productClient.CreateCartItem(
          'OfferListingId',
          item.OfferListingId,
          item.amount,
        );
      } else {
        return this.productClient.CreateCartItem(
          'ASIN',
          item.asin,
          item.amount,
        );
      }
    });

  createRemoteBasket = items => {
    const cartItems = this.createCartItems(items);
    return this.productClient.CartCreate(cartItems);
  };

  addToRemoteBasket = (remoteCart, items) => {
    const cartItems = this.createCartItems(items);
    return this.productClient.CartAdd(
      cartItems,
      remoteCart.CartId,
      remoteCart.HMAC,
    );
  };

  getRemoteBasket = (cartId, HMAC) => {
    return this.productClient.CartGet(cartId, '', HMAC);
  };
}

const itemAPI = new ItemAPI();
export default itemAPI;
