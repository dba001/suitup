import { API, Logger } from '@aws-amplify/core';

const logger = new Logger('sessions', 'INFO');

export const awsGetUserSessions = async () => {
  try {
    return Promise.resolve(await API.get('userCRUD', '/user/sessions'));
  } catch (err) {
    const msg = 'awsGetUserSessions: Request failed.';
    logger.debug(msg, err);
    return Promise.reject(new Error(msg, err));
  }
};

export const awsPutUserSessions = async payload => {
  if (!Array.isArray(payload)) {
    return Promise.reject(
      new Error('awsPutUserSessions: payload has to be an array.'),
    );
  }
  try {
    return Promise.resolve(
      await API.put('userCRUD', '/user/sessions', { body: payload }),
    );
  } catch (err) {
    const msg = 'awsPutUserSessions: Not able to fullfill request';
    logger.debug(msg, err);
    return Promise.reject(new Error(msg, err));
  }
};
