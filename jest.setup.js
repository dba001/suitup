if (typeof window !== 'object') {
  global.window = global;
  global.window.navigator = {};
  global.window.addEventListener = jest.fn();
}

jest.mock('helpers/locale', () => ({
  getLocale: () => 'us-en',
  getLocaleFallback: () => 'us-en',
}));
