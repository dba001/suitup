import { AppRegistry, YellowBox } from 'react-native';
import { screensEnabled } from 'react-native-screens';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import IonIcons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import AppContainer from 'src/index';

YellowBox.ignoreWarnings(['Warning:']);
// console.warn = () => {};

FontAwesome.loadFont();
IonIcons.loadFont();
MaterialIcons.loadFont();

screensEnabled();
AppRegistry.registerComponent('suitup', () => AppContainer);
