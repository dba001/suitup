/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
const blacklist = require('metro-config/src/defaults/blacklist');

module.exports = {
  resolver: {
    blacklistRE: blacklist([
      /react-native\/local-cli\/core\/__fixtures__.*/,
      /react-native-oidc-client[/\\]node_modules[/\\]react-native[/\\].*/,
      /awsmobilejs[/\\].*/,
    ]),
  },
};
